# WeiEdge

## QESF

Quick Embeded Software Framework(QESF)，快速嵌入式软件框架

* [qesf](qesf/README.md)：QESF工程源码
* [win](win/README.md)：QESF windows mingw环境测试

### Features

* DeviceModel：统一设备模型
    * UART
    * IIC
    * SPI
    * SpiFlash
* 标准C库函数实现：vsnprintf、sprintf、snprintf、memset、memcpy、memcmp、htonl、htons、strlen、strstr...
* Looger，支持前缀、等级、颜色，hexdump、bitdump
* initcall
* 泛型数据结构：DoubleList、RingBuffer、RingQueue、GenQueue、GenBuffer、MinPriorityQueue、StringBuilder、Bitmap、WorkQueue

### TODO

- [ ] cmake统一windows/linux/arm等编译器
- [ ] cmake config配置
- [ ] 内置FatFs文件系统
- [ ] 轻量级shell
- [ ] SpiFlash支持自定义dummy时序
- [ ] trace跟踪
- [ ] ...
