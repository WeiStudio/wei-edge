
#ifndef __QE_ARCH_H__
#define __QE_ARCH_H__


#include "qe_def.h"


qe_u32 qe_time(void);
qe_u32 qe_time_ms(void);
qe_u64 qe_time_us(void);

void qe_sleep(qe_u32 ts);
void qe_msleep(qe_u32 ms);
void qe_usleep(qe_u64 us);

void qe_interrupt_disable(void);
void qe_interrupt_enable(void);
void qe_irq_disable(int irq);
void qe_irq_unregister(int irq);
qe_err_t qe_irq_enable(int irq);
qe_err_t qe_irq_register(int irq, void (*handuler)(void *), void *ref);
#endif /* __QE_HW_H__ */
