
#ifndef __QE_TIMER_H__
#define __QE_TIMER_H__



#include "qe_core.h"



#define QE_TMR_DEV_CTRL_INIT        0x20
#define QE_TMR_DEV_CTRL_START       0x21
#define QE_TMR_DEV_CTRL_STOP      	0x22
#define QE_TMR_DEV_CTRL_CFG         0x23
#define QE_TMR_DEV_CTRL_CLEAR_ISR	0x24

#define QE_TMR_DEV_AUTO_RELOAD		0x01
#define QE_TMR_DEV_COUNT_DOWN		0x02
#define QE_TMR_DEV_INT_MODE			0x04

struct qe_timer_configure {
	qe_u32 period;
	qe_u8  option;
	qe_u8  reserve[3];
};

#endif /* __QE_TIMER_H__ */

