

#ifndef __QE_SERIAL_H__
#define __QE_SERIAL_H__



#include "qe_device.h"


#define BAUD_RATE_2400                  2400
#define BAUD_RATE_4800                  4800
#define BAUD_RATE_9600                  9600
#define BAUD_RATE_19200                 19200
#define BAUD_RATE_38400                 38400
#define BAUD_RATE_57600                 57600
#define BAUD_RATE_115200                115200
#define BAUD_RATE_230400                230400
#define BAUD_RATE_256000                256000
#define BAUD_RATE_460800                460800
#define BAUD_RATE_921600                921600
#define BAUD_RATE_2000000               2000000
#define BAUD_RATE_3000000               3000000

#define DATA_BITS_5                     5
#define DATA_BITS_6                     6
#define DATA_BITS_7                     7
#define DATA_BITS_8                     8
#define DATA_BITS_9                     9

#define STOP_BITS_1                     0
#define STOP_BITS_2                     1
#define STOP_BITS_3                     2
#define STOP_BITS_4                     3

#define PARITY_NONE                     0
#define PARITY_ODD                      1
#define PARITY_EVEN                     2

#define BIT_ORDER_LSB                   0
#define BIT_ORDER_MSB                   1

#define NRZ_NORMAL                      0       /* Non Return to Zero : normal mode */
#define NRZ_INVERTED                    1       /* Non Return to Zero : inverted mode */

#ifndef QE_SERIAL_RBSZ
#define QE_SERIAL_RBSZ					64
#endif

#define QE_SERIAL_DMA_RX                0x01

#define QE_SERIAL_EVENT_RX_IND          0x01    /* Rx indication */
#define QE_SERIAL_EVENT_TX_DONE         0x02    /* Tx complete   */
#define QE_SERIAL_EVENT_RX_DMADONE      0x03    /* Rx DMA transfer done */
#define QE_SERIAL_EVENT_TX_DMADONE      0x04    /* Tx DMA transfer done */
#define QE_SERIAL_EVENT_RX_TIMEOUT      0x05    /* Rx timeout    */


/* Default config for serial configure struct */
#define QE_SERIAL_DEFCONFIG						\
{												\
	BAUD_RATE_115200, /* 115200 bits/s */		\
	DATA_BITS_8,	  /* 8 databits */			\
	STOP_BITS_1,	  /* 1 stopbit */		    \
	PARITY_NONE,	  /* No parity  */			\
	BIT_ORDER_LSB,								\
	NRZ_NORMAL,									\
	QE_SERIAL_RBSZ,								\
	0											\
}

struct qe_serial_configure {
    qe_u32 baud_rate;

    qe_u32 data_bits               :4;
    qe_u32 stop_bits               :2;
    qe_u32 parity                  :2;
    qe_u32 bit_order               :1;
    qe_u32 invert                  :1;
    qe_u32 bufsz                   :16;
    qe_u32 reserved                :6;
};

struct qe_serial_rxfifo {
	qe_u8 *buffer;
	qe_u16 put_index;
	qe_u16 get_index;
	qe_bool_t	is_full;
};

struct qe_serial_rxdma {
    qe_bool_t activated;
};

struct qe_serial_txfifo
{
    void *completion;
};

struct qe_serial_device {
    struct qe_device          	parent;

    const struct qe_uart_ops 	*ops;
    struct qe_serial_configure   config;

    void *rx;
    void *tx;
};
typedef struct qe_serial_device qe_serial_t;

/**
 * uart operators
 */
struct qe_uart_ops {
    qe_err_t (*configure)(struct qe_serial_device *serial, struct qe_serial_configure *cfg);
    qe_err_t (*control)(struct qe_serial_device *serial, int cmd, void *arg);

    int (*putc)(struct qe_serial_device *serial, char c);
    int (*getc)(struct qe_serial_device *serial);

    qe_size_t (*dma_transmit)(struct qe_serial_device *serial, qe_u8 *buf, qe_size_t size, int direction);
};

qe_err_t qe_serial_register(qe_serial_t              *serial,
								   const char 			   *name,
								   qe_u32 			    flag,
								   void 				    *data);

void qe_hw_serial_isr(struct qe_serial_device *serial, int event);

#endif /* __QE_SERIAL_H__ */

