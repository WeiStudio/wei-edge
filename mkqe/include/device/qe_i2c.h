
#ifndef __QE_I2C_H__
#define __QE_I2C_H__

#include "qe_core.h"



#define QE_I2C_WR                0x0000
#define QE_I2C_RD               (1u << 0)
#define QE_I2C_ADDR_10BIT       (1u << 2)  /* this is a ten bit chip address */
#define QE_I2C_NO_START         (1u << 4)
#define QE_I2C_IGNORE_NACK      (1u << 5)
#define QE_I2C_NO_READ_ACK      (1u << 6)  /* when I2C reading, we do not ACK */
#define QE_I2C_NO_STOP          (1u << 7)

#define QE_I2C_DEV_CTRL_10BIT        0x20
#define QE_I2C_DEV_CTRL_ADDR         0x21
#define QE_I2C_DEV_CTRL_TIMEOUT      0x22
#define QE_I2C_DEV_CTRL_RW           0x23
#define QE_I2C_DEV_CTRL_CLK          0x24

struct qe_i2c_message {
    qe_u16 addr;
    qe_u16 flags;
    qe_u16 len;
    qe_u8  *buf;
};

struct qe_i2c_bus;

struct qe_i2c_bus_ops {
    qe_size_t (*master_xfer)(struct qe_i2c_bus *bus,
                             struct qe_i2c_message msgs[],
                             qe_u32 num);
    qe_size_t (*slave_xfer)(struct qe_i2c_bus *bus,
                            struct qe_i2c_message msgs[],
                            qe_u32 num);
    qe_err_t (*control)(struct qe_i2c_bus *bus,
                                qe_u32,
                                qe_u32);
};

struct qe_i2c_bus {
    struct qe_device parent;
    const struct qe_i2c_bus_ops *ops;
    qe_u16  flags;
    qe_u16  addr;
    qe_u32  timeout;
    qe_u32  retries;
	void *lock;
    void *priv;

	qe_u32 multi_access:1;
	qe_u32 reserve:31;
	
	qe_err_t (*bus_lock)(struct qe_i2c_bus *bus, qe_s32 ms);
	void     (*bus_unlock)(struct qe_i2c_bus *bus);
};

struct qe_i2c_priv_data {
    struct qe_i2c_message *msgs;
    qe_size_t  number;
};



qe_err_t qe_i2c_bus_register(struct qe_i2c_bus *bus,
									  const char        *name);

struct qe_i2c_bus *qe_i2c_bus_find(const char *name);

qe_size_t qe_i2c_transfer(struct qe_i2c_bus     *bus,
                          		struct qe_i2c_message  msgs[],
                          		qe_u32            num);

qe_err_t qe_i2c_control(struct qe_i2c_bus *bus,
                        	  qe_u32        cmd,
                        	  qe_u32        arg);

qe_size_t qe_i2c_master_send(struct qe_i2c_bus *bus,
									   qe_u16 		  addr,
									   qe_u16  	  flags,
									   const qe_u8  *buf,
									   qe_u32        count);

qe_size_t qe_i2c_master_recv(struct qe_i2c_bus *bus,
                             		  qe_u16         addr,
                             		  qe_u16         flags,
                             		  qe_u8         *buf,
                             		  qe_u32         count);
#endif /* __QE_I2C_H__ */

