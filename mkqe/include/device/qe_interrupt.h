
#ifndef __QE_INTC_H__
#define __QE_INTC_H__


#define QE_INT_DEV_CTRL_INIT        0x20
#define QE_INT_DEV_CTRL_START       0x21
#define QE_INT_DEV_CTRL_STOP      	0x22
#define QE_INT_DEV_CTRL_CFG         0x23
#define QE_INT_DEV_CTRL_CLEAR_ISR	0x24


#endif /* __QE_INTC_H__ */

