
#ifndef __QE_DEVICE_H__
#define __QE_DEVICE_H__



#include "qe_core.h"


#include "qe_spi.h"
#include "qe_spi_flash.h"

#include "qe_i2c.h"

#include "qe_serial.h"

#include "qe_imgsen.h"

#include "qe_timer.h"



qe_err_t qe_device_register(struct qe_device *dev,
                                          const char *name,
                                          qe_u16 flags);
qe_err_t qe_device_unregister(struct qe_device *dev);
struct qe_device *qe_device_find(const char *name);
qe_err_t qe_device_control(qe_device_t dev, int cmd, void *arg);
qe_err_t qe_device_close(struct qe_device *dev);
qe_err_t qe_device_open(struct qe_device *dev, qe_u16 oflag);
qe_size_t qe_device_read(struct qe_device *dev,
								 qe_off_t		   pos,
								 void             *buffer,
								 qe_size_t   size);
qe_size_t qe_device_write(struct qe_device *dev,
								 qe_off_t		   pos,
  								 const void       *buffer,
  								 qe_size_t         size);
qe_err_t 
qe_device_set_rx_indicate(struct qe_device *dev,
                          qe_err_t (*rx_ind)(struct qe_device *, qe_size_t));
qe_err_t qe_device_set_tx_complete(struct qe_device *dev,
                         					     qe_err_t (*tx_done)(struct qe_device *, void *));


#define QE_DEV_LOGNAME    "dev"
#define dev_trace(...)  qelog_trace(QE_DEV_LOGNAME,  __VA_ARGS__);
#define dev_debug(...)  qelog_debug(QE_DEV_LOGNAME,  __VA_ARGS__);
#define dev_info(...)   qelog_info(QE_DEV_LOGNAME,   __VA_ARGS__);
#define dev_notice(...) qelog_notice(QE_DEV_LOGNAME, __VA_ARGS__);
#define dev_error(...)  qelog_error(QE_DEV_LOGNAME,  __VA_ARGS__);
#define dev_warn(...)   qelog_warn(QE_DEV_LOGNAME,   __VA_ARGS__);
#define dev_bug(...)    qelog_bug(QE_DEV_LOGNAME,    __VA_ARGS__);

#endif /* __QE_DEVICE_H__ */

