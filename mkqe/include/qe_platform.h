


#ifndef __QE_PLATFORM_H__
#define __QE_PLATFORM_H__



#include "qe_def.h"



#if defined(CONFIG_PLATFORM_OS)
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#define qe_malloc 		malloc
#define qe_free 		free
#define qe_memcpy 		memcpy
#define qe_memset		memset
#define qe_memmove      memmove
#define qe_memcmp       memcmp
#define qe_strcpy       strcpy
#define qe_strncpy      strncpy
#define qe_strcmp       strcmp
#define qe_strncmp      strncmp
#define qe_strlen 		strlen
#define qe_sprintf		sprintf
#define qe_snprintf     snprintf
#define qe_vsnprintf	vsnprintf
#else
#include <stdarg.h>

void  qe_free(void *ptr);
void *qe_malloc(qe_size_t size);
void *qe_zalloc(qe_size_t size);
void *qe_realloc(void *buf, qe_size_t size);

int qe_strncasecmp(const char *s1, const char *s2, qe_size_t len);
int qe_strcasecmp(const char *s1, const char *s2);
char *qe_strcpy(char *dest, const char *src);
char *qe_strncpy(char *dest, const char *src, qe_size_t count);
qe_size_t qe_strlcpy(char *dest, const char *src, qe_size_t size);
char *qe_strcat(char *dest, const char *src);
char *qe_strncat(char *dest, const char *src, qe_size_t count);
int qe_strcmp(const char *cs, const char *ct);
int qe_strncmp(const char *cs, const char *ct, qe_size_t count);
char *qe_strchr(const char *s, int c);
const char *qe_strchrnul(const char *s, int c);
char *qe_strrchr(const char *s, int c);
qe_size_t qe_strlen(const char *s);
qe_size_t qe_strnlen(const char *s, qe_size_t count);
qe_size_t qe_strcspn(const char *s, const char *reject);
char *qe_strdup(const char *s);
char *qe_strndup(const char *s, qe_size_t n);
qe_size_t qe_strspn(const char *s, const char *accept);
char *qe_strpbrk(const char * cs, const char * ct);
char *qe_strtok(char *s, const char *ct);
char *qe_strsep(char **s, const char *ct);
char *qe_strswab(const char *s);

void *qe_memset(void *s, int c, qe_size_t count);
void *qe_memcpy(void *dst, void *src, qe_size_t count);
int qe_memcmp(const void *cs,const void *ct, qe_size_t count);
void *qe_memmove(void *dest, const void *src, qe_size_t count);
void *qe_memscan(void *addr, int c, qe_size_t size);
char *qe_strstr(const char *s1, const char *s2);
void *qe_memchr(const void *s, int c, qe_size_t n);

int qe_vsnprintf(char *buf, qe_size_t size, const char *fmt, va_list args);
int qe_snprintf(char *buf, qe_size_t size, const char *fmt, ...);
int qe_vsprintf(char *buf, const char *format, va_list arg_ptr);

#endif

#define qe_assert(ex)                                                      	\
if (!(ex))                                                               	\
{                                                                         	\
    qe_assert_handler(#ex, __FUNCTION__, __LINE__);                       	\
}

qe_u32 qe_time(void);
qe_u32 qe_time_ms(void);
void qe_sleep(qe_u32 ms);



#endif /* __QE_PLATFORM_H__ */