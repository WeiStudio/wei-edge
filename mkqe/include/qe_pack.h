


#ifndef __QE_PACK_H__
#define __QE_PACK_H__



#define qe_pack_align(n)                __attribute__ ((aligned (n)))

#define _qe_pack_bytes(data, p, bytes) do {\
					qe_memcpy(p, (char *)&data, bytes);\
					p+= bytes;\
				}while(0)
#define _qe_pack_stris(data, p, bytes) do {\
					qe_strncpy((char *)p, (char *)data, bytes);\
					p+= bytes;\
				}while(0)
#define qe_pack_byte(data, p) 				_qe_pack_bytes(data, p, 1)
#define qe_pack_word(data, p) 				_qe_pack_bytes(data, p, 2)
#define qe_pack_dwrd(data, p) 				_qe_pack_bytes(data, p, 4)
#define qe_pack_bytes(data, p, len) 		_qe_pack_bytes(data, p, len)
#define qe_pack_stris(data, p, len)			_qe_pack_stris(data, p, len)

#define _qe_pack_bytes_safe(data, p, bytes, end) do {\
					if ((end - p) >= bytes)\
						qe_memcpy(p, (unsigned char *)&data, bytes);\
					p+= bytes;\
				}while(0)
#define _qe_pack_stris_safe(data, p, bytes, end) do {\
					if ((end - p) >= bytes)\
						qe_strncpy(p, data, bytes);\
					p+= bytes;\
				}while(0)

#define qe_pack_byte_safe(data, p, end) 		_qe_pack_bytes_safe(data, p, 1, end)
#define qe_pack_word_safe(data, p, end) 		_qe_pack_bytes_safe(data, p, 2, end)
#define qe_pack_dwrd_safe(data, p, end) 		_qe_pack_bytes_safe(data, p, 4, end)
#define qe_pack_bytes_safe(data, p, end, ln)	_qe_pack_bytes_safe(data, p, ln, end)
#define qe_pack_stris_safe(data, p, end, ln)	_qe_pack_bytes_safe(data, p, ln, end)

#define _qe_unpack_bytes(data, p, bytes) do {\
		qe_memcpy((unsigned char *)&data, p, bytes);\
		p+= bytes;\
	}while(0)
#define _qe_unpack_stris(data, p, bytes) do {\
			qe_strncpy((char *)data, (char *)p, bytes);\
			p+= bytes;\
		}while(0)

#define qe_unpack_byte(data, p)			_qe_unpack_bytes(data, p, 1)
#define qe_unpack_word(data, p)			_qe_unpack_bytes(data, p, 2)
#define qe_unpack_dwrd(data, p)			_qe_unpack_bytes(data, p, 4)
#define qe_unpack_bytes(data, p, len)	_qe_unpack_bytes(data, p, len)
#define qe_unpack_stris(data, p, len)	_qe_unpack_stris(data, p, len)



#endif /* __QE_PACK_H__ */