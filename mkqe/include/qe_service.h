
#ifndef __QE_SERVICE_H__
#define __QE_SERVICE_H__


#include "qe_def.h"
#include "qe_list.h"
#include "qe_pack.h"


/**
 * qe_container_of - return the member address of ptr, if the type of ptr is the
 * struct type.
 */
#define qe_container_of(ptr, type, member) \
    ((type *)((char *)(ptr) - (unsigned long)(&((type *)0)->member)))


#define qe_array_size(array) (sizeof(array)/sizeof(array[0]))
#define qe_array_foreach(head, size, p)				\
		int __i;									\
		p = &head[0];								\
													\
		for (__i = 0;								\
			 __i < size;							\
			 __i++, 								\
				p = &head[__i]) 					\

#define qe_array_foreach_start(head, size, p) 		\
{													\
	int __i;										\
	p = &head[0];									\
	for (__i = 0;									\
		__i < size;									\
		__i++, 										\
		p = &head[__i]) 							\

#define qe_array_foreach_end() \
}

#define qe_mask_exst(mask, flag)   		((mask) & (flag))
#define qe_mask_push(mask, flag)   		((mask) |= (flag))
#define qe_mask_pull(mask, flag)		((mask) &= (~flag))
#define qe_mask_only(mask, flag)   		(!((mask) & (~(flag))))


int qe_strb_build(qe_strb_t *strb, const char *fmt, ...);
qe_strb_t qe_strb_frombuf(char *buf, int size);

#define qe_strb_none()					{QE_NULL, QE_NULL, 0, 0}
#define qe_strb_init(s, max)			{(s), (s), max, qe_strlen(s)}

#define qe_strb_string(p, s)			(qe_strb_build(&p, "%s", s))
#define qe_strb_number(p, n)			(qe_strb_build(&p, "%d", n))
#define qe_strb_hex(p, h)				(qe_strb_build(&p, "%x", h))
#define qe_strb_format(p, fmt, ...)		(qe_strb_build(&p, fmt, ##__VA_ARGS__))

#endif /* SRC_QE_SERVICE_H_ */
