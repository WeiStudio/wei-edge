/**
 * @file qe_shell.h
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2023-02-11
 * 
 * @copyright Copyright (c) 2023
 * 
 */


#ifndef __QE_SHELL_H__
#define __QE_SHELL_H__



#include "qe_core.h"



typedef struct {

    struct {
        unsigned int cursor;                                 /* cursor position */
        struct qe_gbuf *buffer;
        char *param[QE_SHELL_PARAMETER_MAX_NUM];
        unsigned param_count;
        int key;
    }parser;

#if QE_SHELL_HISTORY_MAX_NUM > 0
#endif

    struct {
        void *base;
        unsigned short count;
    } command_list;

    struct {
        unsigned char tab:1;
    } flag;

    signed short (*read)(char *, unsigned short);
    signed short (*write)(char *, unsigned short);
    
} qe_shell;

#endif /* __QE_SHELL_H__ */