


#ifndef __ISP_H__
#define __ISP_H__



#include "qe_core.h"
#include "bpc.h"



#define SPC_CC_LINES   	    (2)

#define FFC_FKB_CC_STATIC   (1)
#define FFC_FKB_CC_LINES 	(2)       /* FastKB Calcuate Cache(CC) lines */
#define FFC_FKB_CC_SIZE     (640 * sizeof(qe_u32) * FFC_FKB_CC_LINES * 4)
#define CONFIG_FKB_DEBUG    (0)


#define ISP_LOGNAME         "isp"
#define isp_trace(...)      qelog_trace(ISP_LOGNAME,  __VA_ARGS__)
#define isp_debug(...)      qelog_debug(ISP_LOGNAME,  __VA_ARGS__)
#define isp_info(...)       qelog_info(ISP_LOGNAME,   __VA_ARGS__)
#define isp_notice(...)     qelog_notice(ISP_LOGNAME, __VA_ARGS__)
#define isp_warn(...)       qelog_warn(ISP_LOGNAME,   __VA_ARGS__)
#define isp_error(...)      qelog_error(ISP_LOGNAME,    __VA_ARGS__)
#define isp_bug(...)        qelog_bug(ISP_LOGNAME,    __VA_ARGS__)


int bpc_xp2bit(qe_dim2 dim, struct bpc_xpoint xp, qe_u8 type);
void bpc_seq2xp(struct bpc_xpoint *xp, qe_u32 seq);
void bpc_dynamic_points_remove(struct bpc_desc *desc);
void bpc_desc_init(struct bpc_desc *desc, qe_dim2 dim, qe_u8 fmt, 
    void *comp_addr, unsigned int comp_size);
qe_err_t bpc_xp2seq(struct bpc_xpoint xp);
qe_err_t bpc_bptab_parse(struct bpc_desc *desc, void *tabaddr);
qe_err_t bpc_bptab_export(struct bpc_desc *desc, qe_u8 fmt, 
    void *expaddr, int expsize);
qe_err_t bpc_add_point(struct bpc_desc *desc, struct bpc_npoint *np);
qe_err_t bpc_add_points(struct bpc_desc *desc, qe_list_t *plist);
qe_u32 bpc_frame_detect(struct bpc_desc *desc, void *frame, int kernel_size, 
    qe_s32 thres, qe_list_t *plist);

qe_err_t spc_calc(void *frames_addr, int num_frames, qe_dim2 dim, qe_box box, 
    void *expaddr);

qe_err_t ffc_fastkb(void *lframes, void *hframes, unsigned int num_frames, 
    qe_dim2 dim, qe_box box, void *expaddr);


#endif /* __ISP_H__ */
