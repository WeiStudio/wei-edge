/*
 * @Author       : Wei.Studio
 * @Date         : 2023-01-28 18:47:14
 * @LastEditors  : Wei.Studio awokezhou@gmail.com
 * @LastEditTime : 2023-01-30 11:21:19
 * @FilePath     : \win\include\qe_log.h
 * @Description  : 
 * 
 * Copyright (c) 2022 by Wei.Studio, All Rights Reserved. 
 */



#ifndef __QE_DEBUG_H__
#define __QE_DEBUG_H__



#include "qe_def.h"



#define QELOG_BFZS     		256
#define QELOG_MODEL_MAX		6

#define _QELOG_MODEL_STR(num) #num
#define QELOG_MODEL_STR(num) _QELOG_MODEL_STR(num)


/**
 * Log Color define
 */
#define QE_LOG_COLOR_RESET				"\033[0m"
#define QE_LOG_COLOR_RED				"\033[31m"
#define QE_LOG_COLOR_GREEN				"\033[32m"
#define QE_LOG_COLOR_YELLOW				"\033[33m"
#define QE_LOG_COLOR_BLUE				"\033[34m"
#define QE_LOG_COLOR_PINK				"\033[35m"
#define QE_LOG_COLOR_CYAN				"\033[36m"
#define QE_LOG_COLOR_WHITE				"\033[37m"
#define QE_LOG_COLOR_RED_LIGHT			"\033[91m"
#define QE_LOG_COLOR_GREEN_LIGHT		"\033[92m"
#define QE_LOG_COLOR_YELLOW_LIGHT		"\033[93m"
#define QE_LOG_COLOR_BLUE_LIGHT			"\033[94m"
#define QE_LOG_COLOR_PINK_LIGHT			"\033[95m"
#define QE_LOG_COLOR_CYAN_LIGHT			"\033[96m"
#define QE_LOG_COLOR_WHITE_LIGHT		"\033[97m"

/**
 * Log Level define
 */
typedef enum {
	QELOG_NONE = 0,
	QELOG_BURST,
	QELOG_TRACE,
	QELOG_DEBUG,
	QELOG_INFO,
	QELOG_NOTICE,
	QELOG_WARN,
	QELOG_ERR,
	QELOG_BUG,
	QELOG_DISABLE,	
} qelog_level_e;

struct qelog_level {
	const char *string;
	qe_u8 level;
	const char *color;
};

/* Log Context */
#define QELOG_F_TIME	0x0001		/* Log output with time */
#define QELOG_F_MODEL	0x0002		/* Log output with model name */
#define QELOG_F_FUNC	0x0004		/* Log output with function information */
#define QELOG_F_COLOR	0x0008		/* Log output with color */
#define QELOG_F_LEVEL	0x0010		/* Log output with level */
#define QELOG_F_RING    0x0020      /* Log will output to ringbuffer */
struct qelog_context {
	qe_u8 level;
	qe_u8 f_initialized:1;
    qe_u8 f_reserve:7;
	qe_u16 features;
    struct qe_ringbuffer ring;
	void (*output)(char *buf, int len);
};

void qelog(qe_u8 level, const char *mname, 
    const char *func, int line, const char *fmt, ...);
void qelog_set_output(void (*output)(char *buf, int len));
void qelog_set_level(qe_u8 level);
void qelog_set_features(qe_u8 features);
qe_ringbuffer_t *qelog_get_buffer(void);
qe_err_t qelog_set_buffer(void *buf, qe_u32 size);
void qelog_init(qe_u8 level, qe_u8 features);

void qe_hexdump(qe_u8 level, const char *func, int linenr, 
    const void *vbuf, qe_size_t len);
void qe_bitdump(qe_u8 level, const char *func, int linenr, 
	const void *vbuf, qe_size_t len);

#define qelog_burst(name, ...)	    qelog(QELOG_BURST, 	name, __func__, __LINE__, __VA_ARGS__)
#define qelog_trace(name, ...)	    qelog(QELOG_TRACE, 	name, __func__, __LINE__, __VA_ARGS__)
#define qelog_debug(name, ...)	    qelog(QELOG_DEBUG, 	name, __func__, __LINE__, __VA_ARGS__)
#define qelog_info(name, ...)		qelog(QELOG_INFO, 	name, __func__, __LINE__, __VA_ARGS__)
#define qelog_notice(name, ...)	    qelog(QELOG_NOTICE,	name, __func__, __LINE__, __VA_ARGS__)
#define qelog_error(name, ...)		qelog(QELOG_ERR, 	name, __func__, __LINE__, __VA_ARGS__)
#define qelog_warn(name, ...)		qelog(QELOG_WARN, 	name, __func__, __LINE__, __VA_ARGS__)
#define qelog_bug(name, ...)		qelog(QELOG_BUG, 	name, __func__, __LINE__, __VA_ARGS__)

#define qe_hexdump_burst(...)		qe_hexdump(QELOG_BURST, 	__func__, __LINE__, __VA_ARGS__)
#define qe_hexdump_trace(...)		qe_hexdump(QELOG_TRACE, 	__func__, __LINE__, __VA_ARGS__)
#define qe_hexdump_debug(...)		qe_hexdump(QELOG_DEBUG, 	__func__, __LINE__, __VA_ARGS__)
#define qe_hexdump_info(...)		qe_hexdump(QELOG_INFO, 		__func__, __LINE__, __VA_ARGS__)
#define qe_hexdump_notice(...)		qe_hexdump(QELOG_NOTICE, 	__func__, __LINE__, __VA_ARGS__)
#define qe_hexdump_err(...)			qe_hexdump(QELOG_ERR, 	    __func__, __LINE__, __VA_ARGS__)
#define qe_hexdump_warn(...)		qe_hexdump(QELOG_WARN, 		__func__, __LINE__, __VA_ARGS__)
#define qe_hexdump_bug(...)			qe_hexdump(QELOG_BUG, 	    __func__, __LINE__, __VA_ARGS__)

#define qe_bitdump_burst(...)		qe_bitdump(QELOG_BURST, 	__func__, __LINE__, __VA_ARGS__)
#define qe_bitdump_trace(...)		qe_bitdump(QELOG_TRACE, 	__func__, __LINE__, __VA_ARGS__)
#define qe_bitdump_debug(...)		qe_bitdump(QELOG_DEBUG, 	__func__, __LINE__, __VA_ARGS__)
#define qe_bitdump_info(...)		qe_bitdump(QELOG_INFO, 		__func__, __LINE__, __VA_ARGS__)
#define qe_bitdump_notice(...)		qe_bitdump(QELOG_NOTICE, 	__func__, __LINE__, __VA_ARGS__)
#define qe_bitdump_err(...)			qe_bitdump(QELOG_ERR, 	    __func__, __LINE__, __VA_ARGS__)
#define qe_bitdump_warn(...)		qe_bitdump(QELOG_WARN, 		__func__, __LINE__, __VA_ARGS__)
#define qe_bitdump_bug(...)			qe_bitdump(QELOG_BUG, 	    __func__, __LINE__, __VA_ARGS__)

#define SYS_LOGNAME     "sys"
#define sys_trace(...)  qelog_trace(SYS_LOGNAME, __VA_ARGS__);
#define sys_debug(...)  qelog_debug(SYS_LOGNAME, __VA_ARGS__)
#define sys_info(...)   qelog_info(SYS_LOGNAME, __VA_ARGS__)
#define sys_notice(...) qelog_notice(SYS_LOGNAME, __VA_ARGS__)
#define sys_warn(...)   qelog_warn(SYS_LOGNAME, __VA_ARGS__)
#define sys_error(...)  qelog_error(SYS_LOGNAME, __VA_ARGS__)
#define sys_bug(...)	qelog_bug(SYS_LOGNAME, __VA_ARGS__)
#endif /* __QE_DEBUG_H__ */

