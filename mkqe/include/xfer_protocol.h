
#ifndef __XFER_PROTOCOL_H__
#define __XFER_PROTOCOL_H__



#include "qe_core.h"
#include "qe_cmder.h"



#define XFER_PREFIX			0xAA
#define XFER_POSFIX			0xFF

#define XFER_CMD_RD			0x01
#define XFER_CMD_WR			0x02
#define XFER_CMD_CL			0x03

#define XFER_SPACE_BIT 		3	
#define XFER_SPACE_SHIFT 	29
#define XFER_SPACE_MASK 	(qe_u32)(((1<<XFER_SPACE_BIT)-1)<<XFER_SPACE_SHIFT)

#define XFER_OFFSET_BIT 	(32-XFER_SPACE_BIT)
#define XFER_OFFSET_MASK 	((qe_u32)~XFER_SPACE_MASK)

#define XFER_SPACE_MCU    	(0)
#define XFER_SPACE_FPGA   	(1)
#define XFER_SPACE_SENSOR 	(2)
#define XFER_SPACE_FLASH    (3)
#define XFER_SPACE_VAM      (4)

#define XFER_MAXPAYLOAD 	256

#define __XFER_GET_SPACE(x)    	((qe_u32)(x&XFER_SPACE_MASK))
#define __XFER_GET_OFFSET(x)    ((qe_u32)(x&XFER_OFFSET_MASK))
#define XFER_GET_SPACE(cmd)    (__XFER_GET_SPACE(((struct xfer_head *)cmd)->addr))
#define XFER_GET_OFFSET(cmd)   (__XFER_GET_OFFSET(((struct xfer_head *)cmd)->addr))
#define XFER_GET_DATA(cmd)     (((qe_u8 *)cmd)+sizeof(struct xfer_head))

typedef enum {
    XFER_ADDR_PDTMODEL =        0x00000080,
    XFER_ADDR_PDTMANU =         0x00000090,
    XFER_ADDR_SN =              0x000000A0,
    XFER_ADDR_CAMID =           0x000000A4,
    XFER_ADDR_SWVER =           0x00000100,
    XFER_ADDR_HWVER =           0x00000110,
    XFER_ADDR_FPVER =           0x00000120,
    XFER_ADDR_LOG_LEVEL =       0x00000400,
    XFER_ADDR_BITWIDTH =        0x00001000,
    XFER_ADDR_BIT8 =            0x00001004,
    XFER_ADDR_FPS =             0x00001010,
    XFER_ADDR_FPS_MIN =         0x00001014,
    XFER_ADDR_FPS_MAX =         0x00001018,
    XFER_ADDR_TRIGGER =         0x00001030,
    XFER_ADDR_INVS_EN =         0x00001040,
    XFER_ADDR_HINVS =           0x00001044,
    XFER_ADDR_VINVS =           0x00001048,
    XFER_ADDR_CROSS_EN =        0x00001050,
    XFER_ADDR_CROSS_X =         0x00001054,
    XFER_ADDR_CROSS_Y =         0x00001058,
    XFER_ADDR_CROSS_DEF =       0x0000105C,
    XFER_ADDR_EXPO =            0x00001060,
    XFER_ADDR_EXPO_MIN =        0x00001064,
    XFER_ADDR_EXPO_MAX =        0x00001068,
    XFER_ADDR_EXPO_STEP =       0x0000106C,
    XFER_ADDR_GAIN =            0x00001070,
    XFER_ADDR_GAIN_MIN =        0x00001074,
    XFER_ADDR_GAIN_MAX =        0x00001078,
    XFER_ADDR_GAIN_STEP =       0x0000107C,
    XFER_ADDR_AE_ENABLE =       0x00001080,
    XFER_ADDR_AE_EXPO_EN =      0x00001084,
    XFER_ADDR_AE_GAIN_EN =      0x00001088,
    XFER_ADDR_AE_FRAMES =       0x0000108C,
    XFER_ADDR_AE_MTH =          0x00001090,
    XFER_ADDR_AE_NTH =          0x00001094,
    XFER_ADDR_AE_BRIGHT =       0x00001098,
    XFER_ADDR_AE_TARGET =       0x0000109C,
    XFER_ADDR_TEMP_SENSOR =     0x000010C4,
    XFER_ADDR_ROI0_EN =         0x00001200,
    XFER_ADDR_ROI0_X =          0x00001204,
    XFER_ADDR_ROI0_Y =          0x00001208,
    XFER_ADDR_ROI0_W =          0x0000120C,
    XFER_ADDR_ROI0_H =          0x00001210,

    XFER_ADDR_IPADDR =          0x00001800,
    XFER_ADDR_NETMASK =         0x00001804,
    XFER_ADDR_MACADDR =         0X00001808,
    
    XFER_ADDR_CONFIG_SAVE =     0x00002000,
    XFER_ADDR_CONFIG_RESTORE =  0x00002004,

    XFER_ADDR_STREAM_FIN      = 0x00002400,

    XFER_ADDR_TEC_EN =          0x00002030,
    XFER_ADDR_TEC_TARGET =      0x00002034,
    XFER_ADDR_TEC_MIN =         0x00002038,
    XFER_ADDR_TEC_MAX =         0x0000203C,

    XFER_ADDR_FPS_ERR_COUNT   = 0x00004000,
    XFER_ADDR_FPS_ERR_CLEAR   = 0x00004004,
    
    XFER_ADDR_ORIGIN_IMAGE =    0x00020000,
    XFER_ADDR_COLOR_MODE =      0x00020004,
    XFER_ADDR_BLC =             0x00030000,
    XFER_ADDR_BPC =             0x00031000,
    
    XFER_ADDR_SPC_ENABLE =      0x00032000,
    XFER_ADDR_SPC_CALC =        0x00032004,
    XFER_ADDR_SPC_PARAM =       0x00032008,
    XFER_ADDR_DBPC_EN =         0x0003200C,
    XFER_ADDR_DBPC_PARAM =      0x00032010,
    
    XFER_ADDR_NUC =             0x00033000,
    XFER_ADDR_GAMMA =           0x00034000,
    XFER_ADDR_IFFR =            0x00035000,
    XFER_ADDR_IFFR_TH =         0x00035004,
    XFER_ADDR_IFFR_DI =         0x00035008,
    XFER_ADDR_BINING =          0x00036000,
    XFER_ADDR_CST =             0x00040000,

	XFER_ADDR_LQIT    =         0x02000000,
	XFER_ADDR_LQST    =         0x02000020,
} xfer_mcu_address_e;

struct xfer_head {
	qe_u8 mark;
	qe_u8 cmd:2;
	qe_u8 seq:2;
    qe_u8 space:4;
	qe_u16 len;
	qe_u32 addr;
} qe_pack_align(1);

struct xfer_tail {
	qe_u8 code;
	qe_u8 mark;//0xff
};

struct xfer_mcu_req {
	qe_u32 address;
	qe_err_t (*set)(struct qe_cmder_protocol *proto, 
        qe_u8 *in, int length);
	qe_err_t (*get)(struct qe_cmder_protocol *proto, struct xfer_head *head, 
        qe_gbuf_t *rsp);
};



qe_err_t xfer_scan(struct qe_cmder_protocol *proto, 
					    void                     *buf, 
					    int                       len, 
					    int                      *rlen);

qe_err_t xfer_match(struct qe_cmder_protocol *proto, 
						  void                     *buf, 
						  int 						len, 
						  int 					   *rlen);

qe_err_t xfer_pack_header(qe_gbuf_t *rsp, 
								   struct xfer_head    *head, 
								   qe_u8           code);

qe_err_t xfer_pack_data(qe_gbuf_t *rsp, 
							   struct xfer_head    *head, 
    						   qe_u8          *buf, 
    						   int                  len, 
    						   qe_u8           pad);



#endif /* __XFER_PROTOCOL_H__ */

