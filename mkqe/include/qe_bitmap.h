


#ifndef __QE_BITMAP_H__
#define __QE_BITMAP_H__



#include "qe_def.h"


/**
 * @brief 
 * 
 * @param bit 
 * @param p 
 */
static inline void qe_set_bit(unsigned int bit, volatile unsigned long *p)
{
	unsigned long mask = 1UL << (bit & (QE_BITS_PER_LONG-1));
	p += bit >> QE_LONG_POW;
	*p |= mask;
}

static inline void qe_clear_bit(unsigned int bit, volatile unsigned long *p)
{
	unsigned long mask = 1UL << (bit & (QE_BITS_PER_LONG-1));
	p += bit >> QE_LONG_POW;
	*p &= ~mask;
}

static inline void qe_change_bit(unsigned int bit, volatile unsigned long *p)
{
	unsigned long mask = 1UL << (bit & (QE_BITS_PER_LONG-1));
	p += bit >> QE_LONG_POW;
	*p ^= mask;
}

static inline int qe_test_bit(unsigned int bit, volatile unsigned long *p)
{
	return 1UL & (p[QE_BITS_WORD(bit)] >> (bit & (QE_BITS_PER_LONG-1)));
}

qe_bool_t qe_bitmap_empty(const unsigned long *bitmap, int bits);
qe_bool_t qe_bitmap_full(const unsigned long *bitmap, int bits);
void qe_bitmap_set(unsigned long *map, int start, int nr);
void qe_bitmap_clear(unsigned long *map, int start, int nr);
void qe_bitmap_zero(unsigned long *dst, int nbits);
void qe_bitmap_fill(unsigned long *dst, int nbits);



#endif