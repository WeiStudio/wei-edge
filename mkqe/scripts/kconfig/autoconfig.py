import argparse



def main(args):
    kf = open(args.input, 'r')
    mf = open(args.output, 'w')
    while True:
        line = kf.readline()
        if not line:
            break
        if line[0] == '#':
            continue
        if 'y' in line.split('=')[1]:
            mf.write(line)
    kf.close()
    mf.close()

def parse_args():
    
    parser = argparse.ArgumentParser()

    parser.add_argument("input",
                        default='.config',
                        help="input kconfig file")
    parser.add_argument("output",
                        default='autoconfig.mk',
                        help="output makefile file")

    return parser.parse_args()

if __name__ == '__main__':
    args = parse_args()
    try:
        main(args)
    except SystemExit:
        pass