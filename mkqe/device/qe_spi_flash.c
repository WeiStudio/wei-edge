
#include "qe_device.h"
#include "qe_spi.h"
#include "qe_spi_flash.h"



static struct qe_spiflash_chip flash_chip_table[] = {
{"AT45DB161E", 	JEDEC_ID_ATMEL,   0x26, 0x00, 0x81, FLASH_CMB(2),   FLASH_WM_BYTE_DBF,  0x0, 0x0, 512},
{"W25Q40BV", 	JEDEC_ID_WINBOND, 0x40, 0x13, 0x20, FLASH_CKB(512), FLASH_WM_PAGE_256B, 0x0, 0x0, 4096},
{"W25Q16BV", 	JEDEC_ID_WINBOND, 0x40, 0x15, 0x20, FLASH_CMB(2),   FLASH_WM_PAGE_256B, 0x0, 0x0, 4096},
{"W25Q64CV", 	JEDEC_ID_WINBOND, 0x40, 0x17, 0x20, FLASH_CMB(8),   FLASH_WM_PAGE_256B, 0x0, 0x0, 4096},
{"W25Q64DW", 	JEDEC_ID_WINBOND, 0x60, 0x17, 0x20, FLASH_CMB(8),   FLASH_WM_PAGE_256B, 0x0, 0x0, 4096},
{"W25Q128BV", 	JEDEC_ID_WINBOND, 0x40, 0x18, 0x20, FLASH_CMB(16),  FLASH_WM_PAGE_256B, 0x0, 0x0, 4096},
{"W25Q256FV", 	JEDEC_ID_WINBOND, 0x40, 0x19, 0x20, FLASH_CMB(32),  FLASH_WM_PAGE_256B, 0x0, 0x0, 4096},
{"W25QM512JV",	JEDEC_ID_WINBOND, 0x71, 0x19, 0x20, FLASH_CMB(16),  FLASH_WM_PAGE_256B, 0x0, 0x0, 4096},
{"SST25VF016B", JEDEC_ID_SST, 	  0x25, 0x41, 0x20, FLASH_CMB(2),   FLASH_WM_BYTE_AAI,	0x0, 0x0, 4096},
{"M25P32", 		JEDEC_ID_MICRON,  0x20, 0x16, 0xD8, FLASH_CMB(4),   FLASH_WM_PAGE_256B, 0x0, 0x0, FLASH_SKB(64)},
{"M25P80", 		JEDEC_ID_MICRON,  0x20, 0x14, 0xD8, FLASH_CMB(1),   FLASH_WM_PAGE_256B, 0x0, 0x0, FLASH_SKB(64)},
{"M25P40", 		JEDEC_ID_MICRON,  0x20, 0x13, 0xD8, FLASH_CKB(512), FLASH_WM_PAGE_256B, 0x0, 0x0, FLASH_SKB(64)},
{"MT25QL256", 	JEDEC_ID_MICRON,  0xBA, 0x19, 0x20, FLASH_CMB(32),  FLASH_WM_PAGE_256B, 0x0, 0x0, 4096},
{"EN25Q32B", 	JEDEC_ID_EON, 	  0x30, 0x16, 0x20, FLASH_CMB(4),   FLASH_WM_PAGE_256B, 0x0, 0x0, 4096},
{"GD25Q64B", 	JEDEC_ID_GIGADEV, 0x40, 0x17, 0x20, FLASH_CMB(8),   FLASH_WM_PAGE_256B, 0x0, 0x0, 4096},
{"GD25Q16B", 	JEDEC_ID_GIGADEV, 0x40, 0x15, 0x20, FLASH_CMB(2),   FLASH_WM_PAGE_256B, 0x0, 0x0, 4096},
{"S25FL216K", 	JEDEC_ID_CYPRESS, 0x40, 0x15, 0x20, FLASH_CMB(2),   FLASH_WM_PAGE_256B, 0x0, 0x0, 4096},
{"S25FL032P", 	JEDEC_ID_CYPRESS, 0x02, 0x15, 0x20, FLASH_CMB(4),   FLASH_WM_PAGE_256B, 0x0, 0x0, 4096},
{"A25L080", 	JEDEC_ID_AMIC, 	  0x30, 0x14, 0x20, FLASH_CMB(1),   FLASH_WM_PAGE_256B, 0x0, 0x0, 4096},
{"F25L004", 	JEDEC_ID_ESMT, 	  0x20, 0x13, 0x20, FLASH_CKB(512), FLASH_WM_BYTE_AAI,	0x0, 0x0, 4096},
{"PCT25VF016B", JEDEC_ID_SST, 	  0x25, 0x41, 0x20, FLASH_CMB(2),   FLASH_WM_BYTE_AAI,	0x0, 0x0, 4096},
};

static struct spiflash_mf flash_mf_table[] = {
    {"Cypress",    JEDEC_ID_CYPRESS},
    {"Fujitsu",    JEDEC_ID_FUJITSU},
    {"EON",        JEDEC_ID_EON},
    {"Atmel",      JEDEC_ID_ATMEL},
    {"Micron",     JEDEC_ID_MICRON},
    {"AMIC",       JEDEC_ID_AMIC},
    {"Sanyo",      JEDEC_ID_SANYO},
    {"Intel",      JEDEC_ID_INTEL},
    {"ESMT",       JEDEC_ID_ESMT},
    {"Fudan",      JEDEC_ID_FUDAN},
    {"Hyundai",    JEDEC_ID_HYUNDAI},
    {"SST",        JEDEC_ID_SST},
    {"GigaDevice", JEDEC_ID_GIGADEV},
    {"ISSI",       JEDEC_ID_ISSI},
    {"Winbond",    JEDEC_ID_WINBOND},
    {"Macronix",   JEDEC_ID_MACRONIX},
};



static void make_address_byte_array(struct qe_spiflash_device *flash, 
											     qe_u32 				addr, 
											     qe_u8                *array)
{
    qe_u8 len, i;

    len = flash->address_4byte ? 4 : 3;

    for (i=0; i<len; i++) {
        array[i] = (addr >> ((len - (i + 1)) * 8)) & 0xFF;
    }
}

static qe_err_t spiflash_read_jedec_id(struct qe_spiflash_device *flash)
{
	qe_err_t ret;
	qe_u8 cmd[1], data[3];

	cmd[0] = QE_SPIFLASH_CMD_JEDEC_ID;
	ret = qe_spi_send_recv(flash->spi_device, cmd, 1, data, 3);
	if (ret != qe_ok) {
		dev_trace("read JEDEC ID error:%d", ret);
		return ret;
	}

	flash->chip.mf_id = data[0];
	flash->chip.mt_id = data[1];
	flash->chip.capacity_id = data[2];

	dev_trace("Manufacturer ID: 0x%2X", flash->chip.mf_id);
	dev_trace("Memory type ID: 0x%2X",  flash->chip.mt_id);
	dev_trace("Capacity ID: 0x%2X",     flash->chip.capacity_id);

	return ret;
}

static qe_err_t spiflash_read_status(struct qe_spiflash_device *flash, qe_u8 *status)
{
	qe_err_t ret;
	qe_u8 cmd = QE_SPIFLASH_CMD_READ_SR;
	ret = qe_spi_send_recv(flash->spi_device, &cmd, 1, status, 1);
    //dev_debug("ret:%d", ret);
	if (ret != qe_ok) {
		return qe_err_send;
	}

	return qe_ok;
}

static qe_err_t spiflash_wait_busy(struct qe_spiflash_device *flash)
{
	qe_err_t ret;
	qe_u8 status = 0x0;

    //dev_debug("wait busy");

	while (1) {
		ret = spiflash_read_status(flash, &status);
        //dev_debug("status:0x%x ret:%d", status, ret);
		if ((ret==qe_ok) && !(status & QE_SPIFLASH_SR_BUSY)) {
			break;
		}
	}

	if ((ret!=qe_ok) || (status & QE_SPIFLASH_SR_BUSY)) {
		dev_error("wait busy");
	}

    //dev_debug("wait busy ok");

	return ret;
}

static qe_err_t spiflash_write_enable(struct qe_spiflash_device *flash, qe_bool_t en)
{
	qe_size_t n;
	qe_err_t ret = qe_ok;
	qe_u8 cmd, sr;

	qe_assert(flash);

	if (en) {
		cmd = QE_SPIFLASH_CMD_WRITE_ENABLE;
	} else {
		cmd = QE_SPIFLASH_CMD_WRITE_DISABLE;
	}

	n = qe_spi_transfer(flash->spi_device, &cmd, QE_NULL, 1);
	if (n) {
		ret = spiflash_read_status(flash, &sr);
	}

	if (ret == qe_ok) {
		if (en && !(sr & QE_SPIFLASH_SR_WEL)) {
			//dev_error("write enable status error");
			return qe_err_write;
		} else if (!en && (sr & QE_SPIFLASH_SR_WEL)) {
			return qe_err_write;
		}
	}

	return ret;
}

static qe_err_t spiflash_reset(struct qe_spiflash_device *flash)
{
	qe_size_t n;
	qe_u8 cmd[1];

	cmd[0] = QE_SPIFLASH_CMD_ENABLE_RESET;
	n = qe_spi_transfer(flash->spi_device, cmd, QE_NULL, 1);
	if (n <= 0) {
		dev_error("flash enable reset failed");
		return qe_err_send;
	}

    dev_trace("enable reset");
	spiflash_wait_busy(flash);
    dev_trace("wait busy");

	cmd[0] = QE_SPIFLASH_CMD_RESET;
	n = qe_spi_transfer(flash->spi_device, cmd, QE_NULL, 1);
	if (n <= 0) {
		dev_error("flash reset failed");
		return qe_err_send;
	}

    dev_trace("reset");
	spiflash_wait_busy(flash);
    dev_trace("wait busy");

	return qe_ok;
}

/**
 * This function will write flash data for write 1 to 256 bytes per page
 * 
 * @flash      : the flash device
 * @addr  	   : write start address
 * @size       : write size
 * @write_gran : write granularity bytes, ony 1 or 256
 *
 * @return write length
 */
qe_err_t spiflash_page256_or_1byte_write(struct qe_spiflash_device *flash,
														qe_u32 			   addr,
														qe_size_t				   size,
														qe_u16			       write_gran,
														const qe_u8  		  *buffer)
{
	qe_size_t n;
	qe_err_t ret = qe_ok;
	static qe_u8 cmd[5 + 256];
	qe_u8 cmd_size;
	qe_size_t write_size;

	qe_assert(flash);
	qe_assert((write_gran==1) || (write_gran==256));

	/* check flash address bound */
	if ((addr+size) > flash->chip.capacity) {
		dev_error("flash address out of bound");
		return qe_err_oor;
	}

	/* loop write operate. write unit is write granularity */
	while (size) {

		/* write enable */		
		ret = spiflash_write_enable(flash, qe_true);
		if (ret != qe_ok) {
			goto __exit;
		}

		cmd[0] = QE_SPIFLASH_CMD_PAGE_PROGRAM;
		make_address_byte_array(flash, addr, &cmd[1]);
		cmd_size = flash->address_4byte ? 5 : 4;

		/* make write align and calculate next write address */
		if (addr % write_gran != 0) {
			if (size > write_gran - (addr % write_gran)) {
				write_size = write_gran - (addr % write_gran);
			} else {
				write_size = size;
			}
		} else {
			if (size > write_gran) {
            	write_size = write_gran;
        	} else {
            	write_size = size;
        	}
		}

		size -= write_size;
        addr += write_size;

		qe_memcpy(&cmd[cmd_size], (void *)buffer, write_size);
		dev_trace("write size:%d", write_size);
		n = qe_spi_transfer(flash->spi_device, cmd, QE_NULL, cmd_size+write_size);
		if (n == 0) {
			dev_error("write spi error");
			goto __exit;
		}

		/* wait busy */
		ret = spiflash_wait_busy(flash);
		if (ret != qe_ok) {
			dev_error("wait busy");
			goto __exit;
		}

		buffer += write_size;
	}

__exit:
	spiflash_write_enable(flash, qe_false);
	return ret;
}

/**
 * This function will write flash data for auto address increment mode
 *
 * @note: If address is odd number, it will place on 0xFF before the start of data for protect
 *        the old data. If the latest remain size is 1, it will append one 0xFF at the end of 
 *        data for protect the old data.
 *
 * @flash  : the flash device
 * @addr   : write start address
 * @size   : write size
 * @data   : write data
 *
 * @return result
 */
static qe_err_t spiflash_aai_write(struct qe_spiflash_device *flash,
										  qe_u32                addr,
										  qe_size_t                  size,
										  const qe_u8          *buffer)
{
	qe_size_t n;
	qe_err_t ret;
	qe_u8 cmd[8], cmd_size;
	qe_bool_t first_write = qe_true;

	qe_assert(flash);
	qe_assert(flash->spi_device);

	/* check flash address bound */
	if ((addr+size) > flash->chip.capacity) {
		dev_error("address out of bound");
		return qe_err_oor;
	}

	/**
	 * The address must be even for AAI write mode. So it must write 
     * one byte first when address is odd. 
     */
    if (addr % 2) {
		ret = spiflash_page256_or_1byte_write(flash, addr++, 1, 1, buffer++);
		if (ret != qe_ok) {
			goto __exit;
		}
		size--;
	}

	/* set the flash write enable */
	ret = spiflash_write_enable(flash, qe_true);
	if (ret != qe_ok) {
		goto __exit;
	}

	/* loop write operate. */
	cmd[0] = QE_SPIFLASH_CMD_WORD_PROGRAM;
	while (size >= 2) {
		if (first_write) {
			make_address_byte_array(flash, addr, &cmd[1]);
			cmd_size = flash->address_4byte ? 5 : 4;
			cmd[cmd_size] = *buffer;
			cmd[cmd_size+1] = *(buffer+1);
			first_write = qe_false;
		} else {
            cmd_size = 1;
            cmd[1] = *buffer;
            cmd[2] = *(buffer + 1);			
		}

		n = qe_spi_transfer(flash->spi_device, &cmd, QE_NULL,  cmd_size+2);
		if (!n) {
			dev_error("write spi xfer error");
			goto __exit;
		}

		ret = spiflash_wait_busy(flash);
		if (ret != qe_ok) {
			dev_error("wait busy err:%d", ret);
			goto __exit;
		}

		size -= 2;
        addr += 2;
        buffer += 2;
	}

	/* set the flash write disable for exit AAI mode */
	ret = spiflash_write_enable(flash, qe_false);

	/* write last one byte data when origin write size is odd */
	if (ret == qe_ok && size == 1) {
		ret = spiflash_page256_or_1byte_write(flash, addr, 1, 1, buffer);
	}

__exit:
	if (ret != qe_ok) {
		spiflash_write_enable(flash, qe_false);
	}

	return ret;
}


/**
 * This function enable or disable spiflash 4-Byte address mode
 *
 * @note : The 4-Byte address mode just support for the flash capacity which is large than 16MB(256Mb)
 *
 * @flash : the flash device
 * @en	  : enable if true, else disable
 *
 * @return result
 */
static qe_err_t spiflash_set_4byte_address_mode(struct qe_spiflash_device *flash,
																 qe_bool_t 					en)
{
	qe_size_t n;
	qe_err_t ret;
	qe_u8 cmd;

	qe_assert(flash);

	/* write enable */
	ret = spiflash_write_enable(flash, qe_true);
	if (ret != qe_ok) {
        dev_error("flash write enable err:%d", ret);
		return ret;
	}

	if (en) {
		cmd = QE_SPIFLASH_CMD_ENTER_4B_ADDRESS_MODE;
	} else {
		cmd = QE_SPIFLASH_CMD_EXIT_4B_ADDRESS_MODE;
	}

	n = qe_spi_transfer(flash->spi_device, &cmd, QE_NULL, 1);
	if (n > 0) {
		flash->address_4byte = en ? qe_true : qe_false;
		dev_trace("%s 4-Byte address mode", en?"enter":"exit");
	} else {
		ret = qe_err_send;
		dev_error("%s 4-Byte address mode error", en?"enter":"exit");
	}

	return ret;
}


/**
 * This function will read data from spiflash.
 *
 * @flash  : the spiflash device
 * @addr   : read start address
 * @size   : read size
 * @buffer : read data buffer
 *
 * @return read length
 */
qe_size_t qe_spiflash_read(qe_spiflash_t  flash,
								   qe_u32    addr,
								   qe_size_t      size,
								   void          *buffer)
{
	qe_size_t n;
	qe_err_t ret;
	qe_u8 cmd[5], cmd_size;

	qe_assert(flash != QE_NULL);
	qe_assert(flash->spi_device != QE_NULL);

	/* check flash address bound */
	if ((addr+size) > flash->chip.capacity) {
		dev_error("read address out of bound");
		return qe_err_oor;
	}

	ret = spiflash_wait_busy(flash);
	if (ret == qe_ok) {
		cmd[0] = QE_SPIFLASH_CMD_READ_DATA;
		make_address_byte_array(flash, addr, &cmd[1]);
		cmd_size = flash->address_4byte ? 5 : 4;
		n = qe_spi_send_recv(flash->spi_device, cmd, cmd_size, buffer, size);
	} else {
		return 0;
	}

	return n;
}


/**
 * This function will write data to spiflash.
 * 
 * @flash  : the spiflash device
 * @addr   : write start address
 * @size   : write size
 * @buffer : write data buffer
 * 
 * @return write length
 */
qe_err_t qe_spiflash_write(qe_spiflash_t  flash,
								   qe_u32    addr,
								   qe_size_t      size,
								   const void    *buffer)
{
	qe_err_t ret = qe_ok;

    /*
	ret = qe_spiflash_erase(flash, addr, size);
	if (ret != qe_ok) {
		dev_error("erase error:%d", ret);
		return ret;
	}*/

	if (flash->chip.write_mode & FLASH_WM_PAGE_256B) {
		dev_trace("page256");
		ret = spiflash_page256_or_1byte_write(flash, addr, size, 256, buffer);
	} else if (flash->chip.write_mode & FLASH_WM_AAI) {
		dev_trace("aai write");
		ret = spiflash_aai_write(flash, addr, size, buffer);
	}

	return ret;
}

/*
 * This function will erase all flash data on chip
 *
 * @flash : the flash device
 *
 * @return result
 */
qe_err_t qe_spiflash_chip_erase(qe_spiflash_t flash)
{
	qe_size_t n;
	qe_err_t ret;
	qe_u8 cmd[4];

	qe_assert(flash);
	qe_assert(flash->spi_device);

    dev_trace("chip erase");

	/* set the flash write enable */
	ret = spiflash_write_enable(flash, qe_true);
	if (ret != qe_ok) {
		goto __exit;
	}

	cmd[0] = QE_SPIFLASH_CMD_ERASE_CHIP;
    /* dual-buffer write, like AT45DB series flash chip erase operate is different for other flash */
	if (flash->chip.write_mode & FLASH_WM_DUAL_BUFFER) {
		cmd[1] = 0x94;
        cmd[2] = 0x80;
        cmd[3] = 0x9A;
		n = qe_spi_transfer(flash->spi_device, &cmd, QE_NULL, 4);
	} else {
		n = qe_spi_transfer(flash->spi_device, &cmd, QE_NULL, 1);
	}

	if (!n) {
		dev_error("chip erase spi xfer err");
		goto __exit;
	}

	spiflash_wait_busy(flash);

__exit:
	spiflash_write_enable(flash, qe_false);
	return ret;
}

/**
 * This function will erase align by erase granularity.
 * 
 * @flash : the spiflash device
 * @addr  : erase start address
 * @size  : erase size
 *
 * @return result
 */
qe_err_t qe_spiflash_erase(qe_spiflash_t flash,
								   qe_u32   addr,
								   qe_size_t     size)
{
	qe_err_t ret = qe_ok;
	qe_u8 cmd[5], cmd_size;
	qe_size_t curr_erase_size, n;
	
	qe_assert(flash);
	qe_assert(flash->spi_device);

	/* check flash address bound */
	if ((addr+size) > flash->chip.capacity) {
		dev_error("read address out of bound");
		return qe_err_oor;
	}

	if (size == flash->chip.capacity) {
		return qe_spiflash_chip_erase(flash);
	}

	curr_erase_size = flash->chip.erase_gran;
	dev_trace("curr_erase_size:%d 4byte:%d cmd:0x%x", 
        curr_erase_size, flash->address_4byte, flash->chip.erase_gran_cmd);

	/* loop erase operate. erase unit is erase granularity */
	while (size) {

		/* write enable */
		ret = spiflash_write_enable(flash, qe_true);
		if (ret != qe_ok) {
			dev_error("write enable error:%d", ret);
			goto __exit;
		}

		/* do gran erase */
		cmd[0] = flash->chip.erase_gran_cmd;
		make_address_byte_array(flash, addr, &cmd[1]);
		cmd_size = flash->address_4byte ? 5 : 4;
		n = qe_spi_transfer(flash->spi_device, cmd, QE_NULL, cmd_size);
		if (n == 0) {
			dev_error("erase spi error");
			goto __exit;
		}

		/* wait busy */
		ret = spiflash_wait_busy(flash);
		if (ret != qe_ok) {
			dev_error("wait busy");
			goto __exit;
		}

		/* make erase align and calculate next erase address */
		if (addr % curr_erase_size) {
			if (size > curr_erase_size - (addr % curr_erase_size)) {
				size -= curr_erase_size - (addr % curr_erase_size);
				addr += curr_erase_size - (addr % curr_erase_size);
			} else {
				goto __exit;
			}
		} else {
			if (size > curr_erase_size) {
				size -= curr_erase_size;
				addr += curr_erase_size;
			} else {
				goto __exit;
			}
		}
	}

__exit:
	spiflash_write_enable(flash, qe_false);
	return ret;
}

static qe_size_t spiflash_read(qe_device_t  dev,
								    qe_off_t      pos,
								    void         *buffer,
									qe_size_t     size)
{
	qe_size_t phy_size;
	qe_u32 phy_addr;
	struct qe_spiflash_device *flash;

	flash = (struct qe_spiflash_device *)dev;

	qe_assert(dev);
	qe_assert(flash);
	qe_assert(flash->spi_device);

	/* change the block device's logic address to physical address */
	phy_addr = pos * flash->sector_size;
	phy_size = size * flash->sector_size;
	return qe_spiflash_read(flash, phy_addr, phy_size, buffer);
}

static qe_size_t spiflash_write(qe_device_t  dev,
									 qe_off_t      pos,
									 const void   *buffer,
									 qe_size_t     size)
{
	qe_size_t phy_size;
	qe_u32 phy_addr;
	struct qe_spiflash_device *flash;

	flash = (struct qe_spiflash_device *)dev;
	
	qe_assert(dev);
	qe_assert(flash);
	qe_assert(flash->spi_device);

	/* change the block device's logic address to physical address */
	phy_addr = pos * flash->sector_size;
	phy_size = size * flash->sector_size;
	return qe_spiflash_write(flash, phy_addr, phy_size, buffer);
}

static qe_err_t block_erase(struct qe_spiflash_device *flash, struct qe_spiflash_erase_msg *msg)
{
	qe_err_t ret = qe_ok;
	qe_u8 cmd[5], cmd_size;
	qe_size_t curr_erase_size, n;
    qe_u32 addr, size;
	
	qe_assert(flash);
	qe_assert(flash->spi_device);

    addr = msg->addr;
    size = msg->size;

	/* check flash address bound */
	if ((addr+size) > flash->chip.capacity) {
		dev_error("read address out of bound");
		return qe_err_oor;
	}

	if (size == flash->chip.capacity) {
		return qe_spiflash_chip_erase(flash);
	}

	curr_erase_size = msg->erase_gran;
	dev_trace("curr_erase_size:%d", curr_erase_size);

	/* loop erase operate. erase unit is erase granularity */
	while (size) {

		/* write enable */
		ret = spiflash_write_enable(flash, qe_true);
		if (ret != qe_ok) {
			dev_error("write enable error:%d", ret);
			goto __exit;
		}

		/* do gran erase */
		cmd[0] = msg->erase_cmd;
		make_address_byte_array(flash, addr, &cmd[1]);
		cmd_size = flash->address_4byte ? 5 : 4;
		n = qe_spi_transfer(flash->spi_device, cmd, QE_NULL, cmd_size);
		if (n == 0) {
			dev_error("erase spi error");
			goto __exit;
		}

		/* wait busy */
		ret = spiflash_wait_busy(flash);
		if (ret != qe_ok) {
			dev_error("wait busy");
			goto __exit;
		}

		/* make erase align and calculate next erase address */
		if (addr % curr_erase_size) {
			if (size > curr_erase_size - (addr % curr_erase_size)) {
				size -= curr_erase_size - (addr % curr_erase_size);
				addr += curr_erase_size - (addr % curr_erase_size);
			} else {
				goto __exit;
			}
		} else {
			if (size > curr_erase_size) {
				size -= curr_erase_size;
				addr += curr_erase_size;
			} else {
				goto __exit;
			}
		}
	}

__exit:
	spiflash_write_enable(flash, qe_false);
	return ret;    
}

static qe_err_t spiflash_control(qe_device_t dev, int cmd, void *args)
{
    struct qe_spiflash_device *flash;
    
    flash = (struct qe_spiflash_device *)dev;

	qe_assert(dev);
	qe_assert(flash);
	qe_assert(flash->spi_device);

    switch(cmd) {

    case SPIFLASH_CTRL_GET_CHIPINFO:
        qe_memcpy(args, &flash->chip, sizeof(struct qe_spiflash_chip));
        break;

    case SPIFLASH_CTRL_BLOCK_ERASE:
        block_erase(flash, (struct qe_spiflash_erase_msg *)args);
        break;

    default:
        break;
    }

    return qe_ok;
}

static const struct qe_device_ops spiflash_ops = {
	QE_NULL,
	QE_NULL,
	QE_NULL,
	spiflash_read,
	spiflash_write,
	spiflash_control,
};

qe_spiflash_t qe_spiflash_find(const char *name)
{
	struct qe_spiflash_device *flash;

	flash = (struct qe_spiflash_device *)qe_device_find(name);
	if (flash == QE_NULL || flash->parent.class != QE_DevClass_Block) {
		dev_error("flash device %s not found", name);
		return QE_NULL;
	}

	return flash;
}

qe_spiflash_t qe_spiflash_probe(const char *flash_dev_name, const char *spi_dev_name, 
    struct qe_spiflash_chip *chipinfo)
{
	int i;
	qe_err_t ret;
	qe_bool_t find = qe_false;
	struct qe_spi_device *spi;
	struct qe_spiflash_chip *chip;
	struct qe_spiflash_device *flash;
	char *flash_mf_name = QE_NULL;
	
	qe_assert(flash_dev_name != QE_NULL);
	qe_assert(spi_dev_name != QE_NULL);

	spi = (struct qe_spi_device *)qe_device_find(spi_dev_name);
	if (!spi) {
		dev_error("spi device %s not find", spi_dev_name);
		return QE_NULL;
	}

	flash = qe_malloc(sizeof(struct qe_spiflash_device));
	if (flash == QE_NULL) {
		return QE_NULL;
	}

	qe_memset(flash, 0x0, sizeof(struct qe_spiflash_device));
	flash->spi_device = spi;

	ret = spiflash_read_jedec_id(flash);
	if (ret != qe_ok)
		goto __error;

    if (chipinfo) {
        if ((chipinfo->mf_id == flash->chip.mf_id) &&
            (chipinfo->mt_id == flash->chip.mt_id) &&
            (chipinfo->capacity_id == flash->chip.capacity_id)) {
			flash->chip.name = chipinfo->name;
			flash->chip.capacity = chipinfo->capacity;
			flash->chip.write_mode = chipinfo->write_mode;
			flash->chip.erase_gran = chipinfo->erase_gran;
			flash->chip.erase_gran_cmd = chipinfo->erase_gran_cmd;
            flash->chip.flag = chipinfo->flag;
			find = qe_true;
        }
    }

    if (find) {
        goto __chip_matched;
    }

	for (i=0; i<qe_array_size(flash_chip_table); i++) {
		chip = &flash_chip_table[i];
        /*
        dev_trace("mf_id:%d %d mt_id:%d %d name:%s", 
            chip->mf_id, flash->chip.mf_id, 
            chip->mt_id, flash->chip.mt_id,
            chip->name);*/
		if ((chip->mf_id == flash->chip.mf_id) &&
			(chip->mt_id == flash->chip.mt_id) &&
			(chip->capacity_id == flash->chip.capacity_id)) {
			flash->chip.name = chip->name;
			flash->chip.capacity = chip->capacity;
			flash->chip.write_mode = chip->write_mode;
			flash->chip.erase_gran = chip->erase_gran;
			flash->chip.erase_gran_cmd = chip->erase_gran_cmd;
            flash->chip.flag = chipinfo->flag;
			find = qe_true;
            /*
                dev_trace("write_mode:%d erase_gran:%d erase_gran_cmd:%d",
                    flash->chip.write_mode, flash->chip.erase_gran,
                    flash->chip.erase_gran_cmd);*/
			break;
		}
	}

	if (!find) {
		dev_warn("flash device %s not support", flash_dev_name);
		goto __error;
	}


__chip_matched:

	/* find the manufacturer information */
	for (i=0; i<sizeof(flash_mf_table); i++) {
		if (flash_mf_table[i].id == flash->chip.mf_id) {
			flash_mf_name = flash_mf_table[i].name;
			break;
		}
	}

	/* print manufacturer and flash chip name */
	if (flash_mf_name && flash->chip.name) {
		dev_info("%s %s %d %ld bytes (%dMB)",
            flash_mf_name, flash->chip.name, 
            flash->chip.erase_gran,
            flash->chip.capacity, 
            flash->chip.capacity/(1024*1024));
	}

	/* reset flash device */
    if (flash->chip.flag & FLASH_F_RESET) {
        ret = spiflash_reset(flash);
        if (ret != qe_ok) {
            goto __error;
        }
    }

    if (flash->chip.flag & FLASH_F_4BYTE) {
        /* if the flash is large than 32MB(256Mb) then enter 4-Byte address mode */
        if (flash->chip.capacity > (1L << 24)) {
            ret = spiflash_set_4byte_address_mode(flash, qe_true);
            if (ret != qe_ok) {
                goto __error;
            }
        } else {
            flash->address_4byte = qe_false;
        }
    }

	flash->nr_sectors  = flash->chip.capacity / flash->chip.erase_gran;
	flash->sector_size = flash->chip.erase_gran;
	flash->block_size  = flash->chip.erase_gran;

	flash->parent.class = QE_DevClass_Block;
	flash->parent.ops = &spiflash_ops;
	qe_device_register(&(flash->parent), flash_dev_name, QE_DEV_F_RDWR|QE_DEV_F_STANDALONE);
	dev_debug("probe spiflash %s by %s success", flash_dev_name, spi_dev_name);
	return flash;

__error:
	if (flash)
		qe_free(flash);
	return QE_NULL;
}



