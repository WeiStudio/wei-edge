


#include "qe_arch.h"


void qe_weak qe_interrupt_disable(void)
{
}

void qe_weak qe_interrupt_enable(void)
{
}

qe_err_t qe_weak qe_irq_enable(int irq)
{
    (void)irq;
    return qe_ok;
}

void qe_weak qe_irq_disable(int irq)
{
    (void)irq;
}

qe_err_t qe_weak qe_irq_register(int irq, void (*handuler)(void *), void *ref)
{
    (void)irq;
    (void)handuler;
    (void)ref;
    return qe_ok;
}

void qe_weak qe_irq_unregister(int irq)
{
    (void)irq;
}

void qe_weak qe_sleep(qe_u32 sec)
{
    (void)sec;
}
void qe_weak qe_msleep(qe_u32 sec)
{
    (void)sec;
}
void qe_weak qe_usleep(qe_u64 sec)
{
    (void)sec;
}

qe_u32 qe_weak qe_time(void)
{
    return 0;
}

qe_u32 qe_weak qe_time_ms(void)
{
    return 0;
}

qe_u64 qe_weak qe_time_us(void)
{
    return 0;
}