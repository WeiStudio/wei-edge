


#include "qe_core.h"
#include "uart.h"



#define AXI_UARTLITE_SR_PARITY_ERROR		0x80
#define AXI_UARTLITE_SR_FRAMING_ERROR	    0x40
#define AXI_UARTLITE_SR_OVERRUN_ERROR	    0x20
#define AXI_UARTLITE_SR_INTR_ENABLED		0x10	/* interrupt enabled */
#define AXI_UARTLITE_SR_TX_FIFO_FULL		0x08	/* transmit FIFO full */
#define AXI_UARTLITE_SR_TX_FIFO_EMPTY	    0x04	/* transmit FIFO empty */
#define AXI_UARTLITE_SR_RX_FIFO_FULL		0x02	/* receive FIFO full */
#define AXI_UARTLITE_SR_RX_FIFO_VALID_DATA	0x01	/* data in receive FIFO */

struct axi_uartlite_regs {
    qe_u32 rxfifo;
    qe_u32 txfifo;
    qe_u32 sr;
    qe_u32 cr;
};

static qe_bool_t uart_txfifo_full(struct axi_uartlite_regs *regs)
{
    return (regs->sr & AXI_UARTLITE_SR_TX_FIFO_FULL);
}

static qe_bool_t uart_rxfifo_empty(struct axi_uartlite_regs *regs)
{
    return (!(regs->sr & AXI_UARTLITE_SR_RX_FIFO_VALID_DATA));
}

void qe_arch_uart_outc(qe_u32 base, char data)
{
    struct axi_uartlite_regs *regs = (struct axi_uartlite_regs *)base;
    while (uart_txfifo_full(regs));
    regs->txfifo = data;
}

qe_u8 qe_arch_uart_inc(qe_u32 base)
{
    struct axi_uartlite_regs *regs = (struct axi_uartlite_regs *)base;
    while (uart_rxfifo_empty(regs));
    return regs->rxfifo;
}