


#ifndef __UART_H__
#define __UART_H__


#include "qe_def.h"


void qe_arch_uart_outc(qe_u32 regs, char data);
qe_u8 qe_arch_uart_inc(qe_u32 base);

#endif /* __UART_H__ */