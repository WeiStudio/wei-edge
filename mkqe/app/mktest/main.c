#include "qe_core.h"
#include "uart.h"



#define CONFIG_HEAP_SIZE        8192
#define CONFIG_LOG_UART_BASE    0x41400000U


static char heap[CONFIG_HEAP_SIZE];


static void log_output(char *buf, int len)
{
	for (int i=0; i<len; i++) {
		qe_arch_uart_outc(CONFIG_LOG_UART_BASE, buf[i]);
	}
}

void outbyte(char c)
{
    qe_arch_uart_outc(CONFIG_LOG_UART_BASE, c);
}

char inbyte(void)
{
    return qe_arch_uart_inc(CONFIG_LOG_UART_BASE);
}

static void assert_hook(const char *ex, const char *func, qe_size_t line)
{
    sys_error("assert(%s) %s:%d", ex, func, line);
    while (1);
}

static void log_init(void)
{
    qe_u8 level;
    qe_u8 features;

    level = QELOG_DEBUG;
    features = QELOG_F_TIME | QELOG_F_MODEL | QELOG_F_FUNC | QELOG_F_LEVEL | QELOG_F_RING;

    qelog_init(level, features);
    qelog_set_output(log_output);
}

int main(void)
{
    /* memory init */
    qemem_heap_set((void *)heap, CONFIG_HEAP_SIZE);

    /* logger init */
    log_init();

    /* assert hook */
    qe_assert_set_hook(assert_hook);

	/* show version */
    sys_notice("");
    sys_notice("ComBootGen");
    sys_notice("Build time %s %s", __DATE__, __TIME__);
    sys_notice("Copyright (c) 2018-2023 Luster SCBU Team");
    sys_notice("");

	/* initcall */
	qe_initcall();

	/* There will not come */
	sys_error("system exit!!!!!!");
	for( ;; );
}