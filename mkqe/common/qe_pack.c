


#include "qe_def.h"
#include "qe_core.h"


qe_u8 qe_checksum_u8(char *buf, unsigned int len)
{
    int i;
    qe_u8 sum = 0;

    for (i=0; i<len; i++) {
        sum += buf[i];
    }

    return sum;
}

qe_u32 qe_checksum_u32(char *buf, unsigned int len)
{
    int i;
    qe_u32 sum = 0;

    for (i=0; i<len; i++) {
        sum += buf[i];
    }

    return sum;
}