
#include "qe_def.h"
#include "qe_log.h"
#include "qe_service.h"



static int call_start(void)
{
	return 0;
}
QE_EXPORT(call_start, "0");

static int call_end(void)
{
    return 0;
}
QE_EXPORT(call_end, "7.end");

void qe_initcall(void)
{
	qe_initcall_t *pfn;

	for (pfn = &__qe_initcall_call_start; pfn<&__qe_initcall_call_end; pfn++) {
		(*pfn)();
	}
}
