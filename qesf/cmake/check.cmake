
# Check if data model LLP64
include(CheckTypeSize)
check_type_size("long" LONG_SIZE BUILTIN_TYPES_ONLY)
if(CMAKE_SIZEOF_VOID_P EQUAL 8)
    if(LONG_SIZE EQUAL 4)
        message(STATUS "System data model LLP64 (32-bit long)")
        add_definitions(-DCONFIG_SYS_LLP64)
    endif()
endif()

# Check has python Interpreter
include(FindPython)
#find_package(Python COMPONENTS Interpreter Development)
if(Python_FOUND)
    message(STATUS "Python ${Python_VERSION} found")
else()
    message(STATUS "Python not found")
endif()

# Check has Sphinx
find_program(SPHINX_EXECUTABLE NAMES sphinx-build)
if(SPHINX_EXECUTABLE)
    message(STATUS "Sphinx found")
else()
    message(STATUS "Sphinx not found")
endif()

# Check include files
include(CheckIncludeFile)
if(NOT HAVE_STRING_H)
check_include_file(string.h  HAVE_STRING_H)
endif()
if(NOT HAVE_MALLOC_H)
check_include_file(malloc.h  HAVE_MALLOC_H)
endif()
if(NOT HAVE_STDARG_H)
check_include_file(malloc.h  HAVE_STDARG_H)
endif()
if(NOT HAVE_PTHREAD_H)
check_include_file(pthread.h HAVE_PTHREAD_H)
endif()
if(NOT HAVE_TIME_H)
check_include_file(time.h    HAVE_TIME_H)
endif()
if(NOT HAVE_DIRECT_H)
check_include_file(dirent.h  HAVE_DIRECT_H)
endif()


# Check symbols
include(CheckSymbolExists)
if(NOT HAVE_VASPRINTF)
check_symbol_exists(vasprintf "stdio.h" HAVE_VASPRINTF)
endif()
if(NOT HAVE_FOPEN)
check_symbol_exists(fopen "stdio.h" HAVE_FOPEN)
endif()
if(NOT HAVE_TIME)
check_symbol_exists(time "time.h" HAVE_TIME)
endif()
if(NOT HAVE_CALLOC)
check_symbol_exists(calloc "malloc.h" HAVE_CALLOC)
endif()
if(NOT HAVE_ZALLOC)
check_symbol_exists(zalloc "malloc.h" HAVE_ZALLOC)
endif()
if(NOT HAVE_REALLOC)
check_symbol_exists(realloc "malloc.h" HAVE_REALLOC)
endif()
if(NOT HAVE_MEMDUP)
check_symbol_exists(memdup "string.h" HAVE_MEMDUP)
endif()


configure_file(
    ${PROJECT_ROOT}/cmake/autocheck.h.in 
    ${AUTOGEN_DIR}/autocheck.h)
message(STATUS "Generated ${AUTOGEN_DIR}/autocheck.h")