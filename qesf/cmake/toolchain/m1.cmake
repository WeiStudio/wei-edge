
set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR cortex-m1)
set(ARCH arm)

# path
set(TOOLCHAIN_PATH "D:/Software/Keil/keil5/ARM/ARMCLANG/bin")

# compilers
set(CMAKE_C_COMPILER   ${TOOLCHAIN_PATH}/armclang.exe)
set(CMAKE_CXX_COMPILER ${TOOLCHAIN_PATH}/armclang.exe)
set(CMAKE_ASM_COMPILER ${TOOLCHAIN_PATH}/armasm.exe)
set(CMAKE_AR           ${TOOLCHAIN_PATH}/armar.exe)

set(CMAKE_OBJCOPY ${TOOLCHAIN_PATH}/fromelf.exe)

# linker
set(TOOLCHAIN_PATH "D:/software/keil/keil5/ARM/ARMCLANG/bin")

set(CROSS_COMPILE "clang")

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fshort-enums -fshort-wchar")

# compilers
set(CMAKE_C_COMPILER   ${TOOLCHAIN_PATH}/armclang.exe)
set(CMAKE_ASM_COMPILER ${TOOLCHAIN_PATH}/armasm.exe)
set(CMAKE_CXX_COMPILER ${TOOLCHAIN_PATH}/armclang.exe)

set(CMAKE_C_LINK_EXECUTABLE ${TOOLCHAIN_PATH}/armlink.exe)
set(CMAKE_ASM_LINK_EXECUTABLE ${TOOLCHAIN_PATH}/armlink.exe)
set(CMAKE_CXX_LINK_EXECUTABLE ${TOOLCHAIN_PATH}/armlink.exe)

if ("${CMAKE_C_COMPILER_ID}" STREQUAL "ARMCC")
    set(ARMCC 1)
endif()

if ("${CMAKE_C_COMPILER_ID}" STREQUAL "ARMClang")
    set(ARMClang 1)
endif()

if (ARMCC)
endif()

set(CMAKE_C_COMPILER_WORKS   TRUE)
set(CMAKE_CXX_COMPILER_WORKS TRUE)
set(CMAKE_ASM_COMPILER_WORKS TRUE)

