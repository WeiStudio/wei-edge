# SPDX-License-Identifier: Apache-2.0



# 
# kconfig_import
#
# @prefix  : prefix to match
# @kconfig : kconfig file
# 
# Parse a KConfig fragment (typically with extension .config) and
# introduce all the symbols that are prefixed with 'prefix' into the
# CMake namespace. List all created variable names in the 'keys'
# output variable if present.
# 
# Example:
#   kconfig_import(CONFIG_ .config)
#
function(kconfig_import prefix kconfig_file)
    
    # Parse the lines prefixed with 'prefix' in ${kconfig_file}
    file(
        STRINGS
        ${kconfig_file}
        KCONFIG_LIST
        REGEX "^${prefix}"
        ENCODING "UTF-8"
    )

    foreach(CONFIG ${KCONFIG_LIST})

        # Match the first part, the variable name
        string(REGEX MATCH "[^=]+" CONF_VARIABLE_NAME ${CONFIG})
    
        # Match the second part, variable value
        string(REGEX MATCH "=(.+$)" CONF_VARIABLE_VALUE ${CONFIG})
        # The variable value match we just did include the '=' symbol.
        # To just get the part on the RHS we use match group 1
        set(CONF_VARIABLE_VALUE ${CMAKE_MATCH_1})

        # Is surrounded by quotes
        if("${CONF_VARIABLE_VALUE}" MATCHES "^\"(.*)\"$")
            set(CONF_VARIABLE_VALUE ${CMAKE_MATCH_1})
        endif()

        # Create cmake variable
        set("${CONF_VARIABLE_NAME}" "${CONF_VARIABLE_VALUE}" PARENT_SCOPE)

    endforeach()
endfunction()

function(hconfig_import prefix hconfig_file)

    # Parse the lines prefixed with 'prefix' in ${kconfig_file}
    file(
        STRINGS
        ${hconfig_file}
        KCONFIG_LIST
        REGEX "^#define[ \t]+${prefix}([^ \n]+)[ \t]+(.+)$"
        ENCODING "UTF-8"
    )

    foreach(CONFIG ${KCONFIG_LIST})

        #message(STATUS ${CONFIG})

        string(REGEX REPLACE "^#define[ \t]+${prefix}([^ \n]+)[ \t]+(.+)$" "\\1;\\2" _ ${CONFIG})

        # Match the first part, the variable name
        set(CONF_VARIABLE_NAME "CONFIG_${CMAKE_MATCH_1}")
        set(CONF_VARIABLE_VALUE ${CMAKE_MATCH_2})
        #message(STATUS "${CONF_VARIABLE_NAME}:${CONF_VARIABLE_VALUE}")

        # Is surrounded by quotes
        if("${CONF_VARIABLE_VALUE}" MATCHES "^\"(.*)\"$")
            set(CONF_VARIABLE_VALUE ${CMAKE_MATCH_1})
        endif()

        # Create cmake variable
        set("${CONF_VARIABLE_NAME}" "${CONF_VARIABLE_VALUE}" PARENT_SCOPE)

    endforeach()
endfunction()

#
# add_subdir_if_kconfig
#
# @dir : feature code's directory
#
# Including a kconfig feature directory that have matching
# the feature. The config name int the kconfig must the same
# as dir, like CONFIG_UART and dir uart
#
function(add_subdir_if_kconfig dir)
    string(TOUPPER config_${dir} UPPER_CASE_CONFIG)
    add_subdir_ifdef(${UPPER_CASE_CONFIG} ${dir})
endfunction()


#
# add_subdir_ifdef
#
# @feature : cmake variable that define a feature
# @dir     : feature code's directory
#
# Including a feature code's directory that have matching
# the feature
#
function(add_subdir_ifdef feature dir)
    if(${feature})
        add_subdirectory(${dir})
        set(QELIB_INSTALL_HEADERS ${QELIB_INSTALL_HEADERS} PARENT_SCOPE)
    endif()
endfunction()



#
# QESF Library 
# ================
# 
# qesf_library_*
# 
#
macro(qelib_library name)
    set(QESF_CURRENT_LIBRARY ${name})
    if(CONFIG_BUILD_SHARED)
      add_library(${name} SHARED "")
    else()
      add_library(${name} STATIC "")
    endif()
    set(QELIB_INSTALL_HEADERS "")
    set(QELIB_TEST_LIST)
endmacro()

function(qelib_sources source)
    target_sources(${QESF_CURRENT_LIBRARY} PRIVATE ${source} ${ARGN})
endfunction()

function(qelib_include_directories)
  target_include_directories(${QESF_CURRENT_LIBRARY} PUBLIC ${ARGN})
endfunction()

function(qesf_library_link_libraries item)
  target_link_libraries(${QESF_CURRENT_LIBRARY} PUBLIC ${item} ${ARGN})
endfunction()

function(qesf_library_compile_definitions item)
    target_compile_definitions(${QESF_CURRENT_LIBRARY} PRIVATE ${item} ${ARGN})
endfunction()

function(qesf_library_compile_options item)
    target_compile_options(${QESF_CURRENT_LIBRARY} PRIVATE ${item} ${ARGN})
endfunction()

function(qelib_sources_ifdef feature source)
  if(${feature})
    qelib_sources(${source} ${ARGN})
  endif()
endfunction()

function(qesf_library_sources_if_kconfig item)
  get_filename_component(item_basename ${item} NAME_WE)
  string(TOUPPER CONFIG_${item_basename} UPPER_CASE_CONFIG)
  qelib_sources_ifdef(${UPPER_CASE_CONFIG} ${item})
endfunction()

function(qesf_library_compile_definitions_ifdef feature item)
  if(${feature})
    qesf_library_compile_definitions(${item} ${ARGN})
  endif()
endfunction()

function(qesf_library_compile_options_ifdef feature item)
  if(${feature})
    qesf_library_compile_options(${item} ${ARGN})
  endif()
endfunction()

function(qelib_install_headers)
  list(APPEND QELIB_INSTALL_HEADERS ${ARGN})
  set(QELIB_INSTALL_HEADERS ${QELIB_INSTALL_HEADERS} PARENT_SCOPE)
endfunction()

macro(qelib_install_headers_ifdef cond)
  if(${cond})
    list(APPEND QELIB_INSTALL_HEADERS ${ARGN})
    set(QELIB_INSTALL_HEADERS ${QELIB_INSTALL_HEADERS} PARENT_SCOPE)
  endif()
endmacro()