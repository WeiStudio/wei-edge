# version
configure_file(
    ${PROJECT_ROOT}/cmake/version.h.in
    ${AUTOGEN_DIR}/qe_version.h
    @ONLY)
message(STATUS "Generated ${AUTOGEN_DIR}/qe_version.h")