
set(CMAKE_SYSTEM_NAME Generic)
set(ARCH aarch64)

#set(TOOLCHAIN_PATH "/home/weizhou/opt/gcc/gcc-arm-linux-4.9.4")
#set(TOOLCHAIN_PATH "D:/software-setup/Xilinx/SDK/2018.2/gnu/microblaze/nt/")
set(TOOLCHAIN_PATH "D:/Software/Xilinx/SDK/2018.2/gnu/aarch64/nt/aarch64-none/bin")

#set(CROSS_COMPILE "arm-linux-gnueabi")
#set(CROSS_COMPILE "mb")
set(CROSS_COMPILE "aarch64-none-elf")

# compilers
set(CMAKE_C_COMPILER   ${TOOLCHAIN_PATH}/${CROSS_COMPILE}-gcc.exe)
set(CMAKE_CXX_COMPILER ${TOOLCHAIN_PATH}/${CROSS_COMPILE}-g++.exe)
set(CMAKE_LINKER       ${TOOLCHAIN_PATH}/${CROSS_COMPILE}-ld.exe)

set(CMAKE_C_COMPILE_WORKS   TRUE)
set(CMAKE_CXX_COMPILE_WORKS TRUE)
set(CMAKE_LINKER_WORKS TRUE)

