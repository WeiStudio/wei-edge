
if(HCONFIG_FILE)
    message(STATUS "Config file ${HCONFIG_FILE}")
else()
    set(HCONFIG_FILE ${PROJECT_ROOT}/configs/defconfig.h)
    message(STATUS "Config file ${HCONFIG_FILE}")
endif()
get_filename_component(HCONFIG_FILE_NAME ${HCONFIG_FILE} NAME)

message(STATUS "Copy ${HCONFIG_FILE} to ${AUTOGEN_DIR}/autoconfig.h")
file(COPY ${HCONFIG_FILE} DESTINATION ${AUTOGEN_DIR})
file(RENAME ${AUTOGEN_DIR}/${HCONFIG_FILE_NAME} ${AUTOGEN_DIR}/autoconfig.h)

hconfig_import(CONFIG_ ${AUTOGEN_DIR}/autoconfig.h)
