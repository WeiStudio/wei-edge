# Directory set
set(PROJECT_ROOT ${CMAKE_SOURCE_DIR})
set(BUILD_DIR    ${CMAKE_CURRENT_BINARY_DIR})
get_filename_component(BUILD_NAME ${BUILD_DIR} NAME)
message(STATUS "Build name ${BUILD_NAME}")
set(BOARD_DIR    ${PROJECT_ROOT}/configs)
set(AUTOGEN_DIR  ${BUILD_DIR}/generated)

include(cmake/check.cmake)
include(cmake/function.cmake)
if(Python_FOUND)
#if(0)
    message(STATUS "Using kconfig")
    include(cmake/kconfig.cmake)
else()
message(STATUS "Using hconfig")
    include(cmake/hconfig.cmake)
endif()
include(cmake/version.cmake)