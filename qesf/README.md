# QESF(WIN)
QESF库的windows mingw环境调试工程，主要为了方便在windows环境下进行功能验证

## 环境依赖
需要安装mingw，并安装gnu编译链

## Build

两种构建目标，一种是构建运行在Host上的库和apps，用于测试，另一种是交叉编译，只构建库

在toolchain/w64_toolchain.cmake中指定编译器

```shell
mkdir build
mkdir x64
cd build/x64
cmake ../../ -G "MinGW Makefiles" -D CMAKE_TOOLCHAIN_FILE=../../toolchain/win32_mingw.cmake

mkdir microblaze
cmake ../../ -G "MinGW Makefiles" -D CMAKE_TOOLCHAIN_FILE=../../toolchain/microblaze.cmake
```

## 单元测试
apps/unit-test路径下为命令行方式的单元测试程序，在utest.c中编写测试函数，并注册命令参数。每个测试单元以一条命令的形式存在

运行utest.exe并显示usage

```shell
cd build
.\apps\unit-test\utest.exe --help

utest <cmd> <opt>
  -h,?,--help                print help information
  --loglevel                 set log level(0~5)
  --fibonacci <num>          do fibonacci with param
  --stringbuilder            string builder test
  --ringbuffer               ring buffer test
  --ringqueue                ring queue test
  --minpq                    minpq test
  --queue                    queue test
  --bitmap                   bitmap test
  --gbuf                     gbuf test
  --gbufpool                 gbuf pool test
  --bpc-gen-seq-4k <file>    generate bpc SEQ4K table
  --bpc-gen-seq-v2 <file>    generate bpc SEQ4K table
  --bpc-gen-bmp-ca <file>    generate bpc SEQ4K table
  --bpc-gen-bmp-ra <file>    generate bpc SEQ4K table
  --bpc-parse-seq-4k <file>  parse bpc SEQ4K table
  --bpc-parse-seq-v2 <file>  parse bpc SEQV2 table
  --bpc-parse-bmp-ca <file>  parse bpc BMPCA table
  --bpc-parse-bmp-ra <file>  parse bpc BMPRA table
  --bpc-export-seq-4k <file> parse and export bpc SEQ4K table
  --bpc-export-seq-v2 <file> parse and export bpc SEQV2 table
  --bpc-export-bmp-ca <file> parse and export bpc BMPCA table
  --bpc-export-bmp-ra <file> parse and export bpc BMPRA table
  --bpc-frame-detect <file>  parse bpc table and detect point from random and add to xtab
```