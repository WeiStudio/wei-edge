/*
 * Copyright (C) 2021-2023, WeiStudio
 *
 * License: Apache-2.0
 *
 * Change Logs:
 * Date			Author		    Note
 * 2021-07-20	WeiStudio		the first version
 * 2022-10-25   WeiStudio       add bitmap
 */



#include "qe_service.h"
#include <stdarg.h>



int qe_cpu_endian(void)
{
	union {
		unsigned long int i;
		unsigned char s[4];
	}c;

	c.i = 0x12345678;
	return (0x12 == c.s[0]);
}

#define qe_biglittle_swap16(a)	((((qe_u16)(a) & 0xff00) >> 8) | (((qe_u16)(a) & 0x00ff) << 8))

#define qe_biglittle_swap32(a) 	((((qe_u32)(a) & 0xff000000) >> 24) | \
								 (((qe_u32)(a) & 0x00ff0000) >> 8) | \
								 (((qe_u32)(a) & 0x0000ff00) << 8) | \
								 (((qe_u32)(a) & 0x000000ff) << 24))

unsigned long int qe_htonl(unsigned long int x)
{
	return qe_cpu_endian() ? x : qe_biglittle_swap32(x);
}

unsigned short int qe_htons(unsigned short int x)
{
	return qe_cpu_endian() ? x : qe_biglittle_swap16(x);
}