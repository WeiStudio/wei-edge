


#include "qe_thread.h"



#if defined(__linux__)
#include <sys/types.h>
#include <unistd.h>
int qe_get_pid(void)
{
    return getpid();
}

#elif defined(_WIN32)

#include <Windows.h>
int qe_get_pid(void)
{
    DWORD pid = GetCurrentProcessId();
    return (int)pid;
}
#else

int qe_weak qe_get_pid(void)
{
    return 0;
}

#endif
