


#include "qe_errno.h"



/* global errno */
static volatile int errno;

qe_ret qe_get_errno(void)
{
	return errno;
}

void qe_set_errno(qe_ret e)
{
	errno = e;
}
 
int qe_errno(void)
{
    return errno;
}