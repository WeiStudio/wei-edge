


#include "qe_assert.h"



static qe_assert_hook_t assert_hook = QE_NULL;

/**
 * This function will set a hook to assert.
 *
 * @hook: the hook function
 */
void qe_assert_set_hook(void (*hook)(const char *, const char *, qe_size))
{
	assert_hook = hook;
}

void qe_assert_handler(const char *ex_string, const char *func, qe_size line)
{
	volatile char dummy = 0;
	
	if (assert_hook == QE_NULL) {
		while (dummy == 0);
	} else {
		assert_hook(ex_string, func, line);
	}
}