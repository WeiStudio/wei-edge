/*
 * Copyright (c) 2021-2023, Wei.Studio
 *
 * License: Apache-2.0
 *
 * Change Logs:
 * Date           Author        Notes
 * 2021-07-13     Wei.Studio    the first version
 */



#include "autocheck.h"
#include "autoconfig.h"
#include "qe_assert.h"
#include "qe_memory.h"



#if defined(CONFIG_NANO)
typedef struct
{
	qe_size total_bytes;
	qe_size alloced_bytes;
	void *pool;
	qe_size pool_size;
	qemem_allocator *allocator;
} qemem_context;



#if defined(CONFIG_MEM_BGET)
#include "bget.h"
static void *bgets_malloc(qe_size size)
{
	return bget(size);
}

static void *bgets_calloc(qe_size nitems, qe_size size)
{
	if (!nitems || !size)
		return QE_NULL;
	qe_size alloc = nitems * size;
	void *m = bget(alloc);
	qe_assert(m != QE_NULL);
	qe_memset(m, 0x0, alloc);
	return m;
}

static void *bgets_realloc(void *mem, qe_size size)
{
	if (!mem || !size)
		return QE_NULL;

	void *m = bgetr(mem, size);
	qe_assert(m != QE_NULL);
	return m;
}

static void bgets_free(void *mem)
{
	brel(mem);
}

static void bgets_pool(void *pool, qe_size size)
{
	bpool(pool, size);
}

static void bgets_minfo(qemem_minfo *info)
{
	long alloc, totfree, maxfree, nget, nrel;
	bstats(&alloc, &totfree, &maxfree, &nget, &nrel);
	info->total = alloc + totfree;
	info->used  = alloc;
	info->free  = totfree;
}

static qemem_allocator bgets_allocator = {
	.malloc   = bgets_malloc,
	.calloc   = bgets_calloc,
	.realloc  = bgets_realloc,
	.free     = bgets_free,
	.pool     = bgets_pool,
	.minfo    = bgets_minfo,
	.flags    = QEMEM_POOL,
};
#endif

static qemem_context mm = {
	.alloced_bytes = 0,
	.total_bytes   = 0,
	.pool          = QE_NULL,
	.pool_size     = 0,
#if defined(CONFIG_MEM_BGET)
	.allocator     = &bgets_allocator,
#else
	.allocator     = QE_NULL,
#endif
};

void qemem_set_pool(void *start, qe_size size)
{
	qe_assert(mm.allocator != QE_NULL);
	if (!(mm.allocator->flags & QEMEM_POOL))
		return;
	qe_assert(mm.allocator->pool != QE_NULL);
	mm.allocator->pool(start, size);
	mm.pool = start;
	mm.pool_size = size;
}

void qemem_get_minfo(qemem_minfo *info)
{
	qe_assert(mm.allocator != QE_NULL);
	qe_assert(mm.allocator->minfo != QE_NULL);
	return mm.allocator->minfo(info);
}

void qemem_set_allocator(qemem_allocator *allocator)
{
	mm.allocator = allocator;
}

void *qe_malloc(qe_size size)
{
	qe_assert(mm.allocator != QE_NULL);
	qe_assert(mm.allocator->malloc != QE_NULL);
	if ((mm.allocator->flags & QEMEM_POOL) && !mm.pool) {
		return QE_NULL;
	}
	return mm.allocator->malloc(size);
}

void *qe_calloc(qe_size num, qe_size size)
{
	qe_assert(mm.allocator != QE_NULL);
	qe_assert(mm.allocator->calloc != QE_NULL);
	if ((mm.allocator->flags & QEMEM_POOL) && !mm.pool) {
		return QE_NULL;
	}
	return mm.allocator->calloc(num, size);
}

void *qe_realloc(void *buf, qe_size size)
{
	qe_assert(mm.allocator != QE_NULL);
	qe_assert(mm.allocator->realloc != QE_NULL);
	if ((mm.allocator->flags & QEMEM_POOL) && !mm.pool) {
		return QE_NULL;
	}
	return mm.allocator->realloc(buf, size);
}

void qe_free(void *ptr)
{
	qe_assert(mm.allocator != QE_NULL);
	qe_assert(mm.allocator->free != QE_NULL);
	if ((mm.allocator->flags & QEMEM_POOL) && !mm.pool) {
		return;
	}
	return mm.allocator->free(ptr);
}

/**
 * @brief This function will copy memory content from source address to destination
 * address.
 *
 * @param[in] dst   : the address of destination memory
 * @param[in] src   : the address of source memory
 * @param[in] count : the copied length
 *
 * @return the address of destination memory
 */
void *qe_memcpy(void *dst, void *src, qe_size count)
{
#if (CONFIG_MEMCPY_TINY == 1)
	char *tmp = (char *)dst, *s = (char *)src;
	qe_ubase_t len;
	if (tmp <= s || tmp > (s + count)) {
		while (count--)
			*tmp ++ = *s ++;
	} else {
		for (len = count; len > 0; len --)
			tmp[len - 1] = s[len - 1];
	}
	return dst;
#else
#define UNALIGNED(X, Y) \
    (((qe_base_t)X & (sizeof (qe_base_t) - 1)) | ((qe_base_t)Y & (sizeof (qe_base_t) - 1)))
#define BIGBLOCKSIZE    (sizeof (long) << 2)
#define LITTLEBLOCKSIZE (sizeof (long))
#define TOO_SMALL(LEN)  ((LEN) < BIGBLOCKSIZE)
	char *dst_ptr = (char *)dst;
    char *src_ptr = (char *)src;
    long *aligned_dst;
    long *aligned_src;
    int len = count;

	/* If the size is small, or either SRC or DST is unaligned,
	then punt into the byte copy loop.	This should be rare. */
	if (!TOO_SMALL((unsigned int)len) && !UNALIGNED(src_ptr, dst_ptr)) 
	{
		aligned_dst = (long *)dst_ptr;
		aligned_src = (long *)src_ptr;
		
		/* Copy 4X long words at a time if possible. */
		while ((unsigned int)len >= (unsigned int)BIGBLOCKSIZE)
		{
            *aligned_dst++ = *aligned_src++;
            *aligned_dst++ = *aligned_src++;
            *aligned_dst++ = *aligned_src++;
            *aligned_dst++ = *aligned_src++;
            len -= BIGBLOCKSIZE;
		}

		/* Copy one long word at a time if possible. */
        while ((unsigned int)len >= LITTLEBLOCKSIZE)
        {
            *aligned_dst++ = *aligned_src++;
            len -= LITTLEBLOCKSIZE;
        }

		/* Pick up any residual with a byte copier. */
		dst_ptr = (char *)aligned_dst;
		src_ptr = (char *)aligned_src;
	}

	while (len--)
		*dst_ptr++ = *src_ptr++;

	return dst;
#undef UNALIGNED
#undef BIGBLOCKSIZE
#undef LITTLEBLOCKSIZE
#undef TOO_SMALL
#endif
}

/**
 * @brief Fill a region of memory with the given value
 * @s: Pointer to the start of the area.
 * @c: The byte to fill the area with
 * @count: The size of the area.
 *
 */
void *qe_memset(void *s, int c, qe_size count)
{
#if (CONFIG_MEMSET_TINY == 1)
	char *xs = (char *)s;
	while (count--)
		*xs++ = c;
	return s;
#else
#define LBLOCKSIZE		(sizeof(long))
#define UNALIGNED(x)	((qe_base_t)x & (LBLOCKSIZE-1))
#define TOO_SMALL(len)  ((len) < LBLOCKSIZE)

	unsigned int i;
	char *m = (char *)s;
	unsigned long buffer;
	unsigned long *aligned_addr;
	unsigned int d = c & 0xff;	/* To avoid sign extension, copy C to an unsigned variable */

	if (!TOO_SMALL(count) && !UNALIGNED(s)) {

		/* If we get this far, we know that count is large and s is word-aligned. */
		aligned_addr = (unsigned long *)s;

		/* Store d into each char sized location in buffer so that
         * we can set large blocks quickly.
         */
		if (LBLOCKSIZE == 4) {
			buffer = (d << 8) | d;
			buffer |= (buffer << 16);
		} else {
			buffer = 0;
			for (i = 0; i < LBLOCKSIZE; i ++)
				buffer = (buffer << 8) | d;
		}

		while (count >= LBLOCKSIZE * 4) {
			*aligned_addr++ = buffer;
			*aligned_addr++ = buffer;
			*aligned_addr++ = buffer;
			*aligned_addr++ = buffer;
			count -= 4 * LBLOCKSIZE;
		}

		while (count >= LBLOCKSIZE) {
            *aligned_addr++ = buffer;
            count -= LBLOCKSIZE;
        }

		/* Pick up the remainder with a bytewise loop. */
		m = (char *)aligned_addr;
	}

	while (count--)
		*m++ = (char)d;
	return s;
#undef LBLOCKSIZE
#undef UNALIGNED
#undef TOO_SMALL
#endif
}

/**
 * @brief Compare two areas of memory
 * @cs: One area of memory
 * @ct: Another area of memory
 * @count: The size of the area.
 */
int qe_memcmp(const void *cs,const void *ct, qe_size count)
{
	const unsigned char *su1, *su2;
	int res = 0;

	for( su1 = cs, su2 = ct; 0 < count; ++su1, ++su2, count--)
		if ((res = *su1 - *su2) != 0)
			break;
	return res;
}

/**
 * @brief Copy one area of memory to another
 * @dest: Where to copy to
 * @src: Where to copy from
 * @count: The size of the area.
 */
void *qe_memmove(void *dest, const void *src, qe_size count)
{
	char *tmp, *s;

	if (dest <= src) {
		qe_memcpy(dest, (void *)src, count);
	} else {
		tmp = (char *) dest + count;
		s = (char *) src + count;
		while (count--)
			*--tmp = *--s;
	}

	return dest;
}

/**
 * @brief Find a character in an area of memory.
 * @addr: The memory area
 * @c: The byte to search for
 * @size: The size of the area.
 *
 * returns the address of the first occurrence of @c, or 1 byte past
 * the area if @c is not found
 */
void *qe_memscan(void *addr, int c, qe_size size)
{
	unsigned char * p = (unsigned char *) addr;

	while (size) {
		if (*p == c)
			return (void *) p;
		p++;
		size--;
	}
	return (void *) p;
}
#endif


void *qe_memdup(void *mem, qe_size size)
{
	if (!mem || !size)
		return QE_NULL;

	void *out = qe_malloc(size);

	if (!out)
		return QE_NULL;

	qe_memcpy(out, mem, size);

	return out;
}
