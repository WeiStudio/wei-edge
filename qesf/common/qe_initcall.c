
#include "qe_def.h"
#include "qe_log.h"
#include "qe_initcall.h"
#include "qe_service.h"


#define initcall_debug(...)     qelog_debug(QELOG_DOMAIN_INITCALL, __VA_ARGS__)


static int call_start(void)
{
	return 0;
}
QE_EXPORT(call_start, "0");

static int call_end(void)
{
    return 0;
}
QE_EXPORT(call_end, "7.end");

void qe_initcall(void)
{
	qe_initcall_t *pfn;

	initcall_debug("start:%p end:%p", &__qe_initcall_call_start, 
		&__qe_initcall_call_end);

	for (pfn = &__qe_initcall_call_start; pfn<&__qe_initcall_call_end; pfn++) {
		initcall_debug("call pfn:%p", pfn);
		(*pfn)();
	}
}
