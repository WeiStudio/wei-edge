/*
 * Copyright (C) 2021, WeiStudio
 *
 * License: Apache-2.0
 *
 * Change Logs:
 * Date			Author		Note
 * 2021-07-20	WeiStudio		the first version
 *
 */



#include "qe_spi.h"
#include "qe_assert.h"
#include "qe_log.h"



#define SPI_LOG_DOMAIN    	"spi"
#define spi_debug(...)  	qelog_debug(SPI_LOG_DOMAIN,  __VA_ARGS__)
#define spi_info(...)   	qelog_info(SPI_LOG_DOMAIN,   __VA_ARGS__)
#define spi_notice(...) 	qelog_notice(SPI_LOG_DOMAIN, __VA_ARGS__)
#define spi_warning(...) 	qelog_warning(SPI_LOG_DOMAIN,__VA_ARGS__)
#define spi_error(...)  	qelog_error(SPI_LOG_DOMAIN,  __VA_ARGS__)
#define spi_fatal(...)   	qelog_fatal(SPI_LOG_DOMAIN,  __VA_ARGS__)



qe_size spibus_device_read(qe_dev *dev, qe_offs pos,
	void *buffer, qe_size size)
{
	qe_spi_bus *bus;

	(void)pos;

	bus = (qe_spi_bus *)dev;
	qe_assert(bus != QE_NULL);
	qe_assert(bus->owner != QE_NULL);

	return qe_spi_transfer(bus->owner, QE_NULL, buffer, size);
}

qe_size spibus_device_write(qe_dev *dev,
									   qe_offs     pos,
									   const void  *buffer,
									   qe_size    size)
{
	qe_spi_bus *bus;

	(void)pos;

	bus = (qe_spi_bus *)dev;
	qe_assert(bus != QE_NULL);
	qe_assert(bus->owner != QE_NULL);

	return qe_spi_transfer(bus->owner, buffer, QE_NULL, size);
}

static const qe_dev_ops spi_bus_ops = {
	QE_NULL,
	QE_NULL,
	QE_NULL,
	spibus_device_read,
	spibus_device_write,
	QE_NULL
};

qe_ret qe_spi_bus_register(qe_spi_bus *bus, qe_const_str name,
	const qe_spi_ops *ops)
{
	qe_ret ret;
	qe_dev *dev;
	
	qe_assert(bus != QE_NULL);

	dev = &bus->parent;

	dev->type = QE_DEV_SPI_BUS;
	dev->ops = &spi_bus_ops;
	
	ret = qe_dev_register(dev, name, QE_DEV_F_RDWR);
	if (ret != qe_ok) {
		return ret;
	}
	spi_debug("register spibus:%s", name);
	bus->ops = ops;
	bus->owner = QE_NULL;
	bus->mode = QE_SPI_BUS_SPI;

	return qe_ok;
}

static qe_size spidev_device_read(qe_dev  *dev,
										      qe_offs     pos,
											  void        *buffer,
											  qe_size    size)
{
	struct qe_spi_device *spi;

	(void)pos;

	spi = (struct qe_spi_device *)dev;
	qe_assert(spi != QE_NULL);
	qe_assert(spi->bus != QE_NULL);

	return qe_spi_transfer(spi, QE_NULL, buffer, size);
}

static qe_size spidev_device_write(qe_dev  *dev,
											   qe_offs     pos,
											   const void  *buffer,
											   qe_size    size)
{
	struct qe_spi_device *spi;

	(void)pos;

	spi = (struct qe_spi_device *)dev;
	qe_assert(spi != QE_NULL);
	qe_assert(spi->bus != QE_NULL);

	return qe_spi_transfer(spi, buffer, QE_NULL, size);
}

static const qe_dev_ops spi_device_ops = {
	QE_NULL,
	QE_NULL,
	QE_NULL,
	spidev_device_read,
	spidev_device_write,
	QE_NULL
};

qe_ret qe_spidev_register(qe_spi_dev *spidev, qe_const_str name)
{
	qe_dev *dev;

	qe_assert(spidev != QE_NULL);

	dev = &(spidev->parent);

	/* set device class */
	dev->type = QE_DEV_SPI_DEVICE;

	/* set ops */
	dev->ops = &spi_device_ops;

	return qe_dev_register(dev, name, QE_DEV_F_RDWR);
}

