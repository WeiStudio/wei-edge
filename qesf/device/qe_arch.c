


#include "qe_arch.h"


void qe_weak qe_interrupt_disable(void)
{
}

void qe_weak qe_interrupt_enable(void)
{
}

qe_ret qe_weak qe_irq_enable(int irq)
{
    (void)irq;
    return qe_ok;
}

void qe_weak qe_irq_disable(int irq)
{
    (void)irq;
}

qe_ret qe_weak qe_irq_register(int irq, void (*handuler)(void *), void *ref)
{
    (void)irq;
    (void)handuler;
    (void)ref;
    return qe_ok;
}

void qe_weak qe_irq_unregister(int irq)
{
    (void)irq;
}

void qe_weak qe_irq_set_priority_trigger_type(qe_u32 irq, qe_u8 prior, qe_u8 trigger)
{
    (void)irq;
    (void)prior;
    (void)trigger;
}