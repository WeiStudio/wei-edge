/**
 * Copyright (c) 2021-2023, Wei.Studio
 * 
 * License: MIT
 * 
 * Change Logs:
 * Date             Author      Notes
 * 2023/02/07       weistudio   the first version
 */



#include "qe_list.h"
#include "qe_device.h"
#include "qe_assert.h"
#include "qe_string.h"
#include "qe_log.h"



#define DEV_LOG_DOMAIN    	"dev"
#define dev_debug(...)  	qelog_debug(DEV_LOG_DOMAIN,  __VA_ARGS__)
#define dev_info(...)   	qelog_info(DEV_LOG_DOMAIN,   __VA_ARGS__)
#define dev_notice(...) 	qelog_notice(DEV_LOG_DOMAIN, __VA_ARGS__)
#define dev_warning(...)   	qelog_warning(DEV_LOG_DOMAIN, __VA_ARGS__)
#define dev_error(...)  	qelog_error(DEV_LOG_DOMAIN,  __VA_ARGS__)
#define dev_fatal(...)    	qelog_fatal(DEV_LOG_DOMAIN,    __VA_ARGS__)



qe_list qe_device_list = QE_LIST_OBJECT_INIT(qe_device_list);



/**
 * This function registers a device driver with specified name.
 *
 * @param dev: 	 the pointer of device driver structure
 * @param name:  the device driver's name
 * @param flags: the capabilities flag of device
 * 
 * @return the error code, successfully with qe_ok.
 */
qe_ret qe_dev_register(qe_dev *dev, qe_const_str name,
    qe_u16 flags)
{
	if (dev == QE_NULL) {
		dev_error("dev null");
		return qe_err_param;
	}

	if (qe_dev_find(name) != QE_NULL) {
		dev_warning("%s exist", name);
		return qe_err_exist;
	}

	qe_strncpy(dev->name, name, CONFIG_DEV_NAME_MAX);

	qe_list_append(&dev->list, &qe_device_list);
	
	dev->flag = flags;
	dev->reference = 0;
	dev->open_flag = 0;

	dev_debug("%s register", name);

	return qe_ok;
}

/**
 * This function removes a previously registered device driver
 *
 * @param dev the pointer of device driver structure
 *
 * @return the error code, successfully with qe_ok.
 */
qe_ret qe_dev_unregister(qe_dev *dev)
{
    qe_assert(dev != QE_NULL);

    qe_list_remove(&dev->list);

	dev_debug("%s unregister", dev->name);

    return qe_ok;
}

qe_dev *qe_dev_find(qe_const_str name)
{
	qe_dev *dev;
	struct qe_list_node *node = QE_NULL;


	qe_list_foreach(node, &(qe_device_list)) {
		dev = qe_list_entry(node, qe_dev, list);
		if (qe_strncmp(dev->name, name, CONFIG_DEV_NAME_MAX) == 0) {
			return dev;
		}
	}

	return QE_NULL;
}

qe_ret qe_dev_open(qe_dev *dev, qe_u16 oflag)
{
	qe_ret ret = qe_ok;

	if (!dev) {
		dev_error("dev null");
		return qe_err_param;
	}

	/* if device is not initialized, initialize it. */
	if (!(dev->flag & QE_DEV_F_ACTIVATED)) {
		if (dev->ops->init != QE_NULL) {
			ret = dev->ops->init(dev);
			if (ret != qe_ok) {
				dev_error("init %s err:%d", dev->name, ret);
				return ret;
			}
		}

		dev->flag |= QE_DEV_F_ACTIVATED;
	}

	/* device is a stand alone device and opened */
	if ((dev->flag & QE_DEV_F_STANDALONE) &&
		(dev->open_flag & QE_DEV_OF_OPEN)) {
		dev_error("%s busy", dev->name);
		return qe_err_busy;
	}

	/* call device open interface */
	if (dev->ops->open != QE_NULL) {
		ret = dev->ops->open(dev, oflag);
	} else {
		/* set open flag */
		dev->open_flag = (oflag & QE_DEV_OF_MASK);
	}

	/* set open flag */
	if (ret == qe_ok || ret == qe_err_busy) {
		dev->open_flag |= QE_DEV_OF_OPEN;
		dev->reference++;
	}

	return ret;
}

/**
 * This function will close a device
 *
 * @param dev the pointer of device driver structure
 *
 * @return the result
 */
qe_ret qe_dev_close(qe_dev *dev)
{
	qe_ret ret = qe_ok;

	if (!dev) {
		dev_error("invalid param");
		return qe_err_param;
	}

	if (dev->reference == 0) {
		dev_warning("%s no reference", dev->name);
		return qe_err_common;
	}

	dev->reference--;

	if (dev->reference != 0)
		return qe_ok;

	/* call device close interface */
	if (dev->ops->close != QE_NULL) {
		ret = dev->ops->close(dev);
	}

	/* set open flag */
	if (ret == qe_ok || ret == qe_err_notsupport)
		dev->open_flag = QE_DEV_OF_CLOSE;

	dev_debug("%s close", dev->name);

	return ret;
}


/**
 * This function will read some data from a device.
 *
 * @param dev the pointer of device driver structure
 * @param pos the position of reading
 * @param buffer the data buffer to save read data
 * @param size the size of buffer
 *
 * @return the actually read size on successful, otherwise negative returned.
 *
 */
qe_size qe_dev_read(qe_dev *dev, qe_offs pos, qe_ptr buf, qe_size size)
{
	qe_assert(dev != QE_NULL);

	if (dev->reference == 0) {
		dev_warning("%s no reference", dev->name);
		return 0;
	}

	/* call device read interface */
	if (dev->ops->read != QE_NULL) {
		dev_debug("%s read %d %p %d", dev->name, pos, buf, size);
		return dev->ops->read(dev, pos, buf, size);
	} else {
		dev_warning("%s not support read ops", dev->name);
	}

	return 0;
}

/**
 * This function will write some data to a device.
 *
 * @param dev the pointer of device driver structure
 * @param pos the position of written
 * @param buffer the data buffer to be written to device
 * @param size the size of buffer
 *
 * @return the actually written size on successful, otherwise negative returned.
 *
 */
qe_size qe_dev_write(qe_dev *dev, qe_offs pos, qe_const_ptr buf, qe_size size)
{
	qe_assert(dev != QE_NULL);

	if (dev->reference == 0) {
		dev_warning("%s no reference", dev->name);
		return 0;
	}

	/* call device_write interface */
	if (dev->ops->write!= QE_NULL) {
		dev_debug("%s write %d %p %d", dev->name, pos, buf, size);
		return dev->ops->write(dev, pos, buf, size);
	} else {
		dev_warning("%s not support write ops", dev->name);
	}

	return 0;
}

/**
 * This function will perform a variety of ioctl functions on devices.
 *
 * @param dev the pointer of device driver structure
 * @param cmd the command sent to device
 * @param arg the argument of command
 *
 * @return the result
 */
qe_ret qe_dev_ioctl(qe_dev *dev, qe_int cmd, qe_ptr arg)
{
    qe_assert(dev != QE_NULL);

    /* call device_write interface */
    if (dev->ops->ioctl != QE_NULL) {
		dev_debug("%s ioctl cmd:%d arg:%p", dev->name, cmd, arg);
        return dev->ops->ioctl(dev, cmd, arg);
    } 
	
	dev_warning("%s not support ioctl", dev->name);

    return qe_err_notsupport;
}

/**
 * This function will set the reception indication callback function. This callback function
 * is invoked when this device receives data.
 *
 * @param dev the pointer of device driver structure
 * @param rx_ind the indication callback function
 *
 * @return qe_ok
 */
qe_ret qe_dev_set_rx_indicate(qe_dev *dev, qe_dev_rx_indicate_func rx_indicate)
{
    qe_assert(dev != QE_NULL);

    dev->rx_indicate = rx_indicate;

	dev_debug("%s set rx indicate %p", dev->name, rx_indicate);

    return qe_ok;
}


/**
 * This function will set the indication callback function when device has
 * written data to physical hardware.
 *
 * @param dev the pointer of device driver structure
 * @param tx_done the indication callback function
 *
 * @return qe_ok
 */
qe_ret qe_dev_set_tx_complete(qe_dev *dev, qe_dev_tx_complete_func tx_complete)
{
    qe_assert(dev != QE_NULL);

    dev->tx_complete = tx_complete;

	dev_debug("%s set tx complete %p", dev->name, tx_complete);

    return qe_ok;
}