set(DEVICE_DIR ${PROJECT_ROOT}/device)
set(HEADER_DIR ${PROJECT_ROOT}/include/device)

qelib_include_directories(${HEADER_DIR})

qelib_sources(
    ${DEVICE_DIR}/qe_device.c
    ${DEVICE_DIR}/qe_arch.c)

qelib_install_headers(
    ${HEADER_DIR}/qe_device.h
    ${HEADER_DIR}/qe_arch.h)

# serial
qelib_sources_ifdef(CONFIG_SERIAL ${DEVICE_DIR}/qe_serial.c)
qelib_install_headers_ifdef(CONFIG_SERIAL ${HEADER_DIR}/qe_serial.h)

# spi
qelib_sources_ifdef(
    CONFIG_SPI
    ${DEVICE_DIR}/qe_spi_core.c
    ${DEVICE_DIR}/qe_spi_dev.c)
qelib_install_headers_ifdef(CONFIG_SPI ${HEADER_DIR}/qe_spi.h)

# spi flash
qelib_sources_ifdef(CONFIG_SPI_FLASH ${DEVICE_DIR}/qe_spi_flash.c)
qelib_install_headers_ifdef(CONFIG_SPI_FLASH ${HEADER_DIR}/qe_spi_flash.h)

# iic
qelib_sources_ifdef(CONFIG_IIC ${DEVICE_DIR}/qe_i2c.c)
qelib_install_headers_ifdef(CONFIG_IIC ${HEADER_DIR}/qe_i2c.h)

# image sensor
qelib_sources_ifdef(CONFIG_IMGSEN ${DEVICE_DIR}/qe_imgsen.c)
qelib_install_headers_ifdef(CONFIG_IMGSEN ${HEADER_DIR}/qe_imgsen.h)