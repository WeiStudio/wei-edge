


#include "qe_device.h"
#include "qe_imgsen.h"
#include "qe_macros.h"
#include "qe_assert.h"



#define SEN_LOG_DOMAIN    	"imgsen"
#define sen_debug(...)  	qelog_debug(SEN_LOG_DOMAIN,  __VA_ARGS__)
#define sen_info(...)   	qelog_info(SEN_LOG_DOMAIN,   __VA_ARGS__)
#define sen_notice(...) 	qelog_notice(SEN_LOG_DOMAIN, __VA_ARGS__)
#define sen_warning(...) 	qelog_warning(SEN_LOG_DOMAIN,__VA_ARGS__)
#define sen_error(...)  	qelog_error(SEN_LOG_DOMAIN,  __VA_ARGS__)
#define sen_fatal(...)   	qelog_fatal(SEN_LOG_DOMAIN,  __VA_ARGS__)



static qe_ret imgsen_ioctl(struct qe_device *dev, int cmd, void *args)
{
	qe_ret ret;
	qe_u32 data;
	struct qe_imgsen_device *imgsen;

	qe_assert(dev != QE_NULL);

	imgsen = (struct qe_imgsen_device *)dev;

	switch (cmd) {

	case QE_IMGSEN_CTRL_REG_READ:
		if (!imgsen->reg_read) {
			sen_error("no reg read ops");
			return qe_err_notsupport;
		}
		ret = imgsen->reg_read(imgsen, (qe_imgsen_reg_t *)args);
		break;

	case QE_IMGSEN_CTRL_REG_WRITE:
		if (!imgsen->reg_write) {
			sen_error("no reg write ops");
			return qe_err_notsupport;
		}
		ret = imgsen->reg_write(imgsen, (qe_imgsen_reg_t *)args);
		break;

	case QE_IMGSEN_CTRL_SET_EXP:
		if (!imgsen->set_exposure) {
			sen_error("no set exp ops");
			return qe_err_notsupport;
		}
		data = *(qe_u32 *)args;
		ret = imgsen->set_exposure(imgsen, data);
		break;

	case QE_IMGSEN_CTRL_SET_GAN:
		if (!imgsen->set_gian) {
			sen_error("no set gan ops");
			return qe_err_notsupport;
		}
		data = *(qe_u32 *)args;
		ret = imgsen->set_gian(imgsen, data);
		break;

	default:
		sen_error("unknown cmd:%d", cmd);
		break;
	}

	return ret;
}

static qe_dev_ops imgsen_ops = {
	QE_NULL,
	QE_NULL,
	QE_NULL,
	QE_NULL,
	QE_NULL,
	imgsen_ioctl,
};

qe_ret qe_imgsen_register(struct qe_imgsen_device *sensor, const char *name,
	void *data)
{
	struct qe_device *dev;

	dev = &(sensor->parent);
	dev->type = QE_DEV_IMGSENSOR;
	dev->ops = &imgsen_ops;
	dev->private = data;

	return qe_dev_register(dev, name, QE_DEV_F_RDWR);
}

