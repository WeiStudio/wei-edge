#include "qelib.h"
#include <stdio.h>



qe_ubase_t qe_get_fp()
{
    return (qe_ubase_t)__builtin_frame_address(0);
}

static void call_sub(int a)
{
    int i;
    qe_ubase_t *pos;

    qe_debug("fp %x a %d", qe_get_fp(), a);

    pos = (qe_ubase_t *)qe_get_fp;

    for (i=0; i<32; i+=4) {
        qe_debug("%08x %08x %08x %08x", 
            pos[i], pos[i+1], pos[i+2], pos[i+3]);
    }
}

int main(int argc, char **argv)
{
    int a = 2;

    qelog_init(QELOG_DEBUG, QELOG_CL|QELOG_DM|QELOG_LV|QELOG_HMS);

    call_sub(a);

    return 0;
}