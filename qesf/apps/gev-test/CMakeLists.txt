add_executable(gev-device gev-device.c)

target_include_directories(
    gev-device PRIVATE 
    ${PROJECT_ROOT}/include
    ${AUTOGEN_KCONFIG})
target_link_libraries(gev-device ${QESFLIB})

add_executable(gev-app gev-app.c)

target_include_directories(
    gev-app PRIVATE 
    ${PROJECT_ROOT}/include
    ${AUTOGEN_KCONFIG})
target_link_libraries(gev-app ${QESFLIB})