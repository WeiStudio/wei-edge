


#ifndef __GEV_H__
#define __GEV_H__



#include "qe_def.h"



#define GVCP_PORT   3956

typedef enum {
    GEV_BOOTSTRAP_VERSION = 0x0000, /* Version of the GigE Standard with which the device is compliant */
    GEV_BOOTSTRAP_DEV_MODEL = 0x0004,    /* Information about device mode of operation */
    GEV_BOOTSTRAP_MAC_HIG = 0x0008,
    GEV_BOOTSTRAP_MAC_LOW = 0x000C,
    GEV_BOOTSTRAP_NETIF_CAP = 0x0010,   /* Supported IP Configuration and PAUSE schemes */
    GEV_BOOTSTRAP_NETIF_CFG = 0x0014,   /* Activated IP Configuration and PAUSE schemes */
    GEV_BOOTSTRAP_CURRENT_IP = 0x0024,  /* Current IP address of this device on its first interface */
    GEV_BOOTSTRAP_SUBNET_MASK = 0x0034, /* Current subnet mask used by this device on its first interface */
    GEV_BOOTSTRAP_CURRENT_GW = 0x0044,  /* Current default gateway used by this device on its first interface */
    GEV_BOOTSTRAP_MANU_NAME = 0x0048,   /* Provides the name of the manufacturer of the device */
    GEV_BOOTSTRAP_MODEL_NAME = 0x0068,  /* Provides the model name of the device */
    GEV_BOOTSTRAP_DEV_VERSION = 0x0088, /* Provides the version of the device */
    GEV_BOOTSTRAP_MANU_INFO = 0x00A8,   /* Provides extended manufacturer information about the device */
    GEV_BOOTSTRAP_SN = 0x00D8, /* When supported, this string contains the serial number of the device */
    GEV_BOOTSTRAP_USER_NAME = 0x00E8,   /* User-programmable name to assign to the device */
    GEV_BOOTSTRAP_FIRST_URL = 0x0200,   /* String using the format in character_set_index of Device Mode register */
    GEV_BOOTSTRAP_SECOND_URL = 0x0400,
    GEV_BOOTSTRAP_NETIF_NUM = 0x0600,   /* Indicates the number of physical network interfaces on this device */
    GEV_BOOTSTRAP_PERSISTENT_IP = 0x064C,    /* Only available if Persistent IP is supported by the device */
    GEV_BOOTSTRAP_PERSISTENT_SUBNET_MASK = 0x065C,
    GEV_BOOTSTRAP_PERSISTENT_GW = 0x066C,
    GEV_BOOTSTRAP_LINK_SPEED = 0x0670,
    GEV_BOOTSTRAP_MSG_CH_NUM = 0x0900,
    GEV_BOOTSTRAP_STR_CH_NUM = 0x0904,
    GEV_BOOTSTRAP_ACT_SIG_NUM = 0x0908,
    GEV_BOOTSTRAP_ACT_DEV_KEY = 0x090C,
    GEV_BOOTSTRAP_ACT_LINK_NUM = 0x0910,
    GEV_BOOTSTRAP_GVSP_CAP = 0x092C,
    GEV_BOOTSTRAP_MSG_CH_CAP = 0x0930,
    GEV_BOOTSTRAP_GVCP_CAP = 0x0934,
    GEV_BOOTSTRAP_HEARTBEAT_TIMEOUT = 0x0938,
    GEV_BOOTSTRAP_TS_TICK_FREQ_HIG = 0x093C,
    GEV_BOOTSTRAP_TS_TICK_FREQ_LOW = 0x0940,
    GEV_BOOTSTRAP_TS_CTRL = 0x0944,
    GEV_BOOTSTRAP_TS_VAL_HIG = 0x0948,
    GEV_BOOTSTRAP_TS_VAL_LOW = 0x094C,
    GEV_BOOTSTRAP_DISCOVERY_ACK_DELAY = 0x0950,
    GEV_BOOTSTRAP_GVCP_CFG = 0x0954,
    GEV_BOOTSTRAP_PEND_TIMEOUT = 0x0958,
    GEV_BOOTSTRAP_CTRL_SWITCHOVER_KEY = 0x095C,
    GEV_BOOTSTRAP_GVSP_CFG = 0x0960,
    GEV_BOOTSTRAP_PHY_LINK_CFG_CAP = 0x0964,
    GEV_BOOTSTRAP_PHY_LINK_CFG = 0x0968,
    GEV_BOOTSTRAP_IEEE_1588_STATUS = 0x096C,
    GEV_BOOTSTRAP_SCHED_ACT_CMD_QUEUE_SIZE = 0x0970,
    GEV_BOOTSTRAP_IEEE_1588_EXT_CAP = 0x0974,
    GEV_BOOTSTRAP_IEEE_1588_SUP_PROFILE = 0x0978,
    GEV_BOOTSTRAP_IEEE_1588_SCHED_PROFILE = 0x097C,
    GEV_BOOTSTRAP_CTRL_CH_PRIVILEGE = 0x0A00,
    GEV_BOOTSTRAP_CCP = 0x0A00,
    GEV_BOOTSTRAP_PRIMARY_APP_PORT = 0x0A04,    /* UDP source port of the control channel of the primary                                                                                                                                                      m  application */
    GEV_BOOTSTRAP_PRIMARY_APP_IP = 0x0A14,
    GEV_BOOTSTRAP_MSG_CH_PORT = 0x0B00,
    GEV_BOOTSTRAP_MCP = 0x0B00,
    GEV_BOOTSTRAP_MSG_CH_DST_ADDR = 0x0B10,
    GEV_BOOTSTRAP_MCDA = 0x0B10,
    GEV_BOOTSTRAP_MSG_CH_TRANS_TIMEOUT = 0x0B14,
    GEV_BOOTSTRAP_MCTT = 0x0B14,
    GEV_BOOTSTRAP_MSG_CH_RETRY_COUNT = 0x0B18,
    GEV_BOOTSTRAP_MCRC = 0x0B18,
    GEV_BOOTSTRAP_MSG_CH_SRC_PORT = 0x0B1C,
    GEV_BOOTSTRAP_MCSP = 0x0B1C,
    GEV_BOOTSTRAP_MSG_CH_CFG = 0x0B20,
    GEV_BOOTSTRAP_MCCFG = 0x0B20,
    GEV_BOOTSTRAP_STR_CH_PORT0 = 0x0D00,

} gev_bootstrap_regs_e;


#define GVCP_MSG_KEY    0x42
#define GVCP_FLAG_ACKNOWLEDGE  0x80
typedef struct gvcp_command_header {
    qe_u8  key;
    qe_u8  flag;
    qe_u16 command;
    qe_u16 length;
    qe_u16 req_id;
} gvcp_cmd_header_t;

typedef struct gvcp_acknowledge_header {
    qe_u16 status;
    qe_u16 answer;
    qe_u16 length;
    qe_u16 ack_id;
} gvcp_ack_header_t;

#define GVCP_FLAG_ALLOW_BROADCAST  0x08
typedef struct gvcp_discovery_ack {
    struct gvcp_acknowledge_header head;
    qe_u16 spec_version_major;
    qe_u16 spec_version_minor;
    qe_u32 device_mode;
    qe_u16 reserved1;
    qe_u8  device_mac[6];
    qe_u32 ip_config_options;
    qe_u32 ip_config_current;
    qe_u32 reserved2[3];
    qe_u32 current_ip;
    qe_u32 reserved3[3];
    qe_u32 current_subnet_mask;
    qe_u32 reserved4[3];
    qe_u32 default_gateway;
    char   manufacturer_name[32];
    char   model_name[32];
    char   device_version[32];
    char   manufacturer_specific_information[48];
    char   serial_number[16];
    char   user_defined_name[16];
}gvcp_discovery_ack_t;

typedef struct gvcp_forceip_cmd {
    struct gvcp_command_header head;
    qe_u16 reserved1;
    qe_u8  mac[6];
    qe_u32 reserved2[3];
    qe_u32 static_ip;
    qe_u32 reserved3[3];
    qe_u32 static_subnet_mask;
    qe_u32 reserved4[3];
    qe_u32 static_default_gateway;
}gvcp_forceip_cmd_t;

typedef gvcp_ack_header_t gvcp_forceip_ack_t;

typedef struct gvcp_readreg_cmd {
    struct gvcp_command_header head;
    qe_u32 address[0];
}gvcp_readreg_cmd_t;

typedef struct gvcp_readreg_ack {
    struct gvcp_acknowledge_header head;
    qe_u32 reg[0];
}gvcp_readreg_ack_t;

typedef enum {
    GEV_DISCOVERY_CMD = 0x0002,
    GEV_DISCOVERY_ACK = 0x0003,
    GEV_FORCEIP_CMD = 0x0004,
    GEV_FORCEIP_ACK = 0x0005,
    GEV_PACKETRESEND_CMD = 0x0040,
    GEV_READREG_CMD = 0x0080,
    GEV_READREG_ACK = 0x0081,
    GEV_WRITEREG_CMD = 0x0082,
    GEV_WRITEREG_ACK = 0x0083,
    GEV_READMEM_CMD = 0x0084,
    GEV_READMEM_ACK = 0x0085,
    GEV_WRITEMEM_CMD = 0x0086,
    GEV_WRITEMEM_ACK = 0x0087,
    GEV_PENDING_ACK = 0x0089,
    GEV_EVENT_CMD = 0x00C0,
    GEV_EVENT_ACK = 0x00C1,
    GEV_EVENTDATA_CMD = 0x00C2,
    GEV_EVENTDATA_ACK = 0X00C3,
    GEV_ACTION_CMD = 0x0100,
    GEV_ACTION_ACK = 0x0101,
} gev_cmd_e;

typedef enum {
    GEV_STATUS_SUCCESS = 0x0000,
    GEV_STATUS_PACKET_RESEND = 0x0100,
    GEV_STATUS_NOT_IMPLEMENTED = 0x8001,
    GEV_STATUS_INVALID_PARAMETER = 0x8002,
    GEV_STATUS_INVALID_ADDRESS = 0x8003,
    GEV_STATUS_WRITE_PROTECT = 0x8004,
    GEV_STATUS_BAD_ALIGNMENT = 0x8005,
    GEV_STATUS_ACCESS_DENIED = 0x8006,
    GEV_STATUS_BUSY = 0x8007,
    GEV_STATUS_PACKET_UNAVAILABLE = 0x800C,
    GEV_STATUS_DATA_OVERRUN = 0x800D,
    GEV_STATUS_INVALID_HEADER = 0x800E,
    GEV_STATUS_PACKET_NOT_YET_AVAILABLE = 0x8010,
    GEV_STATUS_PACKET_AND_PREV_REMOVED_FROM_MEMORY = 0x8011,
    GEV_STATUS_PACKET_REMOVED_FROM_MEMORY = 0x8012,
    GEV_STATUS_NO_REF_TIME = 0x8013,
    GEV_STATUS_PACKET_TEMPORARILY_UNAVAILABLE = 0x8014,
    GEV_STATUS_OVERFLOW = 0x8015,
    GEV_STATUS_ACTION_LATE = 0x8016,
    GEV_STATUS_LEADER_TRAILER_OVERFLOW = 0x8017,
    GEV_STATUS_ERROR = 0x8FFF,
} gev_status_e;

typedef enum {
    GEV_EVENT_PRIMARY_APP_SWITCH = 0x0007,
    GEV_EVENT_LINK_SPEED_CHANGE = 0x0008,
    GEV_EVENT_ACTION_LATE = 0x0009,
} gev_event_e;

#endif /* __GEV_H__ */