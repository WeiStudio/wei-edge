


#include "qelib.h"
#include "yyjson.h"



static void read_json()
{
    const char *json = "{\"name\":\"Mash\",\"star\":4,\"hits\":[2,2,1,3]}";
    yyjson_doc *doc = yyjson_read(json, strlen(json), 0);
    yyjson_val *root = yyjson_doc_get_root(doc);

    yyjson_val *name = yyjson_obj_get(root, "name");

    qe_info("name: %s", yyjson_get_str(name));
    qe_info("name length: %d", (int)yyjson_get_len(name));

    yyjson_val *star = yyjson_obj_get(root, "star");
    qe_info("star: %d", (int)yyjson_get_int(star));

    yyjson_val *hits = yyjson_obj_get(root, "hits");
    size_t idx, max;
    yyjson_val *hit;
    yyjson_arr_foreach(hits, idx, max, hit) {
        qe_info("hit[%d]: %d", (int)idx, (int)yyjson_get_int(hit));
    }

    yyjson_doc_free(doc);
}

static void write_json()
{
    yyjson_mut_doc *doc = yyjson_mut_doc_new(NULL);
    yyjson_mut_val *root = yyjson_mut_obj(doc);
    yyjson_mut_doc_set_root(doc, root);

    yyjson_mut_obj_add_str(doc, root, "name", "YYJSONTest");
    yyjson_mut_obj_add_int(doc, root, "year", 2024);
    int dates[] = {3, 6, 21, 01, 03};
    yyjson_mut_val *date = yyjson_mut_arr_with_sint32(doc, dates, 4);
    yyjson_mut_obj_add_val(doc, root, "date", date);

    const char *json = yyjson_mut_write(doc, 0, NULL);
    if (json) {
        qe_info("json: %s", json);
        free((void *)json);
    }

    yyjson_mut_doc_free(doc);
}

static void read_json_file()
{
    yyjson_read_flag flg = YYJSON_READ_ALLOW_COMMENTS | YYJSON_READ_ALLOW_TRAILING_COMMAS;
    yyjson_read_err err;
    yyjson_doc *doc = yyjson_read_file("test.json", flg, NULL, &err);
    yyjson_val *root = yyjson_doc_get_root(doc);

    if (doc) {
        yyjson_val *obj = yyjson_doc_get_root(doc);
        yyjson_obj_iter iter;
        yyjson_obj_iter_init(obj, &iter);
        yyjson_val *key, *val;
        while ((key = yyjson_obj_iter_next(&iter))) {
            val = yyjson_obj_iter_get_val(key);
            qe_info("%s: %s", yyjson_get_str(key), yyjson_get_type_desc(val));
        }
    } else {
        qe_error("read error (%u): %s at position: %ld", err.code, err.msg, err.pos);
    }

    const char *json = yyjson_write(doc, 0, NULL);
    if (json) {
        qe_info("json: %s", json);
        free((void *)json);
    }

    yyjson_doc_free(doc);
}

int main(int argc, char *argv[])
{
    qelog_init(QELOG_DEBUG, QELOG_DM|QELOG_DATE|QELOG_LV|QELOG_CL);

    read_json();

    write_json();

    read_json_file();

    return 0;
}