/*
 * YAFFS: Yet another Flash File System . A NAND-flash specific file system.
 *
 * Copyright (C) 2002-2018 Aleph One Ltd.
 *
 * Created by Charles Manning <charles@aleph1.co.uk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 2.1 as
 * published by the Free Software Foundation.
 *
 * Note: Only YAFFS headers are LGPL, YAFFS C code is covered by GPL.
 */


#ifndef __YPORTENV_H__
#define __YPORTENV_H__


#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include "qelib.h"

#define CONFIG_YAFFS_DIRECT 1
#define CONFIG_YAFFS_SHORT_NAMES_IN_RAM 1
#define CONFIG_YAFFS_YAFFS2 1
#define CONFIG_YAFFS_PROVIDE_DEFS 1
#define CONFIG_YAFFSFS_PROVIDE_VALUES 0
#define CONFIG_YAFFS_DEFINES_TYPES 1
#define CONFIG_YAFFS_USE_32_BIT_TIME_T 1
#define NO_Y_INLINE 1
#define LOFF_T_32_BIT
#define loff_t off_t


/* Definition of types */
#ifdef CONFIG_YAFFS_DEFINES_TYPES
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;
typedef signed int s32;
#endif

#ifdef CONFIG_YAFFS_PROVIDE_DEFS
/* File types */


#define DT_UNKNOWN	0
#define DT_FIFO		1
#define DT_CHR		2
#define DT_DIR		4
#define DT_BLK		6
#define DT_REG		8
#define DT_LNK		10
#define DT_SOCK		12
#define DT_WHT		14


/*
 * Attribute flags.
 * These are or-ed together to select what has been changed.
 */
#define ATTR_MODE	1
#define ATTR_UID	2
#define ATTR_GID	4
#define ATTR_SIZE	8
#define ATTR_ATIME	16
#define ATTR_MTIME	32
#define ATTR_CTIME	64

struct iattr {
	unsigned int ia_valid;
	unsigned ia_mode;
	unsigned ia_uid;
	unsigned ia_gid;
	unsigned ia_size;
	unsigned ia_atime;
	unsigned ia_mtime;
	unsigned ia_ctime;
	unsigned int ia_attr_flags;
};

#endif

#if defined CONFIG_YAFFS_WINCE

#include "ywinceenv.h"


#elif defined CONFIG_YAFFS_DIRECT

/* Direct interface */
#include "ydirectenv.h"

#elif defined CONFIG_YAFFS_UTIL

#include "yutilsenv.h"

#else
/* Should have specified a configuration type */
#error Unknown configuration


#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>

#endif

/* Create some less common define values if they don't exist */
#ifndef XATTR_CREATE
#define XATTR_CREATE 1
#endif

#ifndef XATTR_REPLACE
#define XATTR_REPLACE 2
#endif

#ifndef R_OK
#define R_OK	4
#define W_OK	2
#define X_OK	1
#define F_OK	0
#endif

#ifndef S_IFSOCK
#define S_IFSOCK	0140000
#endif

#ifndef S_IFLNK
#define S_IFLNK		0120000
#endif

#ifndef S_ISSOCK
#define S_ISSOCK(m)	(((m) & S_IFMT) == S_IFSOCK)
#endif

#ifndef Y_DUMP_STACK
#define Y_DUMP_STACK() do { } while (0)
#endif

#ifndef BUG
#define BUG() do {\
	yaffs_trace(YAFFS_TRACE_BUG,\
		"==>> yaffs bug: " __FILE__ " %d",\
		__LINE__);\
	Y_DUMP_STACK();\
} while (0)
#endif

#define YAFFS_LOG_DOMAIN		"yaffs"
#define yaffs_debug(...)		qelog_debug(YAFFS_LOG_DOMAIN, __VA_ARGS__)
#define yaffs_hexdump(...)		qehex_debug(YAFFS_LOG_DOMAIN, __VA_ARGS__)

#endif
