#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include "qelib.h"



int main(int argc, char *argv[])
{
    int n;
    int rn;
    int pos;
    int fd;
    char buffer[2048] = {0x0};
    
    qelog_init(QELOG_DEBUG, QELOG_CL|QELOG_DM|QELOG_DATE|QELOG_LV);

    pos = atoi(argv[2]);
    rn = atoi(argv[3]);

    fd = open(argv[1], O_RDWR|O_BINARY);
    if (fd < 0) {
        printf("open error\r\n");
        return -1;
    }
    lseek(fd, pos, SEEK_SET);
    qehex_debug("read", buffer, rn);
    n = read(fd, buffer, rn);
    printf("read pos:%d n:%d\r\n", pos, n);
    qehex_debug("read", buffer, rn);
    close(fd);
    return 0;
}