


#include <time.h>
#include <stdio.h>
#include "qelib.h"



typedef struct
{
    int count;
    unsigned long long bytes;
}trans_stat_e;



int main(int argc, char *argv[])
{
    int opt;
    FILE *fp;
    time_t ts;
    struct tm *td;
    trans_stat_e stat;
    fp = fopen("stat.txt", "w+");

    for (int i=0; i<1000; i++) {
        time(&ts);
        td = localtime(&ts);
        stat.bytes += 1024;
        stat.count += 1;
        fprintf(fp, "[%d-%02d-%02d %02d:%02d:%02d] %d,%d\n", 
            td->tm_year+1900, td->tm_mon + 1, td->tm_mday, 
            td->tm_hour, td->tm_min, td->tm_sec, 
            stat.count, stat.bytes);
    }

    fclose(fp);

    return 0;
}