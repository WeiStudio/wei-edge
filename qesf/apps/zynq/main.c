


#include "qelib.h"
#include "ps_uart.h"


char heap[8192];


static void log_output(char *buf, int len)
{
	for (int i=0; i<len; i++) {
		ps_uart_out(0xFF010000, buf[i]);
	}    
}

static void assert_hook(const char *func, const char *ext, qe_size line)
{
    sys_fatal("Assert: %s %s %d", func, ext, line);
    while (1);
}

static void logger_init(void)
{
    qelog_init(QELOG_DEBUG, QELOG_DM | QELOG_FUNC | QELOG_LV);

    qelog_set_default_output(log_output);
}

int main(void)
{
    qemem_heap_set(heap, 8192);

    logger_init();

    qe_assert_set_hook(assert_hook);

    sys_notice("ZYNQ Test");
    sys_notice("Build time %s %s", __DATE__, __TIME__);
    sys_notice("Copyright (C) by Wei.Studio");

    qe_initcall();

    sys_warning("System Exit!!!");

    for (;;);
}