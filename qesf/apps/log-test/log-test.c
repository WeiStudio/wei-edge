


#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include "qelib.h"


#define TEST1_LOG_DOMAIN   "test1"
#define TEST2_LOG_DOMAIN   "test2"
#define TEST3_LOG_DOMAIN   "test3"


typedef enum {
    ARGS_HAS_LEVEL = 0,
    ARGS_HAS_DATE,
    ARGS_HAS_FILE,
    ARGS_HAS_FUNC,
    ARGS_HAS_LINE,
    ARGS_HAS_PID,
} args_opt_e;

static void usage(void)
{
    printf("log-test usage\n");
    printf("-l,--loglevel      log level, default:DEBUG\n");
    printf("-t,--pts           log with pts\n");
    printf("-d,--domain        log with domain\n");
    printf("-v,--lvl           log with level\n");
    printf("-p,--pid           log with pid\n");
    printf("-a,--archive       log with archive\n");
    printf("-b,--ringbuffer    log with ringbuffer\n");
    printf("--date             log with date\n");
    printf("--file             log with filename\n");
    printf("--func             log with function name\n");
    printf("--line             log with line number\n");
    printf("-c,--color         log with color\n");
    printf("-h,--help          show usage\n");
}

static void test2_output(const char *message)
{
    printf("test2 output:%s\r\n", message);
}

static void test3_handler(const char *domain_name, qelog_level_e level, 
	const char *file, const char *func, int line, const char *message)
{
    const char *lvl = qelog_get_level_str(level);
    printf("[%s][%s]: %s\r\n", domain_name, lvl, message);
}

int main(int argc, char *argv[])
{
    int opt;
    int loglevel    = QELOG_DEBUG;
    int has_pts     = 0;
    int has_date    = 0;
    int has_domain  = 0;
    int has_level   = 0;
    int has_file    = 0;
    int has_func    = 0;
    int has_line    = 0;
    int has_pid     = 0;
    int has_color   = 0;
    int has_archive = 0;
    int has_buffer  = 0;
    qe_u32 flags    = 0;

    static const struct option long_opts[] = {
        {"loglevel",    required_argument,  QE_NULL, 'l'},
        {"pts",         required_argument,  QE_NULL, 't'},
        {"domain",      no_argument,        QE_NULL, 'd'},
        {"lvl",         no_argument,        QE_NULL, 'v'},
        {"pid",         no_argument,        QE_NULL, 'p'},
        {"archive",     no_argument,        QE_NULL, 'a'},
        {"ringbuffer",  no_argument,        QE_NULL, 'b'},
        {"date",        no_argument,        QE_NULL, ARGS_HAS_DATE},
        {"file",        no_argument,        QE_NULL, ARGS_HAS_FILE},
        {"func",        no_argument,        QE_NULL, ARGS_HAS_FUNC},
        {"line",        no_argument,        QE_NULL, ARGS_HAS_LINE},
        {"color",       no_argument,        QE_NULL, 'c'},
        {"help",        no_argument,        QE_NULL, 'h'},
    };

    while ((opt = getopt_long(argc, argv, "l:tdvmpabch-", long_opts, NULL)) != -1) {
        switch (opt) {
        case 'l':
            loglevel = atoi(optarg);
            break;
        case 't':
            has_pts = 1;
            break;
        case 'd':
            has_domain = 1;
            break;
        case 'v':
            has_level = 1;
            break;
        case 'p':
            has_pid = 1;
            break;
        case ARGS_HAS_DATE:
            has_date = 1;
            break;
        case ARGS_HAS_FILE:
            has_file = 1;
            break;
        case ARGS_HAS_FUNC:
            has_func = 1;
            break;
        case ARGS_HAS_LINE:
            has_line = 1;
            break;
        case 'c':
            has_color = 1;
            break;
        case 'a':
            has_archive = 1;
            break;
        case 'b':
            has_buffer = 1;
            break;
        case '-':
        case '?':
        case 'h':
        default:
            usage();
            exit(1);
        }
    }

    if (has_pts)
        flags |= QELOG_PTS;
    if (has_date)
        flags |= QELOG_DATE;
    if (has_domain)
        flags |= QELOG_DM;
    if (has_color)
        flags |= QELOG_CL;
    if (has_pid)
        flags |= QELOG_PID;
    if (has_level)
        flags |=  QELOG_LV;
    if (has_file)
        flags |= QELOG_FILE;
    if (has_func)
        flags |= QELOG_FUNC;
    if (has_line)
        flags |= QELOG_LINE;
    if (has_archive)
        flags |= QELOG_AR;
    if (has_buffer)
        flags |= QELOG_RB;

    qelog_init(loglevel, flags);
    qelog_set_handler("rfile", QELOG_DEBUG, QE_NULL);
    qelog_domain_set_level(TEST1_LOG_DOMAIN, QELOG_DEBUG);
    qelog_domain_set_level(TEST2_LOG_DOMAIN, QELOG_INFO);
    qelog_domain_set_level(TEST3_LOG_DOMAIN, QELOG_NOTICE);
    qelog_domain_set_output(TEST2_LOG_DOMAIN, test2_output);
    qelog_domain_set_handler(TEST3_LOG_DOMAIN, test3_handler);

    if (has_buffer) {
        qe_gbuf *buf = qe_gbuf_new(8192);
        qelog_set_buffer(buf->p, buf->nbytes);
    }

    if (has_archive) {
        qelog_set_archive(".", "logtest", "log", 4, 1024*32, 1024*1024);
    }

    qe_debug("hello");
    qe_info("hello");
    qe_notice("hello");
    qe_warning("hello");
    qe_critical("hello");
    qe_error("hello");
    qe_fatal("hello");

    qelog_debug(TEST1_LOG_DOMAIN, "hello");
    qelog_info(TEST1_LOG_DOMAIN, "hello");
    qelog_notice(TEST1_LOG_DOMAIN, "hello");
    qelog_warning(TEST1_LOG_DOMAIN, "hello");
    qelog_critical(TEST1_LOG_DOMAIN, "hello");
    qelog_error(TEST1_LOG_DOMAIN, "hello");
    qelog_fatal(TEST1_LOG_DOMAIN, "hello");

    qelog_debug(TEST2_LOG_DOMAIN, "hello");
    qelog_info(TEST2_LOG_DOMAIN, "hello");
    qelog_notice(TEST2_LOG_DOMAIN, "hello");
    qelog_warning(TEST2_LOG_DOMAIN, "hello");
    qelog_critical(TEST2_LOG_DOMAIN, "hello");
    qelog_error(TEST2_LOG_DOMAIN, "hello");
    qelog_fatal(TEST2_LOG_DOMAIN, "hello");

    qelog_debug(TEST3_LOG_DOMAIN, "hello");
    qelog_info(TEST3_LOG_DOMAIN, "hello");
    qelog_notice(TEST3_LOG_DOMAIN, "hello");
    qelog_warning(TEST3_LOG_DOMAIN, "hello");
    qelog_critical(TEST3_LOG_DOMAIN, "hello");
    qelog_error(TEST3_LOG_DOMAIN, "hello");
    qelog_fatal(TEST3_LOG_DOMAIN, "hello");

    if (has_archive) {
        for (int i=0; i<50000; i++) {
            qelog_info(TEST1_LOG_DOMAIN, "Archive testing %d ......", i);
            qelog_info(TEST2_LOG_DOMAIN, "Archive testing %d ......", i);
            usleep(20);
        }
    }

    return 0;
}