


#include "qelib.h"
#include <dirent.h>
#include <fcntl.h>
#include <pthread.h>



#define XLOG_LOG_DOMAIN     "xlog"
#define xlog_debug(...)     qelog_debug(XLOG_LOG_DOMAIN, __VA_ARGS__)
#define xlog_info(...)      qelog_info(XLOG_LOG_DOMAIN, __VA_ARGS__)
#define xlog_notice(...)    qelog_notice(XLOG_LOG_DOMAIN, __VA_ARGS__)
#define xlog_error(...)     qelog_error(XLOG_LOG_DOMAIN, __VA_ARGS__)



typedef struct 
{
    qe_ringbuffer *buf;
    pthread_mutext_t lock;
    pthread_cond_t client_cond;
} xlog;

typedef struct 
{
    xlog *root;
    xlog *log;
    xlog *notify;
    pthread_t tid;
    pthread_mutext_t lock;
    pthread_cond_t cond;
    qe_bool is_wait;
    qe_bool is_run;
} xlogger;

xlogger logger;

void *xlog_work(void *arg)
{
    xlog_notice("work run");

    while (1) {

        if (!logger.is_run) {
            break;
        }

        
    }
}

void xlog_init(int bufsize)
{
    int ret;
    pthread_attr_t attr;

    qe_memset(&logger, 0x0, sizeof(logger));
    pthread_attr_init(&attr);
    pthread_cond_init(&logger.cond, NULL);
    pthread_mutex_init(&logger.lock, NULL);
    logger.is_run = qe_true;
    logger.is_wait = qe_false;

    xlog *log = xlog_new(bufsize);
    qe_assert(log);

    logger.root = log;

    ret = pthread_create(&logger.tid, &attr, xlog_work, QE_NULL);
    if (ret != 0) {
        xlog_errro("create tlog work failed");
        return;
    }

    return;
}

int main(int argc, char *argv[])
{

}