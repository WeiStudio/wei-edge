


#include "qelib.h"
#include <time.h>
#include <stdlib.h>
#include "metric.h"



#define METRIC_LOG_DOMAIN   "metric"
#define metric_debug(...)   qelog_debug(METRIC_LOG_DOMAIN, __VA_ARGS__)
#define metric_info(...)    qelog_info(METRIC_LOG_DOMAIN, __VA_ARGS__)
#define metric_error(...)   qelog_error(METRIC_LOG_DOMAIN, __VA_ARGS__)



static int random_int(int min, int max)
{
    static qe_bool init = qe_false;

    if (init == qe_false) {
        time_t now;
        srand(time(&now));
        init = qe_true;
    }

    return rand() % max + min;
}

static void metric_period_callback(double throughput)
{
    metric_debug("throughput:%s", metric_bps_str(throughput));
}

int main(int argc, char *argv[])
{
    int i;
    int sleep_ms;
    metric_tp mtp;
    metric_op mop;
    qe_u64 bytes;

    qelog_init(QELOG_DEBUG, QELOG_CL|QELOG_DM|QELOG_DATE);

    metric_tp_init(&mtp, 2, metric_period_callback);
    metric_op_init(&mop);

    for (i=0; i<10; i++) {
        bytes = random_int(400, 15000);
        sleep_ms = random_int(10, 180);
        //metric_debug("ramdon bytes:%d", bytes);
        //metric_tp_update(&mtp, bytes);

        metric_op_start(&mop);
        qe_msleep(sleep_ms);
        metric_op_stop(&mop, bytes);
        metric_debug("bytes:%d sleep:%d lantacy:%d throughput:"METRIC_BPS_FMT, 
            bytes, sleep_ms, METRIC_VAL(METRIC_LAT(mop)),
            METRIC_VAL(METRIC_TP(mop)));
    }

    metric_debug("bytes min:%u max:%u acc:%u",
        mop.bytes.min,
        mop.bytes.max,
        mop.bytes.acc);
    metric_debug("lantacy min:%u max:%u acc:%u",
        METRIC_VMIN(METRIC_LAT(mop)),
        METRIC_VMAX(METRIC_LAT(mop)),
        METRIC_VACC(METRIC_LAT(mop)));
    metric_debug("throughput min:"METRIC_BPS_FMT" max:"METRIC_BPS_FMT" acc:"METRIC_BPS_FMT" mean:"METRIC_BPS_FMT,
        mop.tp.min,
        mop.tp.max,
        mop.tp.acc,
        mop.tp.mea);
    return 0;
}