
add_executable(shell-test shell_test.c)
target_include_directories(
    shell-test PRIVATE 
    ${PROJECT_ROOT}/include
    ${BUILD_DIR}/kconfig/include/generated)
target_link_libraries(shell-test ${QESFLIB})
#set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -T ${CMAKE_CURRENT_SOURCE_DIR}/ldscript.ld -Wl,-Map=shell.map")