

#include "qelib.h"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>



int var_test_num = 0;
char var_test_str[] = "string";



int cmd_hello(char *name, int age)
{
    printf("hello name:%s age:%d\n", name, age);
}

int cmd_phy_read(int addr)
{
    printf("phy read 0x%x 0x%x\n", addr, 0x0);
}

int cmd_phy_write(int addr, int value)
{
    printf("phy write 0x%x 0x%x\n", addr, value);
}

int cmd_epc_read(int addr)
{
    printf("epc read 0x%x 0x%x\n", addr, 0x0);
}

int cmd_epc_write(int addr, int value)
{
    printf("epc write 0x%x 0x%x\n", addr, value);
}

const qe_shell_command command_tab[] = {
    
    QE_SHELL_USR_ITEM(QE_SHELL_DEFAULT_USER, QE_SHELL_DEFAULT_PSWD, default user),
    
    QE_SHELL_KEY_ITEM(0x1B5B4100,  qe_shell_key_up,     up),
    QE_SHELL_KEY_ITEM(0x1B5B4200,  qe_shell_key_down,   down),
    QE_SHELL_KEY_ITEM(0x1B5B4300,  qe_shell_key_right,  right),
    QE_SHELL_KEY_ITEM(0x1B5B4400,  qe_shell_key_left,   left),
    QE_SHELL_KEY_ITEM(0x0A000000,  qe_shell_key_enter,  enter),
    QE_SHELL_CMD_EXEC(help,        qe_shell_cmd_help,   show help info),
    QE_SHELL_CMD_FUNC(2, setvar,   qe_shell_cmd_setvar, set variable),
    QE_SHELL_CMD_FUNC(1, getvar,   qe_shell_cmd_getvar, set variable),

    QE_SHELL_CMD_FUNC(1, hello,    cmd_hello,     hello test),
    QE_SHELL_CMD_FUNC(1, phyread,  cmd_phy_read,  read phy reg),
    QE_SHELL_CMD_FUNC(2, phywrite, cmd_phy_write, write phy reg),
    QE_SHELL_CMD_FUNC(1, epcread,  cmd_epc_read,  read epc reg),
    QE_SHELL_CMD_FUNC(2, epcwrite, cmd_epc_read,  write epc reg),

    QE_SHELL_VAR_INT(test_num, &var_test_num, shell test num),
    QE_SHELL_VAR_STR(test_str, var_test_str,  shell test str),
};

int shell_write(char *data, int len)
{
    int n = len;
    while (n--) {
        putchar(*data++);
    }
    return len;
}

int shell_read(char *data, int len)
{
    int n = len;
    while (n--)
    {
        *data++ = getchar();
    }
    return len;
}

static void signal_handler(int signal)
{
    qelog_notice("shell", "exit");
    exit(EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
    qelog_init(QELOG_INFO,
        QELOG_DM | QELOG_FUNC | QELOG_HMS | QELOG_LV | QELOG_CL);

    signal(SIGINT, signal_handler);

    qe_shell shell;
    qe_memset(&shell, 0x0, sizeof(shell));
    qe_shell_init(&shell, 512, (void *)command_tab, qe_array_size(command_tab), 
        QE_SHELL_DEFAULT_USER, shell_read, shell_write);
    // qe_shell_init(&shell, 512,
    //     QE_SHELL_DEFAULT_USER, shell_read, shell_write);    
    qe_shell_task(&shell);
    return 0;
}