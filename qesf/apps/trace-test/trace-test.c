


#include "qelib.h"
#include <time.h>
#include <stdlib.h>
#include "qe_trace.h"



#define TRACE_LOG_DOMAIN   "trc"
#define trace_debug(...)   qelog_debug(TRACE_LOG_DOMAIN, __VA_ARGS__)
#define trace_info(...)    qelog_info(TRACE_LOG_DOMAIN, __VA_ARGS__)
#define trace_error(...)   qelog_error(TRACE_LOG_DOMAIN, __VA_ARGS__)



int main(int argc, char *argv[])
{
    qelog_init(QELOG_DEBUG, QELOG_CL|QELOG_DM|QELOG_DATE);

    qe_trace_init();

    qe_trace_printf("num:%d", 10);
    qe_trace_printf("begin:%d end:%d", 1, 2);
    qe_trace_printf("addr:0x%x temp:%f", 0x3000, 1.12);

    return 0;
}