
#include "qelib.h"

extern qe_ubase_t _binary_res_txt_start;
extern qe_ubase_t _binary_res_txt_end;
extern qe_ubase_t _binary_res_txt_size;

int main(int argc, char *argv[])
{
    qe_ubase_t start, end, size;
    start = &_binary_res_txt_start;
    end   = &_binary_res_txt_end;
    size  = _binary_res_txt_size;

    qe_info("resource embeded testing");
    qe_info("file 0x%x 0x%x %d", start, end, size);

    qe_hexdump_info(start, size);

    return 0;
}