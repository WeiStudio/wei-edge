#include "qelib.h"
#include "symbol_table.h"
#include <time.h>
#include <getopt.h>



#define TEST_LOGNAME                "test"
#define test_debug(...)             qelog_debug(TEST_LOGNAME,   __VA_ARGS__)
#define test_info(...)              qelog_info(TEST_LOGNAME,    __VA_ARGS__)
#define test_notice(...)            qelog_notice(TEST_LOGNAME,  __VA_ARGS__)
#define test_warning(...)           qelog_warning(TEST_LOGNAME, __VA_ARGS__)
#define test_error(...)             qelog_error(TEST_LOGNAME,   __VA_ARGS__)
#define test_hexdump(...)           qehex_debug(TEST_LOGNAME, __VA_ARGS__)



static char *test_strings[] = {
    "Hello world",
    "Backspace",
    "Login",
    "ecapskcaB",
    "dlrow olleH",
    "this is year:%d",
};

void symbol_tab_dump(symbol_table *tab)
{
    test_hexdump(tab->bytes, tab->tabsize);
}

int main(int argc, char *argv)
{
    int i;
    qe_u32 ret;
    qe_u32 channel = 0;
    symbol_table tab;

    qelog_init(QELOG_DEBUG, QELOG_CL|QELOG_LV|QELOG_DM|QELOG_DATE);

    symbol_tab_init(&tab, 256);

    test_info("first foreach, create");
    for (i=0; i<qe_array_size(test_strings); i++) {
        test_info("open symbol %s", test_strings[i]);
        ret = symbol_open(&tab, test_strings[i], channel);
        test_info("open symbol %s ret:%d", test_strings[i], ret);
    }

    test_info("second foreach, create");
    for (i=0; i<qe_array_size(test_strings); i++) {
        test_info("open symbol %s", test_strings[i]);
        ret = symbol_open(&tab, test_strings[i], channel);
        test_info("open symbol %s ret:%d", test_strings[i], ret);
    }

    symbol_tab_dump(&tab);

    return 0;
}