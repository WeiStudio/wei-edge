#ifndef __SYMBOL_TABLE_H__
#define __SYMBOL_TABLE_H__



#include "qe_def.h"



typedef struct
{
    qe_u32 tabsize;

    qe_u32 next_free;

    qe_u8 *bytes;

    qe_u16 hash[64];
} symbol_table;



void symbol_tab_init(symbol_table *tab, qe_size size);
qe_u32 symbol_open(symbol_table *tab, qe_const_str name, qe_u32 tag);




#endif /* __SYMBOL_TABLE_H__ */