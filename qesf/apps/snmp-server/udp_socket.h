


#ifndef __UDP_SOCKET_H__
#define __UDP_SOCKET_H__



#ifdef _WIN32
#include <winsock2.h>
#pragma comment(lib, "ws2_32.lib")
#else
#include <unistd.h>
#include <arpa/inet.h>
#endif
#include "qelib.h"



typedef struct
{
    WSADATA ws;
    SOCKET s;
    SOCKADDR_IN server_addr;
    SOCKADDR_IN client_addr;
    int client_addr_len;
} udp_socket;



qe_ret udp_socket_init(udp_socket *sock, const char *address, qe_u16 port);
qe_ret udp_socket_bind(udp_socket *sock);
qe_ret udp_socket_send(udp_socket *sock, char *buf, int size);
qe_ret udp_socket_recv(udp_socket *sock, char *buf, int size, int *rxsize);
void udp_socket_exit(udp_socket *sock);



#endif /* __UDP_SOCKET_H__ */