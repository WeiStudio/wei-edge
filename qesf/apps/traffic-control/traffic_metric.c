


#include "qelib.h"
#include "traffic_metric.h"



traffic_metric traffic_metric_init(int period)
{
    traffic_metric m;

    m.period_ms = period;
    m.throughput = 0;
    m.ts = qe_time_ms();
    m.value = 0;

    return m;
}

void traffic_metric_set_period(traffic_metric *m, int ms)
{
    m->period_ms = ms;
    m->ts = qe_time_ms();
}

qe_bool traffic_metric_update(traffic_metric *m, int size)
{
    qe_time_t now = qe_time_ms();
    
    m->value += size;

    //printf("now %u ts %u\n", now, m->ts);

    if ((now - m->ts) >= m->period_ms) {
        m->ts = now;
        m->throughput = (double)m->value / (double)m->period_ms;
        m->value = 0;
        //printf("%p calculate ts %u\n", m, m->ts);
        return qe_true;
    }

    return qe_false;
}