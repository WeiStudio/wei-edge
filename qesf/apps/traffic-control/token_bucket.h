


#ifndef __TOKEN_BUCKET_H__
#define __TOKEN_BUCKET_H__



#include "qe_def.h"
#include <pthread.h>



typedef struct
{
    qe_u32 max;
    qe_u32 rate;
    qe_u32 tokens;
    pthread_mutex_t lock;
    pthread_t thread;
} fix_speed_token;



fix_speed_token *fix_speed_token_new(qe_u32 init, qe_u32 max, qe_u32 rate);

qe_bool fix_speed_token_get(fix_speed_token *token, int size);

void fix_speed_token_set(fix_speed_token *token, qe_u32 init, qe_u32 max, qe_u32 rate);

void fix_speed_token_run(fix_speed_token *token);

#endif /* __TOKEN_BUCKET_H__ */