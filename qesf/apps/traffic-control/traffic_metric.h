


#ifndef __TRAFFIC_METRIC_H__
#define __TRAFFIC_METRIC_H__


#include "qe_def.h"



typedef struct
{
    int period_ms;
    double throughput;
    int value;
    qe_time_t ts;
} traffic_metric;


traffic_metric traffic_metric_init(int period);


qe_bool traffic_metric_update(traffic_metric *m, int size);

void traffic_metric_set_period(traffic_metric *m, int ms);



#endif /* __TRAFFIC_METRIC_H__ */