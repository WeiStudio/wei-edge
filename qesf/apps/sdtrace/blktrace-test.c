#include "qelib.h"
#include "blktrace.h"



#define TEST_LOGNAME                "test"
#define test_debug(...)             qelog_debug(TEST_LOGNAME,   __VA_ARGS__)
#define test_info(...)              qelog_info(TEST_LOGNAME,    __VA_ARGS__)
#define test_notice(...)            qelog_notice(TEST_LOGNAME,  __VA_ARGS__)
#define test_warning(...)           qelog_warning(TEST_LOGNAME, __VA_ARGS__)
#define test_error(...)             qelog_error(TEST_LOGNAME,   __VA_ARGS__)


typedef struct
{
    qe_u16 x;
    qe_u16 y;
} record_target;


typedef struct
{
    qe_u32 frame;
    record_target tragets[4];
} record_data;


static qe_ret ramio_init(blktrace_blockio *bio)
{
    return qe_ok;
}

static qe_ret ramio_erase(blktrace_blockio *bio, qe_u32 block)
{
    test_info("erase block %d", block);
    return qe_ok;
}

static qe_ret ramio_write(blktrace_blockio *bio, qe_u32 chunk, 
    qe_const_ptr data, qe_uint len)
{
    return qe_ok;
}

static qe_ret ramio_read(blktrace_blockio *bio, qe_u32 chunk, 
    qe_ptr data, qe_uint len)
{
    return qe_ok;
}

int main(int argc, char *argv[])
{
    qe_ret ret;
    qe_uint num_records;
    blktrace_blockio bio;
    record_data data;
    qelog_init(QELOG_DEBUG, QELOG_CL|QELOG_DM|QELOG_LV|QELOG_DATE);
    qelog_domain_set_level("blktrace", QELOG_INFO);
    blktrace *trace = blktrace_new(sizeof(record_data));
    if (!trace) {
        test_error("trace create failed");
        return -1;
    }

    bio.init = ramio_init;
    bio.block_erase = ramio_erase;
    bio.chunk_write = ramio_write;
    bio.chunk_read = ramio_read;
    bio.config.chunk_size = 512;
    bio.config.chunks_per_block = 8;
    bio.config.num_cache_blocks = 64;
    bio.config.start_block = 8192;
    bio.config.end_block = 18432;
    
    /**
     * 5 ms one sample
     * 1 s 200 samples
     * 3 hour 2160000 samples
     * 1 block 204 samples, need 10589 blocks
     * set blocks to 
     */

    ret = blktrace_set_blockio(trace, &bio);
    if (ret != qe_ok) {
        test_error("set blockio error:%d", ret);
        return -1;
    }

    num_records = 4000000;
    while (num_records--) {
        blktrace_blockio_record(trace, &data, 1);
        qe_usleep(200);
    }

    return 0;
}