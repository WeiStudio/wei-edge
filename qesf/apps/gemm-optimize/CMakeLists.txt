add_executable(gemm_op gemm_op.c)
target_include_directories(
    gemm_op PRIVATE 
    ${PROJECT_ROOT}/include
    ${BUILD_DIR}/kconfig/include/generated)
target_link_libraries(gemm_op ${QESFLIB})

if(CONFIG_X86 AND CONFIG_X86_AVX)
    set(CMAKE_C_FLAGS "-mavx -mfma -mavx512f ${CMAKE_C_FLAGS}")
endif()

target_compile_options(gemm_op PRIVATE -g)