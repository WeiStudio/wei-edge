/*
 * @Author       : Wei.Studio
 * @Date         : 2023-01-30 10:48:35
 * @LastEditors: WeiStudio
 * @LastEditTime: 2023-02-03 11:42:32
 * @FilePath     : \qesf\apps\unit-test\utest.h
 * @Description  : 
 * 
 * Copyright (c) 2022 by Wei.Studio, All Rights Reserved. 
 */



#ifndef __UTEST_H__
#define __UTEST_H__



#include "qelib.h"



typedef enum {

    arg_help = 0,

    arg_loglevel,

    arg_fibonacci,

    arg_array_1d22d,

    arg_stringbuilder,
    arg_ringbuffer,
    arg_ringqueue,
    arg_minpq,
    arg_queue,
    arg_bitmap,
    arg_gbuf,
    arg_gbuf_pool,
#if defined(CONFIG_ROLLING_FILE)
    arg_rolling_file,
#endif
    arg_qestring,
    arg_lz4_test,
    arg_array_test,
    arg_list_test,
    arg_gb2312_utf8,
    arg_bpkg_test,
    arg_hash_test,
} utest_args_e;


#define UTEST_BPC_DIM_X         24
#define UTEST_BPC_DIM_Y         32
#define UTEST_BPC_NUM_POINTS    5 



#define UTEST_LOGNAME   "utest"
#define ut_debug(...)   qelog_debug(UTEST_LOGNAME,   __VA_ARGS__)
#define ut_info(...)    qelog_info(UTEST_LOGNAME,    __VA_ARGS__)
#define ut_notice(...)  qelog_notice(UTEST_LOGNAME,  __VA_ARGS__)
#define ut_warning(...) qelog_warning(UTEST_LOGNAME, __VA_ARGS__)
#define ut_error(...)   qelog_error(UTEST_LOGNAME,   __VA_ARGS__)
#define ut_fatal(...)   qelog_fatal(UTEST_LOGNAME,   __VA_ARGS__)



#endif /* __UTEST_H__ */