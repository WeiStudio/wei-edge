


#include "qelib.h"



#define VSCRYTPO_VERSION        "1.0"
#define VSCRYPTO_TOP_MAGIC      "QEVS"

typedef enum {
    VSCRYPTO_CRY_AES_CBC = 0,
    VSCRYPTO_CRY_AES_CFB,
    VSCRYPTO_CRY_AES_OFB,
    VSCRYPTO_CRY_AES_GCM,
} vscrypto_encrypto_e;

typedef enum {
    VSCRYPTO_CHK_CRC32 = 0,
    VSCRYPTO_CHK_MD5,
} vscrypto_chksum_e;

typedef enum {
    VSCRYPTO_AES_BITS_128 = 0,
    VSCRYPTO_AES_BITS_192,
    VSCRYPTO_AES_BITS_256,
} vscrypto_chksum_e;

/*
 * Batch Header (100 bytes)
 */
typedef struct {
    
    qe_u32 magic;
    
    struct {
        qe_u8 multi_frames:1;
        qe_u8 end_of_stream:1;
        qe_u8 reserved1:6;
    }flags;
    qe_u8  reserved2[3];

    qe_u32 frames;
    qe_u32 length;
    qe_u8  chk_mode;
    qe_u8  reserved3[11];

    qe_u8  aes_mode;
    qe_u8  aes_padding;
    qe_u8  aes_bits;
    qe_u8  reserved4;
    qe_u8  aes_key[32];
    qe_u8  aes_vec[16];
    qe_u32 plain_len;

    qe_u8  reserved4[16];
} vscrypto_batch_header_t;

typedef struct {
    vscrypto_batch_header_t head;
    qe_u32 checksum;
} vscrypto_package_t;

#define VSCRYPTO_M_ENC  0x01
#define VSCRYPTO_M_DEC  0x02
typedef struct vscrypto_ctx {
    qe_u8  aes_mode;
    qe_u16 aes_bits;
    qe_u8  aes_padding;
    qe_u8  mode;
    void  *user;
    qe_ret (*rsa_pub_key_create)(struct vscrypto_ctx *, void *);
    qe_ret (*rsa_pri_key_create)(struct vscrypto_ctx *, void *);
    qe_ret (*aes_key_create)(qe_u8 *, int);
    qe_ret (*aes_vec_create)(qe_u8 *);
    qe_ret (*rsa_encrypt)(qe_u8 *in, int size, qe_u8 *out);
    qe_ret (*rsa_decrypt)(struct vscrypto_ctx *, qe_u8 *in, int size, qe_u8 *out);
    qe_ret (*aes_encrypt)(struct vscrypto_ctx *, qe_in *key, qe_in *vec, qe_u8 *in, int size, qe_u8 *out);
    qe_ret (*aes_decrypt)(struct vscrypto_ctx *, qe_u8 *in, int size, qe_u8 *out);
    qe_u32 initialized;
} vscrypto_ctx_t;

typedef struct vscrypto_enc_ctx {
    qe_u8  aes_mode;
    qe_u16 aes_bits;
    qe_u8  aes_padding;
    void  *user;
    qe_ret (*rsa_pub_key_create)(struct vscrypto_ctx *, void *);
    qe_ret (*aes_key_create)(qe_u8 *, int);
    qe_ret (*aes_vec_create)(qe_u8 *);
    qe_ret (*rsa_encrypt)(qe_u8 *in, int size, qe_u8 *out);
    qe_ret (*aes_encrypt)(struct vscrypto_ctx *, qe_in *key, qe_in *vec, qe_u8 *in, int size, qe_u8 *out);
} vscrypto_enc_ctx_t;

typedef struct vscrypto_dec_ctx {
    void  *user;
    qe_ret (*rsa_pub_key_create)(struct vscrypto_ctx *, void *);
    qe_ret (*rsa_pri_key_create)(struct vscrypto_ctx *, void *);
    qe_ret (*rsa_decrypt)(qe_u8 *in, int size, qe_u8 *out);
    qe_ret (*aes_decrypt)(struct vscrypto_ctx *, qe_in *key, qe_in *vec, qe_u8 *in, int size, qe_u8 *out);
} vscrypto_dec_ctx_t;