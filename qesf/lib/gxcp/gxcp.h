/**
 * @file gxcp.h
 * @author Wei.Studio
 * @brief General X Control Protocol
 * @version 0.1
 * @date 2024-03-07
 * 
 * @copyright Copyright (c) 2024 Wei.Studio
 * 
 */



#ifndef __GXCP_H__  
#define __GXCP_H__



#define GX_TINY

#ifdef GX_TINY
#define GX_TINY_RW_LEN          4
#endif

#define GX_PREAMBLE             0x4758      /* String "GX" */
#define GX_PREAMBLE_BE          0x5847

#define gx_packed               __attribute__((packed))

typedef int (*gx_iowrite_func)(unsigned char *, unsigned int);
typedef int (*gx_readmem_cb)(unsigned int, unsigned short);
typedef int (*gx_writemem_cb)(unsigned int, unsigned short);

typedef enum
{
    GX_READMEM_CMD = 0,
    GX_READMEM_ACK,
    GX_WRITEMEM_CMD,
    GX_WRITEMEM_ACK,
    GX_EVENT_CMD,
    GX_EVENT_ACK,
    GX_READFILE_CMD,
    GX_READFILE_ACK,
    GX_WRITEFILE_CMD,
    GX_WRITEFILE_ACK,
    GX_COMMAND_SIZEOF,
} gx_command_e;

/**
 * Prefix
 */
struct gxcp_gx_prefix
{
#ifdef GX_TINY
    unsigned short preamble;
#else
    unsigned short preamble;
    unsigned char  checksum;
    unsigned char  reserved;
#endif
}gx_packed;
typedef struct gxcp_gx_prefix gx_prefix

/**
 * Common Command Data(CCD)
 * Command Packet Layer
 */
struct gxcp_ccd_cmd
{
#ifdef GX_TINY
    unsigned char command;
#else
    unsigned char  flags;
    unsigned char  command;
    unsigned short length;
    unsigned short request_id;
#endif
}gx_packed;
typedef struct gxcp_ccd_cmd gx_ccd_cmd

/**
 * Acknowledge Packet Layer
 */
typedef struct
{
#ifdef GX_TINY
    unsigned char  code;
#else
    unsigned char  code;
    unsigned char  command;
    unsigned short length;
    unsigned short request_id;
#endif
} gx_packed gx_ccd_ack;

typedef struct
{
    gx_prefix prefix;
    gx_ccd_cmd ccd;
} gx_packed gx_cmd_header;

typedef struct 
{
    gx_prefix prefix;
    gx_ccd_ack ccd;
} gx_packed gx_ack_header;

#define GXCP_MIN_LEN        (sizeof(gx_prefix) + sizeof(gx_ccd_cmd))
#ifdef GX_TINY
#define GXCP_MAX_DATA_SIZE  4
#else
#define GXCP_MAX_DATA_SIZE  512
#endif
#define GXCP_MAX_LEN        GXCP_MIN_LEN + GXCP_MAX_DATA_SIZE

/**
 * Read Memory Command
 */
typedef struct
{
    gx_prefix prefix;
    gx_ccd_cmd ccd;
    unsigned int   address;
#ifndef GX_TINY
    unsigned short length;
#endif
} gx_packed gx_readmem_cmd;

/**
 * Read Memory Acknowledge
 */
typedef struct 
{
    gx_prefix prefix;
    gx_ccd_ack ccd;
    unsigned char data[0];
} gx_packed gx_readmem_ack;


/**
 * Write Memory Command
 */
typedef struct
{
    gx_prefix prefix;
    gx_ccd_cmd ccd;
    unsigned int  address;
    unsigned char data[0];
} gx_packed gx_writemem_cmd;

/**
 * Write Memory Acknowledge
 */
typedef struct
{
    gx_prefix prefix;
    gx_ccd_ack ccd;
    unsigned short length;
} gx_packed gx_writemem_ack;



void gxcp_init(gx_iowrite_func wf, gx_readmem_cb rmcb, gx_writemem_cb wmcb);



#endif /* __GXCP_H__ */