/**
 * @file gxcp.c
 * @author Wei.Studio
 * @brief General X Control Protocol
 * @version 0.1
 * @date 2024-03-07
 * 
 * @copyright Copyright (c) 2024 Wei.Studio
 * 
 */



#include <stdio.h>

#include "gxcp.h"


#define gx_swap16(a)    ((((unsigned short)(a) & 0xff00) >> 8) | \
                        (((unsigned short)(a) & 0x00ff) << 8))

#define gx_swap32(a) 	((((unsigned int)(a) & 0xff000000) >> 24) | \
					    (((unsigned int)(a) & 0x00ff0000) >> 8) |   \
						(((unsigned int)(a) & 0x0000ff00) << 8) |   \
						(((unsigned int)(a) & 0x000000ff) << 24))

typedef struct
{
    gx_iowrite_func write_func;
    gx_readmem_cb readmem_cb;
    gx_writemem_cb writemem_cb
} gxcpm;

static gxcpm gxm = {
    .write_func  = NULL,
    .readmem_cb  = NULL,
    .writemem_cb = NULL,
};


/**
 * @brief match gxcp
 * 
 * @param buf: rx buffer pointer
 * @param len: rx buffer length
 * 
 * @return 0:not match  1:matched
 */
int gxcp_match(unsigned char *buf, unsigned int len)
{
    gx_cmd_header *hdr;

    if (!buf)
        return 0;

    if (len < GXCP_MIN_LEN)
        return 0;

    hdr = (gx_cmd_header *)buf;

    if (hdr->prefix.preamble != GX_PREAMBLE_BE)
        return 0;

    if (hdr->ccd.command >= GX_COMMAND_SIZEOF)
        return 0;

    return 1;
}

static int gx_readmem(gx_cmd_header *hdr, unsigned char *txbuf)
{
    unsigned int addr;
    unsigned short len;

    gx_readmem_cmd *cmd = (gx_readmem_cmd *)hdr;
    gx_readmem_ack *ack = (gx_readmem_ack *)txbuf;

    addr = gx_swap32(cmd->address);
#ifdef GX_TINY
    len = GX_TINY_RW_LEN;
#else
    len = gx_swap16(cmd->length);
#endif

    ack->prefix = hdr->prefix;
    ack->ccd.code = 0;

    if (gxm.readmem_cb)
        return gxm.readmem_cb(addr, len, ack->data) + sizeof(gx_readmem_ack);
    return sizeof(gx_readmem_ack);
}

static int gx_writemem(gx_cmd_header *hdr, unsigned char *txbuf)
{
    unsigned int addr;
    unsigned short len;
    unsigned short wlen;

    gx_writemem_cmd *cmd = (gx_writemem_cmd *)hdr;
    gx_writemem_ack *ack = (gx_writemem_ack *)txbuf;

    addr = gx_swap32(cmd->address);
#ifdef GX_TINY
    len = GX_TINY_RW_LEN;
#else
    len = gx_swap16(cmd->length);
#endif

    ack->prefix = hdr->prefix;
    ack->ccd.code = 0;

    if (gxm.writemem_cb) {
        wlen = gxm.writemem_cb(addr, len, cmd->data);
    } else {
        wlen = 0;
    }

    ack->length = gx_swap16(wlen);

    return sizeof(gx_writemem_ack);
}

int gxcp_write(unsigned char *buf, unsigned int len)
{
    if (!gxm.write_func)
        return 0;

    return gxm.write_func(buf, len);
}

int gxcp_read(unsigned char *buf, unsigned int len)
{   
    int n = 0;
    gx_cmd_header *hdr = (gx_cmd_header *)buf;
    unsigned char txbuf[GXCP_MAX_LEN];

    switch (hdr->ccd.command) {

    case GX_READMEM_CMD:
        n = gx_readmem(hdr, txbuf);
        break;

    case GX_WRITEMEM_CMD:
        n = gx_writemem(hdr, txbuf);
        break;

    default:
        /* Not suppprt command */
        break;
    }

    if (n)
        gxcp_write(txbuf, n);

    return n;
}



void gxcp_init(gx_iowrite_func wf, gx_readmem_cb rmcb, gx_writemem_cb wmcb)
{
    gxm.write_func = wf;
    gxm.readmem_cb = rmcb;
    gxm.writemem_cb = wmcb;
}