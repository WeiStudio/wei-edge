#define CONFIG_X86 1
//#define CONFIG_ENABLE_LOG 1
#define CONFIG_LOG_LEVEL_DEFAULT 1
#define CONFIG_LOG_BUF_SIZE 256
#define CONFIG_LOG_DOMAIN_NAME_SIZE 16
#define CONFIG_LOG_DEFAULT_OUTPUT 1
#define CONFIG_LOG_DEFAULT_HANDLER 1
#define CONFIG_INITCALL 1
#define CONFIG_ROLLING_FILE 1
#define CONFIG_MEM_BGET 1
#define CONFIG_THREAD 1
