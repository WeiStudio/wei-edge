****************
Quick Start
****************

依赖
================
* CMake(必须) qelib目前只支持使用CMake进行构建
* Python3.X(可选) Kconfig以及menuconfig需要
* Sphinx(可选) 文档构建需要

下载
================
从svn checkout qelib仓库到本地，svn地址为

.. code-block:: shell

    svn://192.168.0.1/QE_SYB_StdLib/software/qelib

CMake构建
================
克隆仓库后，进入qelib文件夹下

.. code-block:: shell

    $ cd qelib

CMake需要在一个编译目录下运行，而qelib会使用这个目录名称作为目标名称(target_name)，
最终生成的库名称与target_name是相关联的，和安装目录也是相关的，因此推荐为不同目标平台
创建一个二级编译目录，在二级目录中进行CMake构建以及编译。在类Unix环境中进行CMake构建
使用如下命令

.. code-block:: shell

    $ mkdir -p build/target_name
    $ cd build/target_name
    $ cmake ../../

Windows MinGW环境中CMake构建请使用如下命令

.. code-block:: shell

    $ cmake ../../ -G "Unix Makefiles"

以上情况适用于主机端构建(Host Building)，如果你希望进行交叉编译(Cross Building)，则
需要指定交叉编译cmake脚本，qelib在/cmake/toolchain路径下预先准备了几个交叉编译脚本，
你也可以自定义脚本

.. code-block:: shell

    $ cmake ../../ -DCMAKE_TOOLCHAIN_FILE=../../cmake/toolchain/xxx.cmake

配置
================
CMake构建完成后，可以对源码的功能进行配置，包括了某些功能的使能与否，以及某些模块的默认参数。
CMake在构建过程中会选择默认配置，如果不需要手动配置，也可以跳过配置阶段

qelib支持kconfig和hconfig，关于配置相关内容在配置文档中会详细介绍。在CMake构建阶段会自动
检测是否支持kconfig，优先选择kconfig配置方式，如果不支持kconfig，才会使用hconfig方式。
在CMake执行过程中会将使用的配置方式输出显示

.. code-block:: shell

    # 使用kconfig
    $ -- Using kconfig
    # 使用hconfig
    $ -- Using hconfig

如果是使用kconfig方式，则可以使用以下命令进入图形化配置

.. code-block:: shell

    $ make menuconfig

如果是使用hconfig方式，则需要自己手动编写一个配置头文件xxx_defconfig.h并将其覆盖编译目录
下自动生成的generated/qe_autoconfig.h，这需要你清楚的知道qelib中每个配置项代表什么含义。
qelib在/configs目录下预先准备了一些配置头文件

.. code-block:: shell

    $ cp ../../configs/xxx_defconfig.h generated/qe_autoconfig.h

编译源码
================
直接运行以下命令进行编译

.. code-block:: shell

    $ make 

make后跟"-j"可以指定多线程并行编译，取决于机器的CPU数量

.. code-block:: shell

    $ make -j8

编译文档
================
如果你的环境中正确安装了Python3.X和Sphinx，且在CMake构建阶段被正确检测到，可以在CMake执
行的输出信息中看到以下内容

.. code-block:: shell

    $ -- Python 3.11.7 found
    $ -- Sphinx found

CMake检测到以上内容，你可以执行以下命令编译文档

.. code-block:: shell

    $ make docs

编译后默认生成HTML形式的文档，在/docs/sphinx_html目录下

安装
================
使用如下命令进行安装，会将头文件和库导入到安装路径下，安装路径默认
为/release/version/target_name

.. code-block:: shell

    $ make install

在你的工程中使用qelib
========================
生成的qelib库可以在不同的构建环境中使用，这里仅以使用CMake构建应用程序举例

.. code-block:: cmake

    set(QELIB_DIR "xxx/release/version/target_name")
    set(QELIB_INCLUDE ${QELIB_DIR}/include)
    set(QELIB_LIBRARY ${QELIB_DIR}/lib/libqe-xxx.a)

Linux系统开发
----------------

在Linux系统应用程序中使用qelib的典型main.c如下

.. code-block:: c

    // 引用qelib头文件
    #include qelib.h"

    /*
     * Assert Hook函数
     * 当qelib库主动抛出断言时，将函数名、条件内容和行号传递到hook
     */
    static void assert_hook(const char *func, const char *ext, qe_size line)
    {
        qe_fatal("Assert : %s %s %d", func, ext, line);
        while (1);
    }

    int main(int argc, char **argv[])
    {
        /*
         * 初始化Logger
         * 日志等级为DEBUG
         * 输出带颜色、等级、日志域名称和日期
         * 使能归档
         * 默认输出到printf
         */ 
        qelog_init(QELOG_DEBUG, QELOG_LV   |
                                QELOG_CL   |
                                QELOG_DM   |
                                QELOG_DATE |
                                QELOG_AR);
        /*
         * 设置日志归档为当前目录
         * 日志文件为test.log
         * 回滚文件个数为4，回滚大小为32KB
         * 压缩文件上限为1MB
         */ 
        qelog_set_archive(".", "test", "log", 4, 1024*32, 1024*1024);

        // 设置Assert hook函数
        qe_assert_set_hook(assert_hook);

        qe_notice("XXX Running");
        qe_notice("Build %s %s", __DATE__, __TIME__);

        // 执行应用程序处理
        ......
    }

FreeRTOS系统
----------------
在FreeRTOS中使用qelib的典型main.c如下

.. code-block:: c

    #include "FreeRTOS.h"
    #include "task.h"
    // 引用qelib头文件
    #include "qelib.h"



    // Assert hook
    static void qe_assert_hook(const char *ex, const char *func, qe_size line)
    {
        qe_fatal("Assert : %s %s %d", func, ext, line);
        while (1);
    }

    // 使用FreeRTOS的内存申请释放接口创建内存分配器
    static qemem_allocator sys_allocator = {
        .malloc   = pvPortMalloc,
        .calloc   = QE_NULL,
        .realloc  = QE_NULL,
        .free     = vPortFree,
        .pool     = QE_NULL,
    };

    static void mem_init(void)
    {
        // 指定内存分配器
        qemem_set_allocator(&sys_allocator);
    }

    // 日志输出回调函数，message就是已格式化处理后的字符串
    static void log_output(const char *message)
    {
        int i = 0;
        int len = qe_strlen(message);
        // 例如平台支持串口输出字符，则循环字符串输出每个字符到指定串口
        for (i=0; i<len; i++) {
            uart_send_char(UARTx, message[i]);
        }
    }

    static void log_init(void)
    {
        // 初始化日志
        qelog_init(QELOG_DEBUG, QELOG_LV|QELOG_DM);
        /*
         * 设置日志输出方式
         * 如果配置使能了CONFIG_LOG_DEFAULT_OUTPUT，则也可以不需要设置
         * 手动设置输出方式的好处是可以任意选择从哪个串口输出
         */
        qelog_set_default_output(log_output);
    }

    int main(void)
    {
        // 内存初始化
        mem_init();

        // 日志初始化
        log_init();

        // 设置Assert hook
        qe_assert_set_hook(assert_hook);

        // 如果平台支持修改链接脚本，可以使用initcall，自动调用所有隐式注册的模块
        qe_notice("do initcall");
        qe_initcall();

        // FreeRTOS调度器启动
        qe_notice("scheduler start");
        vTaskStartScheduler();
        qe_fatal("scheduler exit");

        /* Forever Loop */
        for (;;)
            ;

        return 0;
    }

某个task的入口写法

.. code-block:: c

    static qe_init int xxx_task_init(void)
    {
        BaseType_t ret;

        ret = xTaskCreate(xxx_task, "xxx", 0x400, QE_NULL, 3, &task_handle);
        if (ret != pdPASS) {
            qe_error("create xxx task err:%d", ret);
            return -1;
        }
        return 0;
    }
    // 导出xxx_task_init符号到initcall
    QE_APP_EXPORT(xxx_task_init);

裸机
--------
如果是裸机开发，且不支持动态内存分配，main函数可以按如下方式编写

.. code-block:: c
    
    #include "qelib.h"


    static char heap[HEAP_SIZE];

    // Assert hook
    static void qe_assert_hook(const char *ex, const char *func, qe_size line)
    {
        qe_fatal("Assert : %s %s %d", func, ext, line);
        while (1);
    }

    static void mem_init(void)
    {
        /*
         * 指定静态堆内存
         * 前提是CONFIG_BGET和CONFIG_MEM_BGET使能，使用bget作为内存分配器
         * 设置后就可以使用动态内存
         */ 
        qemem_set_pool(heap, HEAP_SIZE);
    }

    // 日志输出回调函数，message就是已格式化处理后的字符串
    static void log_output(const char *message)
    {
        int i = 0;
        int len = qe_strlen(message);
        // 例如平台支持串口输出字符，则循环字符串输出每个字符到指定串口
        for (i=0; i<len; i++) {
            uart_send_char(UARTx, message[i]);
        }
    }

    static void log_init(void)
    {
        // 初始化日志
        qelog_init(QELOG_DEBUG, QELOG_LV|QELOG_DM);
        /*
         * 设置日志输出方式
         * 如果配置使能了CONFIG_LOG_DEFAULT_OUTPUT，则也可以不需要设置
         * 手动设置输出方式的好处是可以任意选择从哪个串口输出
         */
        qelog_set_default_output(log_output);
    }

    int main(void)
    {
        // 内存初始化
        mem_init();

        // 日志初始化
        log_init();

        // 设置Assert hook
        qe_assert_set_hook(assert_hook);

        // 如果平台支持修改链接脚本，可以使用initcall，自动调用所有隐式注册的模块
        qe_notice("do initcall");
        qe_initcall();

        // 程序处理
        ......
    }
