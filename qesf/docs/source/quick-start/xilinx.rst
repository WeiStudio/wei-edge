********************
Xilinx SDK使用qelib
********************

创建库工程
------------

点击File->New->Other，在向导页面选择Xilinx->Library Project

.. image:: /images/quick-start-01.png

输入工程名，一般为qelib

.. image:: /images/quick-start-02.png

点击finish，创建完成。会在sdk工程目录下出现qelib目录

.. image:: /images/quick-start-03.png

拷贝库文件
------------

拷贝qelib库文件libqe-zynq.a和头文件include文件夹到刚创建的库工程目录中

.. image:: /images/quick-start-04.png

修改编译选项
------------

在应用工程上右击，选择 C/C++ Build Settings，弹出如下窗口

.. image:: /images/quick-start-05.png

在 C/C++ Build->Settings->Tool Settings 子页面中，选择
ARM v7 gcc compiler->Directories，添加qelib头文件路径

.. image:: /images/quick-start-06.png

在 ARM v7 gcc linker选项中，分别添加库名称和库文件路径

.. image:: /images/quick-start-07.png