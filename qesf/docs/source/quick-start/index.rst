***************
快速上手
***************

.. toctree::
   :maxdepth: 1

    介绍 <intro.rst>
    Jetson平台 <jetson.rst>
    Xilinx SDK使用qelib <xilinx.rst>