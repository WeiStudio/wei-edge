****************
Configuration
****************

qelib支持对源码进行配置，以自定义所需要的功能和相关组件的默认参数，目前支持以下两种配置方式

* kconfig：类似Linux/uboot使用各层级Kconfig进行配置，可以使用menuconfig进行图形化配置
* hconfig：通过头文件进行配置

kconfig
========
kconfig依赖Python3.X，在执行cmake阶段会自动检测当前环境中是否存在Python3.X，如果存在则
配置方式为kconfig。cmake执行过程中会输出检测结果

.. code-block:: shell

    # 检测到Python环境
    -- Python 3.11.7 found
    ...
    -- Using kconfig

    # 未检测到Python环境
    -- Python not found
    ...
    -- Using hconfig

当cmake执行过程检测到Python环境，确认使用kconfig后，会在后续的cmake过程中自动执行一次
kconfig，也就是自动分析各目录层级中的Kconfig文件，kconfig过程需要一个输入配置文件，在首
次创建编译目录第一次执行cmake时，会以/configs/defconfig文件作为默认输入。kconfig执行
完成后会在编译根目录下生成.config文件，在编译目录下的generated路径下生成autoconfig.h
配置头文件，在进行make时会自动检测.config是否经过修改，修改则重新执行kconfig生成新的
autoconfig.h

在首次执行cmake后，再任意时刻想修改配置，有3种方式

* 手动编辑.config文件
* 从/configs路径下拷贝一个预先准备的默认配置文件覆盖.config
* make menuconfig进入图形化配置界面修改

.. note::

    注意请不要通过直接修改autoconfig.h文件来修改配置，该文件为自动生成

kconfig配置文件、配置头文件、.config、kconfig、menuconfig、autoconfig.h的
关系如下图所示

.. image:: ../images/config-01.png

输入如下命令进入图形化配置界面

.. code-block:: shell

    $ make menuconfig

会在终端显示如下界面，可以进行某个功能的使能或参数设置

.. image:: ../images/config-02.png

hconfig
========

hconfig是为没有Python3.X的环境准备的配置方式，首先在cmake阶段会将/configs/defconfig.h
自动拷贝到generated目录下重命名为autoconfig.h，后续用户如果想修改配置，则可以直接编辑
autoconfig.h，或者使用其他配置文件覆盖autoconfig.h的内容