***************
Device Driver
***************

.. toctree::
   :maxdepth: 1

   设备模型 <device-model.rst>
   UART <uart/index.rst>
   SPI <spi/index.rst>
   SPI Flash <spi/spi-flash.rst>
   Timer <timer/index.rst>
   I2C <i2c/index.rst>
   Logic <logic/index.rst>
