********
UART设备
********

本文从驱动程序和应用程序两个方面分别介绍如何在QESF中使用UART设备

UART设备的应用程序
=====================

应用程序使用设备访问层的API来访问UART设备，与UART设备相关的设备访问层API如下

+-----------------------------+--------------------------+
| 接口                        | 描述                     |
+=============================+==========================+
| qe_dev_find()            | 查找并获取UART设备句柄   |
+-----------------------------+--------------------------+
| qe_dev_open()            | 打开UART设备             |
+-----------------------------+--------------------------+
| qe_dev_read()            | 读取UART设备的数据       |
+-----------------------------+--------------------------+
| qe_dev_write()           | 向UART设备写入数据       |
+-----------------------------+--------------------------+
| qe_device_ioctl()           | 控制UART设备             |
+-----------------------------+--------------------------+
| qe_dev_set_rx_indicate() | 设置UART设备接收回调     |
+-----------------------------+--------------------------+
| qe_dev_set_tx_complete() | 设置UART设备发送完成回调 |
+-----------------------------+--------------------------+
| qe_dev_close()           | 关闭UART设备             |
+-----------------------------+--------------------------+

查找和打开UART设备
----------------------

应用程序首先通过 ``qe_dev_find()`` 查找接口获取UART设备，建议UART设备名称使用"uart0"、"uart1"等。获取UART设备后，通过 ``qe_dev_open()`` 打开UART设备，UART设备可用的打开模式如下

.. code-block:: c

    #define QE_DEV_F_STREAM     0x040   /* 流模式 */
    #define QE_DEV_F_INT_RX     0x100   /* 中断接收模式 */
    #define QE_DEV_F_DMA_RX     0x200   /* DMA接收模式 */
    #define QE_DEV_F_INT_TX     0x400   /* 中断发送模式 */
    #define QE_DEV_F_DMA_TX     0x800   /* DMA发送模式 */

UART设备的数据收发有中断、轮询和 DMA 3种模式，这3种模式只能使用其中一种，如果打开时未指定模式，默认情况下使用轮询模式

以中断接收轮询发送模式打开UART设备的代码示例如下

.. code-block:: c

    qe_dev dev;

    dev = qe_dev_find("uart0");

    qe_dev_open(dev, QE_DEV_F_INT_RX);

以DMA接收轮询发送模式打开UART设备的代码示例如下

.. code-block:: c

    qe_dev dev;

    dev = qe_dev_find("uart0");

    qe_dev_open(dev, QE_DEV_F_DMA_RX);

对UART设备的设置和控制
-------------------------

应用程序通常需要对UART设备的波特率、数据位、校验位、接收缓冲区大小、停止位等进行设置。在QESF的设备核心层，对UART设备类型抽象了Serial设备类，并对UART设备的配置提供了统一的方式，通过向 ``qe_device_ioctl()`` 接口传递命令 ``QE_DEV_CTRL_CONFIG`` 以及携带参数 ``struct qe_serial_configure`` 实现。结构体 ``struct qe_serial_configure`` 的定义如下

.. code-block:: c

    struct qe_serial_configure 
    {
        qe_u32 baud_rate;              /* 波特率 */
        qe_u32 data_bits       :4;     /* 数据位 */
        qe_u32 stop_bits       :2;     /* 停止位 */
        qe_u32 parity          :2;     /* 奇偶校验位 */
        qe_u32 bit_order       :1;     /* 字节序 */
        qe_u32 invert          :1;     /* 模式 */
        qe_u32 bufsz           :16;    /* 接收缓冲区大小 */
        qe_u32 reserved        :6;
    };

UART设备波特率的配置值可选择以下参数

.. code-block:: c

    #define BAUD_RATE_2400                  2400
    #define BAUD_RATE_4800                  4800
    #define BAUD_RATE_9600                  9600
    #define BAUD_RATE_19200                 19200
    #define BAUD_RATE_38400                 38400
    #define BAUD_RATE_57600                 57600
    #define BAUD_RATE_115200                115200
    #define BAUD_RATE_230400                230400
    #define BAUD_RATE_460800                460800
    #define BAUD_RATE_921600                921600
    #define BAUD_RATE_2000000               2000000
    #define BAUD_RATE_3000000               3000000

UART设备的数据位可选以下参数

.. code-block:: c

    #define DATA_BITS_5                     5
    #define DATA_BITS_6                     6
    #define DATA_BITS_7                     7
    #define DATA_BITS_8                     8
    #define DATA_BITS_9                     9    

停止位可选以下参数

.. code-block:: c

    #define STOP_BITS_1                     0
    #define STOP_BITS_2                     1
    #define STOP_BITS_3                     2
    #define STOP_BITS_4                     3

极性可选以下参数

.. code-block:: c

    #define PARITY_NONE                     0
    #define PARITY_ODD                      1
    #define PARITY_EVEN                     2

字节序可选以下参数

.. code-block:: c

    #define BIT_ORDER_LSB                   0
    #define BIT_ORDER_MSB                   1

当UART设备的接收模式为中断方式时，在应用程序调用UART设备的打开接口时，设备核心层的Serial类动态创建，用于驱动框架在UART设备的中断服务例程里将数据暂存在缓冲区。接收缓冲区默认大小为64

.. code-block:: c

    #define QE_SERIAL_RBSZ					64

.. note:: 

    默认的UART接收缓冲区大小为64。Serial创建的缓冲区是RingBuffer型的，若一次接收的中断字节较多，而应用程序没有及时读取，那么缓冲区数据满后新接收的数据会覆盖老的数据，造成数据丢失。应用程序在使用UART设备时需考虑这一点，在可能存在覆盖的情况下应通过ioctl接口调大缓冲区大小。由于接收缓冲区大小是在UART设备打开时动态申请的，Serial不支持缓冲区动态修改大小，因此要调整缓冲区大小，必须在打开之前进行ioctl。

Serial提供了默认的UART设备配置

.. code-block:: c

    #define QE_SERIAL_DEFCONFIG		            \
    {						    \
        BAUD_RATE_115200, /* 115200 bits/s */	    \
        DATA_BITS_8,	  /* 8 databits */	    \
        STOP_BITS_1,	  /* 1 stopbit */           \
        PARITY_NONE,	  /* No parity  */	    \
        BIT_ORDER_LSB,				    \
        NRZ_NORMAL,				    \
        QE_SERIAL_RBSZ,			            \
        0			                    \
    }

如果应用程序使用UART设备时配置与默认配置不相同，可以按照如下代码示例进行设置

.. code-block:: c

    qe_dev dev;
    qe_serial_configure config;

    /* step1: 查找UART设备 */
    dev = qe_dev_find("uart0");

    /* step2: 准备UART设备配置参数 */
    config = QE_SERIAL_DEFCONFIG;
    config.baud_rate = BAUD_RATE_9600;
    config.bufsz = 128;

    /* step3: 设置UART设备配置参数 */
    qe_device_ioctl(dev, QE_DEV_CTRL_CONFIG, &config);

    /* step4: 中断接收模式打开UART设备 */
    qe_dev_open(dev, QE_DEV_F_INT_RX);

UART设备的数据发送
--------------------

应用程序通过 ``qe_dev_write()`` 接口向UART设备发送数据，在UART设备的使用中，该接口的 ``pos`` 参数未使用，设置为0即可。当发送模式为DMA或中断时，可通过 ``qe_dev_set_tx_complete()`` 接口设置发送完成回调。

UART设备的数据接收
--------------------

应用程序通过 ``qe_dev_read()`` 接口从UART设备读取接收的数据，在UART设备的使用中，该接口的 ``pos`` 参数未使用，设置为0即可。一般典型的UART设备使用中断接收模式并配合接收回调函数以完成数据接收，可通过 ``rt_device_set_rx_indicate()`` 接口设置接收回调。

关闭UART设备
---------------

应用程序完成UART设备使用后，可通过 ``qe_dev_close()`` 接口关闭UART设备。

Example:中断接收轮询发送
-----------------------------

以下示例代码展示应用程序最为常见的UART设备使用方法，即通过中断触发数据接收，通过轮询方式发送数据。核心点在于在接收回调中利用回调参数获取待读取的数据长度，然后通过消息队列触发任务读取数据。主要步骤为

1. 查找UART设备获取设备句柄
2. 创建消息队列
3. 以中断接收轮询发送的模式打开UART设备
4. 设置UART设备的接收回调
5. 任务阻塞等待消息队列，当接收到消息时从UART设备读取数据，并将数据再发送给UART设备
6. 接收回调中保存接收长度信息，并向消息队列发送消息

.. code-block:: c

    #include "FreeRTOS.h"
    #include "task.h"
    #include "qelib.h"

    static TaskHandle_t ucr_tsk;
    static QueueHandle_t queue;                 /* 消息队列 */
    static qe_dev dev;                     
    static qe_u32 rx_indicate_size = 0;    /* 待接收数据长度 */
    static rx_buffer[64] = {'\0'};              /* 应用层接收缓冲区 */

    /* 接收回调函数 */
    static qe_ret rx_indicate(struct qe_device *dev, qe_size size)
    {
        qe_u8 event;
        BaseType_t woken = pdFALSE;

        /* 暂存待读取的数据长度 */
        rx_indicate_size = size;

        /* 向消息队列发送消息以唤醒主循环，注意当前在中断上下文中 */
        xQueueSendFromISR(queue, &event, &woken);
        portYIELD_FROM_ISR(woken);

        return qe_ok;
    }

    static void uart_task(void *data)
    {
        qe_u8 event;
        qe_u32 read_bytes;

        /* 查找UART设备获取设备句柄 */
        dev = qe_dev_find("uart0");
        if (!dev) {
            qelog_err("uart", "uart0 not found");
            goto __exit;
        }

        /* 创建消息队列 */
        queue = xQueueCreate(16, 1);

        /* 以中断接收轮询发送模式打开UART设备 */
        qe_dev_open(dev, QE_DEV_F_INT_RX);

        /* 设置接收回调函数 */
        qe_dev_set_rx_indicate(dev, rx_indicate);

        /* 主循环 */
        while (1) {
            /* 阻塞等待消息队列 */
            if (pdPASS == xQueueReceive(queue, &event, pdMS_TO_TICKS(10))) {
                if (rx_indicate_size > 0) {
                    /* 数据已接收，开始读取 */
                    read_bytes = qe_dev_read(dev, 0, rx_buffer, rx_indicate_size);
                    rx_indicate_size = 0;
                    /* 发送数据给UART设备 */
                    qe_dev_write(dev, 0, rx_buffer, read_bytes);
                }
            }
        }

    __exit:
        vTaskDelete(QE_NULL);
    }

    static int uart_task_launch(void)
    {
        /* 创建任务 */
        xTaskCreate(uart_task, "uart", 200, QE_NULL, 1, &ucr_tsk);
        return 0;
    }
    /* 向QESF注册APP启动函数 */
    QE_APP_EXPORT(uart_task_launch);


UART设备的驱动程序
====================
    
    在设备核心层，QESF为UART设备抽象了serial设备对象 ``qe_serial_device`` ，定义如下

.. code-block:: c

    struct qe_serial_device {
        
        struct qe_device          	parent;     /* 设备对象 */

        const struct qe_uart_ops 	*ops;       /* UART设备操作集 */

        struct qe_serial_configure   config;    /* UART设备配置 */

        void *rx;       /* 接收数据域 */
        void *tx;       /* 发送数据域 */
    };
    typedef struct qe_serial_device qe_serial_t;

UART设备驱动程序的编写，需要在Serial的框架下进行，主要需要实现的内容如下

* 创建serial设备实例
* 实现Serial设备的ops操作集
* 将serial设备注册到Serial框架中

创建serial设备并注册到Serial框架
----------------------------------

驱动程序中必须以静态变量的方式创建serial实例，代码如下

.. code-block:: c

    static struct qe_serial_device serial0;     /* serial设备实例 */

    static int uart_driver_init(void)
    {
        /* 设置serial设备的操作集 */
        serial0.ops = &uart_ops;

        /* 注册serial设备，工作模式为中断接收轮询发送 */
        qe_serial_register(&serial0, "uart0",
            QE_DEV_F_RDWR|QE_DEV_F_INT_RX, &uart0_hw);

        return qe_ok;
    }
    /* 向QESF注册UART设备驱动初始化 */
    QE_BOARD_EXPORT(uart_driver_init);

向Serial框架中注册serial设备实例的接口为

.. code-block:: c

    qe_ret qe_serial_register(qe_serial_t *serial, const char *name, qe_u32 flag, void *data);

* 入参
    * serial：serial设备实例
    * name：serial设备的名称
    * flag：设备模式
    * data：私有数据
* 返回值
    * 成功：qe_ok
    * 失败：错误值

serial设备名称与应用程序调用 ``qe_dev_find()`` 接口查找设备的名称相对应，驱动程序中使用什么名称注册，应用程序中就使用什么名称查找。驱动程序中注册的设备模式需与应用程序的打开模式一致。私有数据主要用于驱动程序的各个操作集函数中获取驱动程序私有的数据。

serial设备操作集
--------------------

serial设备操作集定义如下

.. code-block:: c

    struct qe_uart_ops {
        /* serial设备配置接口 */
        qe_ret (*configure)(struct qe_serial_device *serial, struct qe_serial_configure *cfg);
        /* serial设备控制接口 */
        qe_ret (*control)(struct qe_serial_device *serial, int cmd, void *arg);
        /* serial设备发送一个字符接口 */
        int (*putc)(struct qe_serial_device *serial, char c);
        /* serial设备接收一个字符接口 */
        int (*getc)(struct qe_serial_device *serial);
        /* serial设备DMA传输接口 */
        qe_size (*dma_transmit)(struct qe_serial_device *serial, qe_u8 *buf, 
            qe_size size, int direction);
    };

* configure：serial设备的配置接口，应用程序通过ioctl对UART设备进行配置时，底层会调用到该接口。驱动程序根据传入的配置参数对UART设备的波特率、数据位等进行配置
* control：serial设备的控制接口
* putc：发送一个字符的操作
* getc：接收一个字符的操作

.. note:: 
    getc操作中需对UART设备的Fifo中是否存在数据进行检查，如果不存在数据，返回值需设置为-1。putc操作发送成功需返回1

驱动程序中，serial设备的操作集通常也是以静态变量的方式创建

.. code-block:: c

    static struct qe_uart_ops uart_ops = {
        uart_configure,
        uart_control,
        uart_putc,
        uart_getc,
    };

serial设备的中断接收处理
----------------------------

应用层将UART设备以中断接收模式打开时，Serial框架会调用驱动程序的control传入中断使能指令 ``QE_DEV_CTRL_SET_INT`` ，驱动程序需在control中实现UART设备的中断使能以及中断函数注册。应用层关闭UART设备时，Serial框架会调用驱动程序的control传入中断使能指令 ``QE_DEV_CTRL_CLR_INT`` ，驱动程序需实现UART设备的中断禁用以及中断函数注销。代码示例如下

.. code-block:: c

    static qe_ret uart_control(struct qe_serial_device *serial, int cmd, void *arg)
    {
        switch (cmd)
        {
        case QE_DEV_CTRL_CLR_INT:
            /* disable rx irq and unregister irq handle */
            vPortDisableInterrupt(irq);
            break;

        case QE_DEV_CTRL_SET_INT:
            /* enable rx irq and register irq handle */
            xPortInstallInterruptHandler(irq, uart_isr, serial);
            vPortEnableInterrupt(irq);
            break;
        }

        return qe_ok;
    }

Serial框架为驱动程序提供了一个预定义中断服务函数

.. code-block:: c

    void qe_hw_serial_isr(struct qe_serial_device *serial, int event);

* 入参
    * serial：serial设备
    * event：事件

中断事件定义如下

.. code-block:: c

    #define QE_SERIAL_EVENT_RX_IND          0x01    /* Rx indication */
    #define QE_SERIAL_EVENT_TX_DONE         0x02    /* Tx complete   */
    #define QE_SERIAL_EVENT_RX_DMADONE      0x03    /* Rx DMA transfer done */
    #define QE_SERIAL_EVENT_TX_DMADONE      0x04    /* Tx DMA transfer done */
    #define QE_SERIAL_EVENT_RX_TIMEOUT      0x05    /* Rx timeout    */

UART设备驱动的接收中断可以使用Serial框架提供的 ``qe_hw_serial_isr()`` 接口来处理中断，或者自定义中断处理函数。如果使用Serial框架提供的 ``qe_hw_serial_isr()`` 接口来处理中断，在该接口内部会将从UART设备读取的数据暂存在框架创建的接收缓冲区中，然后自动调用应用层注册的接收回调函数，通知应用层数据到来。使用Serial框架的UART设备接收中断代码示例如下

.. code-block:: c

    static void uart_isr(void *data)
    {
        /* 调用Serial框架的中断服务接口，并传入QE_SERIAL_EVENT_RX_IND */
        qe_hw_serial_isr(serial, QE_SERIAL_EVENT_RX_IND);
    }

如果不使用Serial框架提供的中断服务接口，驱动程序在中断中需自行从UART设备读取数据暂存到Serial框架创建的接收缓冲区中，并自行调用应用层注册的接收回调函数，代码示例如下

.. code-block:: c

    static void uart_isr(void *data)
    {
        struct qe_serial_device *serial = (struct qe_serial_device *)data;
        /* 获取Serial框架创建的接收缓冲区指针 */
        struct qe_serial_rxfifo *rxfifo = (struct qe_serial_rxfifo *)serial->rx;

        char c;
        int rx_length;
        while (/* 检查UART设备是否存在数据 */) {
            /* 从UART设备读取一个字节并存入接收缓冲区 */
            rxfifo->buffer[rxfifo->put_index] = uart->fifo;
            /* 接收缓冲区索引后移 */
            rxfifo->put_index += 1;
            /* 接收缓冲区满处理 */
            if (rxfifo->put_index >= serial->config.bufsz) rxfifo->put_index = 0;
        }

        /* 计算接收缓冲区内数据长度 */
        rx_length = (rxfifo->put_index >= rxfifo->get_index)? (rxfifo->put_index - rxfifo->get_index):
                                        (serial->config.bufsz - (rxfifo->get_index - rxfifo->put_index));
        /* 通知应用程序 */
        if (rx_length)
            serial->parent.rx_indicate(&serial->parent, rx_length);
    }

