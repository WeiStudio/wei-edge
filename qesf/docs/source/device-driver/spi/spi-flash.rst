****************
SPI Flash
****************

SPI Flash在操作上有很多共性的地方，为了便于驱动和应用层进行SPI Flash操作，QESF对SPI Flash进行了抽象。在应用程序编写时，操作除了设备访问层的通用API，为了便于SPI Flash，QESF提供了专用于SPI Flash的操作接口，以下是应用层SPI Flash相关接口介绍

+------------------------+-------------------------------------+
|          接口          |                描述                 |
+========================+=====================================+
| qe_dev_find            | 查找SPI Flash设备并获取设备句柄     |
+------------------------+-------------------------------------+
| qe_dev_read            | 以块操作的方式从SPI Flash读取数据   |
+------------------------+-------------------------------------+
| qe_dev_write           | 以块操作的方式向SPI Flash写入数据   |
+------------------------+-------------------------------------+
| qe_spiflash_read       | 以字节操作的方式从SPI Flash读取数据 |
+------------------------+-------------------------------------+
| qe_spiflash_write      | 以字节操作的方式向SPI Flash写入数据 |
+------------------------+-------------------------------------+
| qe_spiflash_chip_erase | SPI Flash芯片擦除                   |
+------------------------+-------------------------------------+
| qe_spiflash_erase      | SPI Flash块擦除                     |
+------------------------+-------------------------------------+

使用设备访问层通用API进行SPI Flash操作时，读写操作都是以块为单位进行的， ``qe_dev_read()`` 接口参数中的 ``pos`` 表示第几个块， ``size`` 表示几个块大小，而不是表示真正的字节数，使用 ``qe_dev_write()`` 接口进行写操作时同理。示例如下

.. code-block:: c

    qe_dev dev;
    char buffer[4096];

    /* 查找并获取SPI Flash设备句柄 */
    dev = qe_dev_find("w25qxxx");

    /* 从SPI Flash的第一个块读取数据 */
    qe_dev_read(dev, 0, buffer, 1);

    /* 向SPI Flash的第一个块写入数据 */
    qe_dev_write(dev, 0, buffer, 1);

以上代码中假设操作的SPI Flash设备块大小为4096，真正的SPI Flash块大小由SPI Flash核心层决定，后文会详细说明。

使用SPI Flash专用API
=======================

``qe_spiflash_read()`` 接口定义如下

.. code-block:: c

    qe_size qe_spiflash_read(qe_spiflash_t  flash, 
                               qe_u32    addr,
                               qe_size      size,
                               void          *buffer);    

* 入参
    * flash：SPI Flash设备
    * addr：读取地址
    * size：读取字节数
    * buffer：读缓冲区
* 返回值
    * 成功：成功读取的字节数
    * 失败：-1，错误值从errno获取

``qe_spiflash_write()`` 接口定义如下

.. code-block:: c

    qe_size qe_spiflash_write(qe_spiflash_t  flash, 
                                qe_u32    addr,
                                qe_size      size,
                                void          *buffer);    

* 入参
    * flash：SPI Flash设备
    * addr：写入地址
    * size：写入字节数
    * buffer：写缓冲区
* 返回值
    * 成功：成功写入的字节数
    * 失败：-1，错误值从errno获取

``qe_spiflash_erase()`` 接口定义如下

.. code-block:: c

    qe_ret qe_spiflash_erase(qe_spiflash_t flash, qe_u32 addr, qe_size size);

* 入参
    * flash：SPI Flash设备
    * addr：擦除地址
    * size：擦除字节数
* 返回值
    * 成功：qe_ok
    * 失败：错误值

使用SPI Flash专用API操作的示例如下

.. code-block:: c

    qe_dev dev;
    char buffer[4096];

    /* 查找并获取SPI Flash设备句柄 */
    dev = qe_dev_find("w25qxx");

    /* 从0地址读取4096字节 */
    qe_spiflash_read(dev, 0x00000000, 4096, buffer);

    /* 从0地址擦除4096字节 */
    qe_spiflash_erase(dev, 0x00000000, 4096);

    /* 向0地址写入4096字节 */
    qe_spiflash_write(dev, 0x00000000, 4096, buffer);

SPI Flash设备核心层
===================

QESF设备核心层已经将SPI Flash的操作流程化了，SPI Flash的驱动程序只需要关注以下内容

* SPI总线已注册并正确实现了总线操作集configure和xfer接口
* SPI设备已关联到SPI总线
* SPI Flash驱动正确的SPI设备上进行探测

SPI总线、SPI设备和SPI Flash设备的注册存在先后顺序，为了确保这种顺序，建议将SPI总线和设备驱动的初始化使用 ``QE_BOARD_EXPORT()`` 接口导出，将SPI Flash的驱动初始化使用 ``QE_DEV_EXPORT()`` 接口导出。在SPI Flash的驱动程序中只需要调用 ``qe_spiflash_probe()`` 即可，SPI Flash设备的注册是在probe中自动完成的。SPI Flash驱动示例

假设已经注册了总线spi0和设备spi0.0，并确保硬件连接关系正确，即SPI Flash芯片就是连接在spi0总线的，drv_spi.c代码示例

.. code-block:: c

    static struct qe_spi_bus spi0_bus;
    static struct qe_spi_device spi0_device0;

    static int drv_spi_init(void)
    {
        /* register spi bus */
        qe_spi_bus_register(&spi0_bus, "spi0", &spi0_ops);

        /* attach spi device to spi bus */
        qe_spi_bus_attach_device(&spi0_device0, "spi0.0", "spi0", &spi0_hw);

        return 0;
    };
    QE_BOARD_EXPORT(drv_spi_init);

drv_spiflash.c代码如下

.. code-block:: c

    static int drv_spiflash_init(void)
    {
        struct qe_spiflash_device *flash;

        /* 以spi0.0探测SPI Flash */
        flash = qe_spiflash_probe("W25Qxx", "spi0.0");
        if (!flash) {
            qelog_err("drv", "spiflash probe error");
            return -1;
        }

        return 0;
    }
    QE_DEVICE_EXPORT(drv_spiflash_init);


在SPI Flash的probe过程中，首先会向SPI总线发送0x9F命令从SPI Flash读取JedecId，并根据回读到的JedecId在SPI Flash设备匹配表 ``flash_chip_table`` 中进行搜索匹配，匹配到表明QESF支持该型号的Flash，会对SPI Flash设备进行通用的reset操作，并根据匹配表中的Flash容量信息设置操作地址模式为3地址还是4地址模式，最后会向QESF注册设备对象。

``flash_chip_table`` 定义如下

.. code-block:: c

    static struct qe_spiflash_chip flash_chip_table[] = {
        {"AT45DB161E", 	FLASH_MF_ID_ATMEL, 	 	0x26, 0x00, 0x81, 2L*1024L*1024L,  FLASH_WM_BYTE|FLASH_WM_DUAL_BUFFER, 0x0, 512},
        {"W25Q40BV", 	FLASH_MF_ID_WINBOND, 	0x40, 0x13, 0x20, 512L*1024L, 	 FLASH_WM_PAGE_256B, 			0x0, 4096},
        {"W25Q16BV", 	FLASH_MF_ID_WINBOND, 	0x40, 0x15, 0x20, 2L*1024L*1024L,  FLASH_WM_PAGE_256B, 			0x0, 4096},
        {"W25Q64CV", 	FLASH_MF_ID_WINBOND, 	0x40, 0x17, 0x20, 8L*1024L*1024L,  FLASH_WM_PAGE_256B, 			0x0, 4096},
        {"W25Q64DW", 	FLASH_MF_ID_WINBOND, 	0x60, 0x17, 0x20, 8L*1024L*1024L,  FLASH_WM_PAGE_256B, 			0x0, 4096},
        {"W25Q128BV", 	FLASH_MF_ID_WINBOND, 	0x40, 0x18, 0x20, 16L*1024L*1024L, FLASH_WM_PAGE_256B, 			0x0, 4096},
        {"W25Q256FV", 	FLASH_MF_ID_WINBOND, 	0x40, 0x19, 0x20, 32L*1024L*1024L, FLASH_WM_PAGE_256B, 	 		0x0, 4096},
        {"W25QM512JV",	FLASH_MF_ID_WINBOND,	0x71, 0x19, 0x20, 32L*1024L*1024L, FLASH_WM_PAGE_256B,          0x0, 4096},
        {"SST25VF016B", FLASH_MF_ID_SST, 	 	0x25, 0x41, 0x20, 2L*1024L*1024L,  FLASH_WM_BYTE|FLASH_WM_AAI,	0x0, 4096},
        {"M25P32", 		FLASH_MF_ID_MICRON,  	0x20, 0x16, 0xD8, 4L*1024L*1024L,  FLASH_WM_PAGE_256B, 			0x0, 64L*1024L},
        {"M25P80", 		FLASH_MF_ID_MICRON,  	0x20, 0x14, 0xD8, 1L*1024L*1024L,  FLASH_WM_PAGE_256B, 			0x0, 64L*1024L},
        {"M25P40", 		FLASH_MF_ID_MICRON,  	0x20, 0x13, 0xD8, 512L*1024L, 	 FLASH_WM_PAGE_256B, 			0x0, 64L*1024L},
        {"MT25QL256", 	FLASH_MF_ID_MICRON,  	0xBA, 0x19, 0x20, 32L*1024L*1024L, FLASH_WM_PAGE_256B, 			0x0, 4096},
        {"EN25Q32B", 	FLASH_MF_ID_EON,      	0x30, 0x16, 0x20, 4L*1024L*1024L,  FLASH_WM_PAGE_256B, 			0x0, 4096},
        {"GD25Q64B", 	FLASH_MF_ID_GIGADEVICE, 0x40, 0x17, 0x20, 8L*1024L*1024L,  FLASH_WM_PAGE_256B, 			0x0, 4096},
        {"GD25Q16B", 	FLASH_MF_ID_GIGADEVICE, 0x40, 0x15, 0x20, 2L*1024L*1024L,  FLASH_WM_PAGE_256B, 			0x0, 4096},
        {"S25FL216K", 	FLASH_MF_ID_CYPRESS, 	0x40, 0x15, 0x20, 2L*1024L*1024L,  FLASH_WM_PAGE_256B, 			0x0, 4096},
        {"S25FL032P", 	FLASH_MF_ID_CYPRESS, 	0x02, 0x15, 0x20, 4L*1024L*1024L,  FLASH_WM_PAGE_256B, 			0x0, 4096},
        {"A25L080", 	FLASH_MF_ID_AMIC, 		0x30, 0x14, 0x20, 1L*1024L*1024L,  FLASH_WM_PAGE_256B, 			0x0, 4096},
        {"F25L004", 	FLASH_MF_ID_ESMT, 		0x20, 0x13, 0x20, 512L*1024L,  	 FLASH_WM_BYTE|FLASH_WM_AAI,	0x0, 4096},
        {"PCT25VF016B", FLASH_MF_ID_SST, 		0x25, 0x41, 0x20, 2L*1024L*1024L,  FLASH_WM_BYTE|FLASH_WM_AAI,	0x0, 4096},
    };

结构体 ``struct qe_spiflash_chip`` 定义如下

.. code-block:: c

    struct qe_spiflash_chip {
        char *name;
        qe_u8 mf_id;           /* 厂商ID */
        qe_u8 mt_id;           /* 内存类型ID */
        qe_u8 capacity_id;     /* 容量ID */
        qe_u8 erase_gran_cmd;  /* 块擦除指令 */
        qe_u32 capacity;       /* 容量大小 */
        qe_u16 write_mode;     /* 写模式 */
        qe_u16 reserve;
        qe_u32 erase_gran;     /* 块大小 */
    };

其中的 mf_id、mt_id和capacity_id就是JedecId，是固定不变的，需要设置的是 erase_gran_cmd 和 erase_gran，应用层无论是通过设备访问层API还是SPI Flash专用API，涉及到块读写和擦除时，都会通过erase_gran来计算和对齐操作地址，并使用erase_gran_cmd指令进行块操作。