************
SPI Driver
************

SPI设备与总线
==================

SPI是一种通信总线，一个SPI总线上可以容纳一个SPI控制器和多个从设备，多个从设备通信是分时复用关系，而SPI支持控制器与不同从设备通信时使用不同的配置模式。基于这种特性，在设备核心层对SPI设备与SPI总线进行了区分和抽象，SPI总线和SPI设备都是设备模型的一种扩展。

SPI总线
------------
SPI总线对象定义如下

.. code-block:: c

    struct qe_spi_bus
    {
        struct qe_device parent;            /* 设备对象 */
        
        qe_u32 mode 			:8;     /* SPI模式 */
        qe_u32 multi_access 	:1;     /* 是否有多个从设备 */
        qe_u32 reserve			:13;
        
        const struct qe_spi_ops *ops;       /* SPI总线操作集 */

        void *lock;                         /* 线程锁 */
        
        struct qe_spi_device *owner;        /* 当前占用SPI总线的SPI设备 */
    };

SPI总线实际上是对SPI控制器的抽象，CPU和SPI外设通信实际上是通过SPI控制器完成的，因此控制SPI总线通信相关内容都是在SPI总线对象中管理的。

SPI设备
--------

SPI设备对象定义如下

.. code-block:: c

    struct qe_spi_device
    {
        struct qe_device parent;            /* 设备对象 */
        struct qe_spi_bus *bus;             /* SPI设备使用的总线 */

        struct qe_spi_configuration config; /* SPI设备配置 */
        void   *user_data;                  /* 用户数据 */
    };

SPI总线和设备的命名
-----------------------

一个系统中可能存在多个SPI总线，而每个总线上可能连接了多个SPI设备，为了方便直观的描述这种连接关系，建议在对SPI总线和设备命名时，遵从以下原则

* SPI总线命名为spix，例如spi0、spi1
* SPI设备命名为spix.y，x表示SPI设备连接的总线标识，y表示SPI设备自己的标识，例如连接到spi0总线的0号SPI设备命名为spi0.0

SPI设备应用程序编写
======================

对SPI设备的访问可以使用设备访问层提供的通用API，但是考虑到SPI应用场景的特殊性，例如控制传感器、读写Flash/EEPROM等，通常读操作是指令+地址的数据格式，写操作是指令+地址+数据的格式，且较多的SPI数据交互是以一种写+读的方式进行的，为了便于进行通信数据的构造以及用软件对通信过程进行描述，在设备核心层SPI提供了专用的API，后文详细说明

查找SPI设备
--------------

查找SPI设备通过 ``qe_dev_find()`` 完成，代码示例如下

.. code-block:: c

    qe_dev dev;

    /* 查找SPI总线0的0号设备 */
    dev = qe_dev_find("spi0.0");

访问SPI设备
---------------

应用程序可以使用设备访问层的通用API ``qe_dev_read()`` 和 ``qe_dev_write()`` 来访问SPI设备，也可以通过设备核心层SPI提供的专用API，如下表所示

+-------------------------+-----------------+
|          接口           |      描述       |
+=========================+=================+
| qe_spi_transfer         | 进行一次SPI传输 |
+-------------------------+-----------------+
| qe_spi_send             | 进行一次SPI发送 |
+-------------------------+-----------------+
| qe_spi_recv             | 进行一次SPI接收 |
+-------------------------+-----------------+
| qe_spi_send_recv        | 先发送后接收    |
+-------------------------+-----------------+
| qe_spi_send_send        | 连续两次发送    |
+-------------------------+-----------------+
| qe_spi_transfer_message | 自定义传输      |
+-------------------------+-----------------+

进行一次SPI传输
^^^^^^^^^^^^^^^^^^

``qe_spi_transfer()`` 接口用于进行一次SPI传输，定义如下

.. code-block:: c

    qe_size qe_spi_transfer(struct qe_spi_device *dev, const void *send_buf, void *recv_buf, 
        qe_size length);

* 入参
    * dev：SPI设备
    * send_buf：发送缓冲区
    * recv_buf：接收缓冲区
    * length：发送/接收字节长度
* 返回值
    * 0：传输失败
    * 非0：传输成功的字节数

如果发送和接收缓冲区同时不为空，则会先进行发送再进行接收，发送和接收的数据长度都为length。设备核心层会在传输前自动片选总线，传输后释放总线。

进行一次SPI发送
^^^^^^^^^^^^^^^^^

如果只发送一次数据，而不关心接收的数据，可以使用以下接口

.. code-block:: c

    qe_size qe_spi_send(struct qe_spi_device *dev, const void *send_buf, qe_size length);

该接口在内部是对 ``qe_spi_transfer()`` 的封装。

进行一次SPI接收
^^^^^^^^^^^^^^^^^

如果只接收一次数据，而不关心发送的数据，可以使用以下接口

.. code-block:: c

    qe_size qe_spi_recv(struct qe_spi_device *dev, const void *recv_buf, qe_size length);

该接口在内部是对 ``qe_spi_transfer()`` 的封装。

连续两次SPI发送
^^^^^^^^^^^^^^^^^^

如果要进行连续两次SPI发送，且中间不释放SPI片选，可以使用如下函数

.. code-block:: c

    qe_ret qe_spi_send_then_send(struct qe_spi_device *dev, 
                                   const void           *send_buf1, 
                                   qe_size             send_length1, 
                                   const void           *send_buf2, 
                                   qe_size             send_length2);

* 入参
    * dev：SPI设备
    * send_buf1：第一个发送缓冲区
    * send_length1：第一个发送缓冲区长度
    * send_buf2：第二个发送缓冲区
    * send_length2：第二个发送缓冲区长度
* 返回值
    * 成功：qe_ok
    * 失败：错误值

该接口进行两次连续发送，并忽略接收的数据，发送第一个缓冲区前进行片选，发送完成第二个缓冲区后释放片选。该接口适合在命令地址+数据的场景中使用，第一次发送命令和地址，第二次发送数据，这样可以避免数据搬运和拼接操作。

先发送后接收
^^^^^^^^^^^^^^^^

如果要先向从设备发送数据，然后再接收从设备发来的数据，且中间片选不释放，可以使用如下接口

.. code-block:: c

    qe_ret qe_spi_send_then_recv(struct qe_spi_device *dev,
                                   const void           *send_buf,
                                   qe_size             send_length,
                                   void                 *recv_buf,
                                   qe_size             recv_length);

* 入参
    * dev：SPI设备
    * send_buf：发送缓冲区
    * send_length：发送缓冲区长度
    * recv_buf：接收缓冲区
    * recv_length：接收缓冲区长度
* 返回值
    * 成功：qe_ok
    * 失败：错误值

该接口适合将从设备数据读出的场景，先发送命令和地址，再读出数据。由于SPI主从在通信结构上是一个双端Fifo，在读出数据时SPI控制器需要向从设备写recv_length长度的无效数据，而无效数据的值由驱动程序自定义。

自定义传输数据
^^^^^^^^^^^^^^^^^^

设备核心层为应用程序提供了自定义传输SPI数据的能力，接口定义如下

.. code-block:: c

    struct qe_spi_message *qe_spi_transfer_message(struct qe_spi_device *dev, 
        struct qe_spi_message *message);

* 入参
    * dev：SPI设备
    * message：消息指针
* 返回值
    * 成功：QE_NULL
    * 失败：返回剩余未传输的消息指针

使用该接口，用户可以进行连续多次SPI传输，每次传输的数据内容以及何时片选何时释放片选，完全由用户决定。message结构由设备核心层SPI提供，定义如下

.. code-block:: c

    struct qe_spi_message
    {
        const void *send_buf;           /* 发送缓冲区 */
        void *recv_buf;                 /* 接收缓冲区 */
        qe_size length;               /* 发送/接收长度 */
        struct qe_spi_message *next;    /* 下一个消息指针 */

        unsigned int cs_take    : 1;    /* 选中片选 */
        unsigned int cs_release : 1;    /* 释放片选 */
        unsigned int reserve    : 30;
    };

send_buf为空时表示本次传输只接收数据，recv_buf为空表示本次传输只发送数据，send_buf和recv_buf不能同时为空。next用于多条消息构成单向链表，如果只进行一次传输，需将next置为空，如果进行多次传输，需将最后一个消息的next指针置为空。

以下示例介绍自定义消息的用法

.. code-block:: c

    qe_u8 cmd = 0x01;
    qe_u8 buffer[5];
    qe_dev dev;
    struct qe_spi_message msg1, msg2;

    /* 查找SPI设备获取句柄 */
    dev = qe_dev_find("spi0.0");

    /* 构造第一条消息 */
    msg1.send_buf   = &cmd;
    msg1.recv_buf   = QE_NULL;
    msg1.length     = 1;
    msg1.cs_take    = 1;        /* 选中片选 */
    msg1.cs_release = 0;
    msg1.next       = &msg2;    /* 链接第二条消息 */

    /* 构造第二条消息 */
    msg2.send_buf   = QE_NULL;
    msg2.recv_buf   = buffer;
    msg2.length     = 5;
    msg2.cs_take    = 0;
    msg2.cs_release = 1;        /* 释放片选 */
    msg2.next       = QE_NULL;  /* 无后续消息 */

    /* 进行传输 */
    rt_spi_transfer_message(dev, &msg1);

SPI设备驱动程序编写
======================

SPI设备的驱动程序主要需要关注以下内容

* SPI总线的创建和注册
* SPI总线操作集的实现
* SPI设备关联到SPI总线

SPI总线以静态方式创建，并通过 ``qe_spi_bus_register()`` 接口注册到QESF，接口定义如下

.. code-block:: c

    qe_ret qe_spi_bus_register(struct qe_spi_bus *bus, const char *name, 
        const struct qe_spi_ops *ops);

* 入参
    * bus：SPI总线
    * name：SPI总线名称
    * ops：SPI总线操作集
* 返回值
    * 成功：qe_ok
    * 失败：错误值

注册了SPI总线后，通过 ``qe_spi_bus_attach_device()`` 接口向SPI总线关联SPI设备

.. code-block:: c

    qe_ret qe_spi_bus_attach_device(struct qe_spi_device *spi,
                                      const char           *name,
                                      const char           *bus_name,
                                      void                 *user_data);

* 入参
    * spi：SPI设备
    * name：SPI设备名
    * bus_name：SPI总线名
    * user_data：用户私有数据
* 返回值
    * 成功：qe_ok
    * 失败：错误值

注册SPI总线和向总线中关联一个SPI设备的示例如下

.. code-block:: c

    static struct qe_spi_bus spi0_bus;
    static struct qe_spi_device spi0_device0;

    static int drv_spi_init(void)
    {
        /* 注册SPI总线 */
        qe_spi_bus_register(&spi0_bus, "spi0", &spi0_ops);

        /* 关联SPI设备到SPI总线 */
        qe_spi_bus_attach_device(&spi0_device0, "spi0.0", "spi0", &spi0_hw);

        return 0;
    };
    /* 注册初始化函数到QESF */
    QE_BOARD_EXPORT(drv_spi_init);

SPI总线操作集
----------------

SPI总线操作集定义如下

.. code-block:: c

    struct qe_spi_ops
    {
        qe_ret (*configure)(struct qe_spi_device *device, struct qe_spi_configuration *configuration);
        qe_u32 (*xfer)(struct qe_spi_device *device, struct qe_spi_message *message);
    };

驱动程序中主要需要实现的是操作集中的以上两个接口：configure和xfer。configure是对SPI设备使用总线时的配置接口，当发起一次SPI传输时，设备核心层SPI框架会先调用SPI设备对应的configure接口，完成SPI总线的配置。配置时，主要是以传入参数中SPI设备的配置参数 ``device->config`` 对SPI总线的模式、时钟速率、数据宽度进行设置。

xfer接口是对一次SPI传输的抽象，驱动程序需根据message参数完成SPI数据传输，并注意是单次传输还是多次传输(message->next)，每次传输的方向是发送、接收、还是先发送后接收。