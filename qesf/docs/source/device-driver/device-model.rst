************
设备模型
************

为了对嵌入式各种外接设备提供便捷、高效和统一的访问控制方式，QESF定义了一个轻量级的设备模型框架，如下图所示

.. image:: ../images/device-model-01.png

在应用层与硬件之间定义了3个层次

* Device Access Layer：设备访问层
* Device Driver Core Layer：设备驱动核心层
* Device Driver Layer：设备驱动层

通过设备访问层，应用程序以统一的设备访问API对设备进行读写控制等操作，而不需要关心设备工作的细节。这样应用程序与某一个具体的设备驱动程序在代码上是完全分离的，应用程序的变动不会影响驱动程序，驱动程序的变动也不会影响应用程序，有效的降低了代码耦合性。

设备驱动核心层是对各种类型设备的软件抽象，将同类设备驱动中的共性操作提取出来，固定为流程和接口，并对非共性部分提供灵活的注册机制，由具体驱动程序去实现。

设备驱动层是具体的设备驱动程序，实现对硬件的访问控制功能。设备实例必须由驱动程序创建并注册到设备模型框架中，应用程序才能够对设备进行访问。

从驱动程序创建设备到应用程序使用设备的示例时序如下图所示。

.. image:: ../images/device-model-02.png

首先，驱动程序根据设备核心层定义创建对应类型的设备实例 ``xxx_device`` ，并将该实例通过 ``qe_xxx_register()`` 接口注册到对应设备核心层，核心层通过 ``qe_dev_register()`` 接口将实例注册到设备访问层。应用程序通过 ``qe_dev_find()`` 接口查找并获取设备句柄，然后通过 ``qe_device_init()`` 接口对设备进行初始化，通过       ``qe_dev_open()`` 接口打开设备， 通过 ``qe_dev_read()``、 ``qe_dev_write()`` 、 ``qe_deive_ioctl()`` 对设备进行读、写、控制，最后通过 ``qe_dev_close()`` 关闭设备。

设备对象
================

QESF使用 ``struct qe_device`` 对设备对象进行描述和定义，具体定义如下

.. code-block:: c 

    struct qe_device 
    {

        enum qe_devclass_type class;    /* 设备类别 */

        char name[QE_NAME_MAX];         /* 设备名称 */

        qe_u16 flag;               /* 设备参数 */
        qe_u16 open_flag;          /* 设备打开标志 */

        qe_u16 reference;          /* 设备引用计数 */

        qe_u16 device_id;          /* 设备ID */

        /* 数据收发回调 */
        qe_ret (*rx_indicate)(qe_dev dev, qe_size size);
        qe_ret (*tx_complete)(qe_dev dev, void *buffer);

        /* 设备操作集 */
        const struct qe_device_ops *ops;

        /* 用户私有数据 */
        void *private;

        /* 设备管理链表 */
        qe_list list;
    };

设备类型定义如下

.. code-block:: c

    enum qe_devclass_type {
        QE_DEV_CHAR = 0,					/* 字符设备 */
        QE_DEV_BLOCK,					/* 块设备 */
        QE_DEV_NETIF,					/* 网络接口设备 */
        QE_DEV_MTD,					/* 内存设备 */
        QE_DEV_RTC,					/* RTC设备 */
        QE_DEV_SOUND,					/* 声音设备 */
        QE_DEV_GRAPHIC,					/* 图形设备 */
        QE_DEV_I2CBUS, 					/* I2C 总线设备 */
        QE_DEV_USB_DEVICE,					/* USB device 设备 */
        QE_DEV_USB_HOST,					/* USB host 设备 */
        QE_DEV_SPI_BUS, 					/* SPI 总线设备 */
        QE_DEV_SPI_DEVICE,					/* SPI 设备 */
        QE_DEV_SDIO,					/* SDIO 设备 */
    };

设备操作集定义如下

.. code-block:: c 

    struct qe_device_ops {
        qe_ret   (*init)   (qe_dev dev);
        qe_ret   (*open)   (qe_dev dev, qe_u16 oflag);
        qe_ret   (*close)  (qe_dev dev);
        qe_size  (*read)   (qe_dev dev, qe_offs pos, void *buffer, qe_size size);
        qe_size  (*write)  (qe_dev dev, qe_offs pos, const void *buffer, qe_size size);
        qe_ret   (*control)(qe_dev dev, int cmd, void *args);
    };

各操作的描述如下表

+-------+---------------------------------------------------------------------------+
| 方法  | 描述                                                                      |
+=======+===========================================================================+
| init  | 对设备进行初始化，初始化完成后，设备对象中的flag被置为QE_DEV_F_ACTIVATED  |
+-------+---------------------------------------------------------------------------+
|| open || 打开设备。某些设备并不是系统启动后就开始工作，而是被上层应用触发打开操作 |
||      || 后才开始工作，这类设备需要在驱动中实现打开方法                           |
+-------+---------------------------------------------------------------------------+
| close | 关闭设备                                                                  |
+-------+---------------------------------------------------------------------------+
| read  | 从设备读取数据                                                            |
+-------+---------------------------------------------------------------------------+
| write | 向设备写入数据                                                            |
+-------+---------------------------------------------------------------------------+
| ioctl | 对设备进行命令控制，驱动程序可通过该接口扩展自定义功能                    |
+-------+---------------------------------------------------------------------------+

设备的创建和注册
================

驱动程序通过静态变量的方式创建设备实例，并注册到设备访问层中，如下所示

.. code-block:: c

    static struct qe_serial_device serial0;

    static int micblz_uart_init(void)
    {
        struct qe_serial_configure config = QE_SERIAL_DEFCONFIG;

        serial0.ops = &uart_ops;
        serial0.config = config;
        qe_serial_register(&serial0, "uart0",
            QE_DEV_F_RDWR|QE_DEV_F_INT_RX, &uart0_hw);

        return qe_ok;
    }
    QE_BOARD_EXPORT(micblz_uart_init);

向QESF注册设备使用的接口为 ``qe_dev_register()``，定义如下

.. code-block:: c

    qe_ret qe_dev_register(struct qe_device *dev, const char *name, qe_u16 flags);

* 入参
    * dev：设备句柄
    * name：设备名称，字符串最大长度由qe_config.h中的QE_NAME_MAX限制
    * flags：设备模式标志
* 返回值：成功返回qe_ok，失败返回错误值

.. note::

    驱动程序大多数情况下并非直接调用 ``qe_dev_register()`` 向QESF注册设备，而是调用设备核心层提供的 ``qe_xxx_register()`` 函数向设备核心层注册设备，由设备核心层间接向QESF注册设备。通过设备核心层接口注册设备的方式在具体设备类型的文档中参考。

入参中的flags即设备模式，支持以下参数或它们的组合

+---------------------------+----------------------------------------------+
| 标识                      | 含义                                         |
+===========================+==============================================+
| QE_DEV_F_RDONLY           | 设备为只读模式                               |
+---------------------------+----------------------------------------------+
| QE_DEV_F_WRONLY           | 设备为只写模式                               |
+---------------------------+----------------------------------------------+
| QE_DEV_F_RDWR             | 设备为读写模式                               |
+---------------------------+----------------------------------------------+
| QE_DEV_F_REMOVABLE        | 设备可删除                                   |
+---------------------------+----------------------------------------------+
| RT_DEVICE_FLAG_STANDALONE | 独立设备，不可以同时被多个应用实例打开并使用 |
+---------------------------+----------------------------------------------+
| QE_DEV_F_SUSPENDED        | 设备支持挂起                                 |
+---------------------------+----------------------------------------------+
| QE_DEV_F_STREAM           | 设备为流模式                                 |
+---------------------------+----------------------------------------------+
| QE_DEV_F_INT_RX           | 设备使用中断的方式接收数据                   |
+---------------------------+----------------------------------------------+
| QE_DEV_F_DMA_RX           | 设备使用DMA的方式接收数据                    |
+---------------------------+----------------------------------------------+
| QE_DEV_F_INT_TX           | 设备使用中断的方式发送数据                   |
+---------------------------+----------------------------------------------+
| QE_DEV_F_DMA_TX           | 设备使用DMA的方式发送数据                    |
+---------------------------+----------------------------------------------+

流模式 QE_DEV_F_STREAM 专用于串口终端设备，当输出字符为"\n"时，会自动前补一个"\r"

设备的注销
============

设备注销使用 ``qe_dev_unregister()`` 接口，定义如下

.. code-block:: c

    qe_ret qe_dev_unregister(struct qe_device *dev);

调用 ``qe_dev_unregister()`` 后，设备将从设备访问层移除， ``qe_dev_find()`` 接口无法获取设备句柄

设备的访问操作
=================

查找设备
-----------------

驱动程序提供设备被访问被使用的能力，应用程序是设备的使用者。应用在使用设备前的第一件事就是获得设备句柄，通过以下接口完成

.. code-block:: c

    struct qe_device *qe_dev_find(const char *name);

* 入参：
    * name：设备名称，需与驱动程序注册设备时的名称一致
* 返回值
    * 成功：返回设备句柄，即一个指向设备对象的指针 ``struct qe_device *``
    * 失败：返回 QE_NULL

初始化设备
----------------

获得设备句柄后，可以通过如下函数进行初始化

.. code-block:: c

    qe_ret qe_device_init(struct qe_device *dev);

* 入参
    * dev：设备句柄
* 返回值
    * 成功：qe_ok
    * 失败：返回错误值

打开和关闭设备
----------------

打开设备时会检测设备是否已经初始化，如果检测到设备未初始化会调用设备的初始化接口进行初始化。通过以下接口打开设备

.. code-block:: c

    qe_ret qe_dev_open(struct qe_device *dev, qe_u16 oflag);

* 入参
    * dev：设备句柄
    * oflag：设备打开模式
* 返回值
    * 成功：qe_ok
    * 失败：返回错误值

设备打开模式支持以下几种

.. code-block:: c

    #define QE_DEV_OF_CLOSE 0x000   /* 设备已经关闭（内部使用）*/
    #define QE_DEV_OF_RDONLY 0x001  /* 以只读方式打开设备 */
    #define QE_DEV_OF_WRONLY 0x002  /* 以只写方式打开设备 */
    #define QE_DEV_OF_RDWR 0x003    /* 以读写方式打开设备 */
    #define QE_DEV_OF_OPEN 0x008    /* 设备已经打开（内部使用）*/
    #define QE_DEV_OF_STREAM 0x040   /* 设备以流模式打开 */
    #define QE_DEV_OF_INT_RX 0x100   /* 设备以中断接收模式打开 */
    #define QE_DEV_OF_DMA_RX 0x200   /* 设备以 DMA 接收模式打开 */
    #define QE_DEV_OF_INT_TX 0x400   /* 设备以中断发送模式打开 */
    #define QE_DEV_OF_DMA_TX 0x800   /* 设备以 DMA 发送模式打开 */

通过以下接口关闭设备

.. code-block:: c

    qe_ret qe_dev_close(struct qe_device *dev);

* 入参
    * dev：设备句柄
* 返回值
    * 成功：qe_ok
    * 失败：返回错误值

控制设备
-------------

通过以下接口对设备进行命令控制

.. code-block:: c

    qe_ret qe_device_ioctl(qe_dev dev, int cmd, void *arg);

* 入参
    * dev：设备句柄
    * cmd：控制命令
    * arg：控制参数
* 返回值
    * 成功：qe_ok
    * 失败：返回错误值

控制命令有几个通用定义

.. code-block:: c

    #define QE_DEV_IOC_RESUME        0x01        /* 恢复设备 */
    #define QE_DEV_IOC_SUSPEND       0x02        /* 挂起设备 */
    #define QE_DEV_IOC_CONFIG        0x03        /* 配置设备 */

    #define QE_DEV_IOC_SET_INT       0x10        /* 设置中断 */
    #define QE_DEV_IOC_CLR_INT       0x11        /* 清中断 */
    #define QE_DEV_IOC_GET_INT       0x12        /* 获取中断标志 */

读写设备
----------------

通过以下函数从设备读取数据

.. code-block:: c

    qe_size qe_dev_read(struct qe_device *dev, qe_offs pos, void *buffer, qe_size size);

* 入参
    * dev：设备句柄
    * pos：读取数据偏移，不同类型设备有不同含义
    * buffer：缓冲区
    * size：读取大小
* 返回值
    * 实际读取的大小
    * 0：需要通过errno判断错误状态

通过以下函数向设备写入数据

.. code-block:: c

    qe_size qe_dev_write(struct qe_device *dev, qe_offs pos, const void *buffer, qe_size size);

* 入参
    * dev：设备句柄
    * pos：写入数据偏移，不同类型设备有不同含义
    * buffer：缓冲区
    * size：写入大小
* 返回值
    * 实际写入的大小
    * 0：需要通过errno判断错误状态

数据接收回调
----------------

应用程序可以通过如下函数向设备注册一个接收回调，当设备收到数据后会通过注册的回调通知应用层

.. code-block:: c

    qe_ret qe_dev_set_rx_indicate(struct qe_device *dev,
                                       qe_ret (*rx_ind)(struct qe_device *, qe_size));

* 入参
    * dev：设备句柄
    * rx_ind：回调函数指针
* 返回值
    * 成功：qe_ok
    * 失败：错误值

回调函数由应用程序提供，当设备收到数据后，应用程序可通过回调函数参数得知接收数据长度

数据发送回调
----------------

应用程序可以通过如下函数向设备注册一个发送回调，当设备自动发送完成后会通过注册的回调通知应用层

.. code-block:: c

    qe_ret qe_dev_set_tx_complete(struct qe_device *dev,
                         			   qe_ret (*tx_done)(struct qe_device *, void *))

* 入参
    * dev：设备句柄
    * tx_done：回调函数指针
* 返回值
    * 成功：qe_ok
    * 失败：错误值

回调函数由应用程序提供，当设备发送数据完成后，建议该回调函数参数中传递发送缓冲区地址，应用程序可以在回调中对缓冲区进行释放或者类似链表的next用法

Example
----------------

下面以一个假设的LED设备为例，展示应用层使用设备模型控制LED闪烁的整个过程

.. code-block:: c

    #include "FreeRTOS.h"
    #include "task"
    #include "qelib.h"

    TaskHandle_t led_tsk;

    static void led_task(void *data)
    {
        qe_ret ret;
        qe_dev dev;

        /* 根据设备名称获取设备句柄 */
        dev = qe_dev_find("led0");
        if (!dev) {
            qelog_err("led", "device not found");
            goto __exit;
        }

        /* 对LED设备进行初始化 */
        ret = qe_device_init(dev);
        if (ret != qe_ok) {
            qelog_err("led", "led init err:%d", ret);
            goto __exit;
        }

        while (1) {

            /* 控制LED亮 */
            qe_device_ioctl(dev, QE_DEV_IOC_LED_ON, QE_NULL);

            qe_hw_msleep(1000);

            /* 控制LED灭 */
            qe_device_ioctl(dev, QE_DEV_IOC_LED_OFF, QE_NULL);
        }

    __exit:
        vTaskDelete(QE_NULL);
    }

    static int led_task_launch(void)
    {   
        xTaskCreate(led_task, "led", 200, QE_NULL, 1, led_tsk);
        return 0;
    }
    QE_APP_EXPORT(led_task_launch);
