****************
Bit Stream
****************

比特流可以用于bit级别的数据格式封装和解析，例如H264/265编码就使用了比特级别编码，
目前仅支持从比特流中读取数据，不支持写入数据

Example
========

.. code-block:: c

    qe_bitstream bs;
    char buffer[1024];

    // 使用buffer初始化一个比特流
    qe_bitstream_init(&bs, buffer, 1024);

    // 从比特流中读取7个bit
    int x1 = qe_bitstream_get_bits(&bs, 7);
    // 从比特流中读取3个bit
    int x2 = qe_bitstream_get_bits(&bs, 3);
    // 跳过14个bit
    qe_bitstream_skip_bits(&bs, 14);
    // 测试5个bit位的内容
    qe_bitstream_test_bits(&bs, 5);