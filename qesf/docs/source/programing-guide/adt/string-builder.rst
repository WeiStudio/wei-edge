****************
String
****************

qelib中提供了两种类型的字符串构建，一种是字符串缓冲区由外部提供，另一种是基于动态内存的

* qe_strb：字符串缓冲区由外部提供，通常用于处理静态buffer
* qe_string：基于动态内存的string类型

qe_strb
========
以下是使用 ``qe_strb`` 来进行字符串格式化的一个例子

.. code-block:: c

    // 创建静态buffer
    char buffer[128];
    // 使用buffer初始化构建器
    qe_strb sbd = qe_strb_init(buffer, 128);
    // 向构建器中写入一个静态字符串
    qe_strb_string(sbd, "Hello");
    // 向构建器中写入格式化内容
    qe_strb_format(sbd, " This is year %d", 20);
    // 向构建器中写入整数
    qe_strb_int(sbd, 24);
    // 构建内容为 Hello This is year 2024

    // 构建完成使用后重新初始化为buffer
    sbd = qe_strb_frombuf(buffer, 128);

以上例子中使用 ``qe_strb_init`` 和 ``qe_strb_frombuf`` 都可以对构建器进行初始化，区别是
``qe_strb_init`` 会保留原始buffer中的字符串，而 ``qe_strb_frombuf`` 不保留

string
========
qelib提供了一个类似C++/Java中的String类的动态字符串类型，可以灵活的进行字符串格式化处理

创建和释放string 
----------------
使用以下两个函数创建string

.. code-block:: c

    char *init_string = "InitString";

    // 从一个已存在的字符串创建
    qe_string *str = qe_string_new(init_string);

    // 创建一个空字符串
    qe_string *str = qe_string_new(QE_NULL);

    // 从init_string取前4个字符创建string
    qe_string *str = qe_string_new_len(init_string, 4);

使用以下方式释放string 

.. code-block:: c

    qe_string_free(str);

插入字符串
----------------
使用以下方式向string中插入字符串

.. code-block:: c

    char *test = "test";

    // 向0位置插入test
    qe_string_insert(str, 0, test);
    qe_string_insert_len(str, 0, test, qe_strlen(test));

    // 头插test
    qe_string_prepend(str, test);
    qe_string_prepend_len(str, test, qe_strlen(test));
    // 头插一个字符
    qe_string_prepend_char(str, 'c');

    // 尾插test
    qe_string_append(str, test);
    qe_string_append_len(str, test, qe_strlen(test));
    // 尾插一个字符
    qe_string_append_char(str, 'c');

格式化
--------
使用以下方式对字符串进行格式化

.. code-block:: c

    // 清空并格式化
    qe_string_format(str, "year:%d month:%d day:%d", year, month, day);

    // 追加方式格式化
    qe_string_append_format(str, "year:%d month:%d day:%d", year, month, day);

替换
--------
使用以下方式将string中某个字符串替换

.. code-block:: c

    // year替换为This year
    qe_string_replace(str, "year", "This year", -1);

比较
--------
使用以下方式进行两个string的比较

.. code-block:: c

    if (qe_string_equal(str1, str2)) {
        /* 相同 */
    }

擦除
--------
使用以下方式擦除指定位置和长度

.. code-block:: c
    
    qe_string_erase(str, 0, 4);
