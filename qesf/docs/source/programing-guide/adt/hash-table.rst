****************
Hash Table
****************

哈希表在需要一个key来索引数据的场景中经常会用到，具有较高的查找性能。qelib提供了一
个可以存储任意类型数据的自动扩容的哈希表，实现提供了几种预设的典型哈希函数供用户使用，
具有比较均匀的分布

* ``qe_str_hash`` 字符串哈希函数，最常用的哈希函数
* ``qe_direct_hash`` 直接地址映射
* ``qe_int_hash`` 有符号整数值哈希
* ``qe_uint_hash`` 无符号整数值哈希
* ``qe_int64_hash`` 有符号64位整数哈希
* ``qe_double_hash`` 双精度浮点哈希

用户也可以自定义哈希函数

创建和销毁哈希表
================

创建哈希表需要传入一个哈希函数和相对应的比较函数。需要注意的是，哈希表中存储的key和value
都只是变量的地址，而不存储任何数据内容，需要注意key和value作用域的问题。创建哈希函数有两
个版本， ``qe_hashtab_new``用于key和value都是静态变量的情况， ``qe_hashtab_new_full``
是针对key和value是动态创建的情况

.. code-block:: c

    // 创建一个以字符串为key的哈希表
    qe_hashtab *htab = qe_hashtab_new(qe_str_hash, qe_str_equal);

    // 使用自定义哈希函数
    static qe_uint my_hash_fn(qe_const_ptr key)
    {
        /* 提供一个key到hash value的映射关系 */
    }

    static qe_bool my_hash_euqal(qe_const_ptr a, qe_const_ptr b)
    {
        /* 提供一个判断key相等的条件 */
    }

    qe_hashtab *htab = qe_hashtab_new(my_hash_fn, my_hash_euqal);

    // 创建一个动态key和value的哈希表
    static void my_key_free(qe_ptr key)
    {
        // 提供key的释放方法
    }
    static void my_val_free(qe_ptr val)
    {
        // 提供val的释放方法
    }
    qe_hashtab *htab = qe_hashtab_new_full(qe_str_hash, qe_str_equal, my_key_free, my_val_free);

销毁哈希表使用 ``qe_hashtab_destroy``，如果创建时使用了 ``qe_hashtab_new_full`` 指定了key和value的
释放方法，销毁每个key和value时会自动调用

.. code-block:: c

    qe_hashtab_destroy(htab);

key和value操作
================

.. code-block:: c

    char name1_key[] = "name";
    char name2_key[] = "name";
    char name1_val[] = "xxx";
    char name2_val[] = "yyy";

    // 向表中插入kv，如果key不存在会自动创建，如果key已经存在，则会释放old value，使用new value
    qe_hashtab_insert(htab, name1_key, name1_val);
    // 使用"zzz"替换xxx
    qe_hashtab_insert(htab, name1_key, "zzz");

    // 替换key对应的val，如果key不存在会自动创建，如果key已经存在，会释放old kv，使用新的kv
    // 由于name1_key==name2_key，name1_key被替换为name2_key，释放old val，使用新的val
    qe_hashtab_replace(htab, name2_key, name2_val);

    // 添加一个key，如果key已存在，替换old key
    qe_hashtab_add(htab, "attr");

    // 删除一个key并释放
    qe_hashtab_remove(htab, "attr");
    // 删除一个key不释放
    qe_hashtab_steal(htab, "attr");

    // 查询表中表项个数(key)
    int size = qe_hashtab_size(htab);

    // 从表中查找对应key的val
    char *val = qe_hashtab_lookup(htab, "name");

迭代器
========
当需要对哈希表进行遍历时，可以使用迭代器 ``qe_hashtab_iter``

.. code-block:: c

    // 创建一个迭代器并初始化
    qe_hashtab_iter iter;
    qe_hashtab_iter_init(&iter, htab);

    // 循环获取每个kv
    while (qe_hashtab_iter_next(&iter, &key, &val)) {
        qe_debug("key:%s val:%p", key, val);
    }
