***********************
Minimum Priority Queue
***********************

qelib提供了一个优先级队列，能够使低优先级元素始终保持在队列头部，支持任意类型元素，
支持手动Resize

创建和释放
================
由于优先级队列是一个泛型数据结构，因此也考虑到了优先级本身的泛型。创建优先队列时需要
传入一个比较器，如果为空，表示优先级使用默认的整型类型，如果不为空，则在判断优先级大
小时使用用户提供的比较器

.. code-block:: c

    // 使用默认整数优先级比较
    qe_minpq *q = qe_minpq_new(int, 8, QE_NULL);

    // 使用用户自定义优先级比较器
    static qe_bool x_comparator(void *a, void *b)
    {
        /* 
         * 自定义比较器判断逻辑
         * a大于b返回true
         */
    }
    qe_minpq *q = qe_minpq_new(struct xxx, 8, x_comparator);

    // 释放队列
    qe_free(q);

插入和删除
================
当使用用户自定义优先级时，优先级信息在元素数据中携带，插入时指定的整数优先级不起作用

.. code-block:: c

    int x = 25;
    int v, p;
    qe_minpq_insert(q, &x, 0);

    // 最低优先级数据出队，p为返回的优先级
    qe_minpq_delmin(q, &v, &p);

    // 获取指定索引位置的元素，不出队
    qe_minpq_peek(q, &v, &p, 0);

队列中剩余元素个数
==================

.. code-block:: c

    int size = qe_minpq_wait(q);

扩容/缩容
================

如果传入的resize大小小于当前队列中剩余元素大小，则不允许resize

.. code-block:: c

    qe_minpq_resize(q, 32);