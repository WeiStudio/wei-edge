******************
Array
******************

动态数组提供了一个能够自动Resize的数组技术，能够动态根据当前元素数量来调整内存使用，使用者
不需要关心内部内存大小问题，且数组不限定存储元素的类型，可以是一个基本类型、结构体或者指针

创建和释放
============
qelib提供了两种创建动态数组的方式

.. code-block:: c

    // 创建一个用于存储int类型的动态数组，初始元素空间为0
    qe_array *array = qe_array_new(sizeof(int));

    // 创建一个用于存储int类型的动态数组，初始元素空间为8
    qe_array *array = qe_array_new_sized(sizeof(int), 8);

释放数组使用如下接口

.. code-block:: c

    qe_array_free(array);

插入和删除
============
支持3种方式插入元素

.. code-block:: c

    int a, b, c;

    // 尾部插入a
    qe_array_append(array, &a);

    // 头部插入b
    qe_array_append(array, &b);

    // 在索引1的位置插入一个c，原本索引1位置的元素会后移
    qe_array_insert(array, 1, &c, 1);

删除元素有2种方式

.. code-block:: c

    // 删除0位置的元素
    qe_array_remove_index(array, 0);

    // 删除从0位置的2个元素
    qe_array_remove_range(array, 0, 2);

获取元素
============
要从数组中拿去元素，使用以下函数

.. code-block:: c

    // 获取索引1位置的元素
    int x = qe_array_index(array, int, 1);

排序
============
由于动态数组是一个泛型数据结构，因此对数组的排序需要用户提供一个比较函数，比较函数中会得到a和b
两个元素的指针，用户需要实现自己的比较逻辑，并通过返回值来告诉qelib比较结果。0表示相同，-1表示
a小于b，1表示a>b

.. code-block:: c


    int array_int_compare(const void *a, const void *b)
    {
        int *v1 = (int *)a;
        int *v2 = (int *)b;

        if (*v1 < *v2)
            return -1;
        else if (*v1 == *v2)
            return 0;
        else
            return 1;
    }

    qe_array_sort(array, array_int_compare);