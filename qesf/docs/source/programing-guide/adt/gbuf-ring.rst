********************
General Buffer Ring
********************

qelib提供了一个通用缓冲区环形结构GenBufRing，在数据收过程中能够避免频繁的数据拷贝
和动态内存分配，并提供了同步加锁版本

GenBufRing的结构如下图，在创建时会申请一片连续的内存空间，将其均分为多个块，每个块的
起始地址、块长度和数据大小用缓冲区描述符(Buffer Descriptor, BD)来表示。读写操作会自动
变更读写块的位置

.. image:: ../../images/gbuf-ring-01.png

创建和释放
================

.. code-block:: c

    // 使用静态结构体初始化buf长度为128、个数为8的Ring
    qe_gbuf_ring ring;
    qe_gbuf_ring_init(&ring, 128, 8);
    // 静态结构体销毁
    qe_gbuf_ring_deinit(&ring);

    // 动态创建buf长度为128、个数为8的Ring
    qe_gbuf_ring *ring = qe_gbuf_ring_new(128, 8);
    // 动态释放
    qe_gbuf_ring_destroy(ring);

写入数据
================

.. code-block:: c

    char data[32];
    qe_ret ret;
    ret = qe_gbuf_ring_push(ring, data, 32);

读出数据描述符
================

.. code-block:: c

    qe_ret ret;
    qe_gbuf bd;
    ret = qe_gbuf_ring_pop(ring, &bd);
    if (ret != qe_ok) {
        /*error*/
    } else {
        /* 
         * 数据位置 qe_gbuf_pos(&bd) 
         * 数据长度 qe_gbuf_data(&bd)
         */
    }

获取数据描述符
================

.. code-block:: c

    qe_gbuf bd;
    /* 所有peek操作不影响GenBufRing中元素头尾位置和个数 */
    qe_gbuf_ring_peek_tail(ring, &bd);
    qe_gbuf_ring_peek_head(ring, &bd);
    qe_gbuf_ring_peek_index(ring, 0, &bd);

获取等待数据包数
================

.. code-block:: c

    qe_uint wait = qe_gbuf_ring_wait(ring);

判断空满
================

.. code-block:: c

    qe_bool qe_gbuf_ring_isempty(ring);
    qe_bool qe_gbuf_ring_isfull(ring);

加锁操作
================

.. code-block:: c

    qe_ret ret;
    /*
     * lock是创建好的锁指针
     * acquire_fn是锁申请函数
     * release_fn是锁释放函数
     */ 
    ret = qe_gbuf_ring_set_lock(ring, lock, acquire_fn, release_fn);

    // 加锁后可以使用带锁后缀的版本来读写数据
    char wdata[32];
    // 带超时时间的写入
    ret = qe_gbuf_ring_push_locked(ring, data, 32, timeout);
    if (ret != qe_ok) {
        /*error*/
    }

    qe_gbuf bd;
    ret = qe_gbuf_ring_poll_locked(ring, &bd, timeout);
    if (ret != qe_ok) {
        /*error*/
    }

清理缓冲区
================

.. code-block:: c

    /* 将GenBufRing中的所有缓冲区数据长度清0，并不释放内存空间 */
    qe_gbuf_ring_clear(ring);