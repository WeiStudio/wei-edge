**************
Ring Buffer
**************

在处理输入输出数据流时，环形缓冲区是一个很好的限制大小的数据存储方式，qelib提供了一个
简单的环形缓冲区实现

创建和释放
================

.. code-block:: c

    // 静态创建
    char buffer[1024];
    qe_ringbuffer ringbuffer;
    qe_ringbuffer_init(&ringbuffer, buffer, 1024);

    // 动态创建
    qe_ringbuffer *rb = qe_ringbuffer_new(1024);
    // 动态释放
    qe_ringbuffer_free(rb);

写入和读取
================

.. code-block:: c

    char txbuf[32];
    char rxbuf[32];

    qe_ringbuffer_write(rb, txbuf, 32);

    qe_ringbuffer_read(rb, rxbuf, 32);

获取有效数据长度和缓冲区容量
============================

.. code-block:: c

    int wait = qe_ringbuffer_wait(rb);
    int capacity = qe_ringbuffer_capacity(rb);

清空
========

.. code-block:: c

    qe_ringbuffer_clear(rb);