****************
Bit Map
****************

位图是编程中常用的一种数据结构，在一些数据量较大但状态表示较单一的场景非常好用。
qelib中的位图可以是静态声明的，也可以是动态创建

创建和释放位图
==============

.. code-block:: c

    // 静态创建声明变量，创建一个128bit的位图
    qe_bitmap_init(map, 128);

    // 在结构体中声明一个128bit的位图
    typedef struct 
    {
        int a;
        char b;
        qe_bitmap_init(map, 128);
    } foo;
    foo f;

    // 动态创建256bit的位图
    qe_bitmap *map = qe_bitmap_new(256);
    // 动态释放
    qe_free(map);

单个位操作
============

.. code-block:: c

    // 位图的13bit置1
    qe_set_bit(13, map);

    // 位图的25bit清零
    qe_clear_bit(25, map);

    // 位图的39bit反转
    qe_change_bit(39, map);

    // 测试27bit
    int val = qe_test_bit(27, map);

多位操作
========

.. code-block:: c

    // 判断位图是否为空，即全零
    qe_bitmap_empty(map);

    // 判断位图是否为满，即全1
    qe_bitmap_full(map);

    // 将bit[5:15]置1
    qe_bitmap_set(map, 5, 11);

    // 将bit[5:15]置0
    qe_bitmap_clear(map, 5, 11);

    // 将bit[0:15]置0 
    qe_bitmap_zero(map, 16);

    // 将bit[0:15]置0
    qe_bitmap_zero(map, 16);

    // 将bit[0:15]置1
    qe_bitmap_full(map, 16);