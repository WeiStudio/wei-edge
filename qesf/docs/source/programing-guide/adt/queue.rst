********
Queue
********

qelib提供了一个FIFO队列Queue，该队列可以存储任意数据类型，空间在创建时固定，不会动态调整

创建和释放队列
================

.. code-block:: c

    // 静态创建
    int datas[10] = {1,2,3,4,5};
    qe_queue queue;
    qe_queue_init(&queue, datas, sizeof(int), 10, 5, 0x0);

    // 动态创建
    qe_queue *q = qe_queue_new(sizeof(int), 8);
    // 动态释放
    qe_free(q);

入队出队
================

.. code-block:: c

    int x, v;
    // 入队一个元素
    qe_queue_enq(q, &x);
    // 入队若干个元素
    qe_queue_enq_elems(q, &x, 8);

    // 出队一个元素
    int vs[8];
    qe_queue_deq(q, &v);
    // 出队若干个元素
    qe_queue_deq_elems(q, vs);

    // 获取一个队头元素但不出队
    int p, ps[8];
    qe_queue_peek(q, &p);
    // 从队头获取若干个元素但不出队
    qe_queue_peek_elems(q, ps, 8);

队列中剩余元素个数
====================

.. code-block:: c

    int size = qe_queue_wait(q);

