****************
Work Queue
****************

工作队列是利用优先级队列实现的一个延迟任务队列

Example 

.. code-block:: c


    // 该任务每分钟输出信息，表明程序未挂死
    static qe_ret work_alive(qe_workq *wq, qe_wq_worker *worker)
    {
        qe_debug("still alive");
        return qe_workqueue_schedule(wq, work_alive, 5000, QE_NULL);
    }

    // 该任务只运行一次，返回即删除任务
    static qe_ret work_future_init(qe_workq *wq, qe_wq_worker *worker)
    {
        struct future_info *future = (struct future_info *)worker->data;
        /* do init process */
        return qe_ok;
    }

    int main()
    {
        qe_u64 wait_ms;

        // 创建一个能够容纳8个任务的工作队列
        qe_workq *workqueue = qe_workqueue_new(8);

        // 创建两个延迟任务，分别在1秒和5秒后执行
        struct future_info future;
        qe_workqueue_schedule(workqueue, work_future_init, 1000, &future);
        qe_workqueue_schedule(workqueue, work_alive, 5000, QE_NULL);

        while (1) {
            // 轮询调度服务，传入当前时间，返回可等待时间
            wait_ms = qe_workqueue_service(workqueue, qe_time_ms());
            qe_msleep(wait_ms);
        }
    }