******************
List
******************

qelib中的双向链表借鉴了Linux kernel代码中的链表，可以依附于任何数据结构中

创建和初始化
==============
有两种方式创建链表，一种是在函数外部创建一个全局的静态链表结构，这种方式在
声明时已经初始化

.. code-block:: c

    static QE_LIST_INIT(client_list);

另一种是在函数内部，定义一个链表结构体或者将链表依附在某个结构体之中

.. code-block:: c

    typedef struct
    {
        int num_clients;
        qe_list client_list;
    } server;

    void server_init(server *s)
    {
        qe_list_init(&s->client_list);
        s->num_clients = 0;
    }

添加和删除
================
链表支持两种添加方式

.. code-block:: c

    // 头插
    void qe_list_prepend(qe_list *node, qe_list *existing);

    // 尾插
    void qe_list_append(qe_list *node, qe_list *existing);

删除元素使用如下函数

.. code-block:: c

    void qe_list_remove(qe_list *entry);

下面以一个例子来说明

.. code-block:: c

    typedef struct
    {
        char name[32];
        unsigned int addr;
        unsigned short port;
        qe_list list;
    } client;

    void add_client(server *s, client *c)
    {
        qe_list_append(&c->list, &s->client_list);
        s->num_clients++;
    }

    void del_client(server *s, client *c)
    {
        qe_list_remove(&c->list);
        s->num_clients--;
    }

遍历
========
在链表中查找元素需要遍历操作，遍历时不删除元素有两种写法

.. code-block:: c

    client *find_client(server *s, char *name)
    {
        client *c;
        struct qe_list_node *node = QE_NULL;

        qe_list_foreach(node, &s->client_list) {
            c = qe_list_entry(node, client, list);
            if (qe_strncmp(c->name, name, 32) == 0) {
                return c;
            }
        }

        return QE_NULL;
    }

    client *find_client2(server *s, char *name)
    {
        client *c;

        qe_list_foreach_entry(c, &s->client_list, list) {
            if (qe_strncmp(c->name, name, 32) == 0) {
                return c;
            }
        }

        return QE_NULL;
    }

遍历过程中需要删除元素，则需要使用带"safe"后缀的版本

.. code-block:: c

    void remove_client(server *s, char *name)
    {
        client *c, *tmp;

        qe_list_foreach_entry_safe(c, tmp, &s->client_list, list) {
            if (qe_strncmp(c->name, name, 32) == 0) {
                qe_list_remove(&c->list);
            }
        }
    }