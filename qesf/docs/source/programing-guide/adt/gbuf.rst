****************
General Buffer
****************

qelib提供了一个通用缓冲区gbuf，携带了容量、有效数据长度以及缓冲池gbuf_pool。
gbuf和gbuf_pool都不支持动态Resize

创建和释放gbuf
================

.. code-block:: c

    // 使用静态buffer初始化
    char buffer[1024];
    qe_gbuf gbuf;
    qe_gbuf_init(&gbuf, buffer, 1024);

    // 动态初始化
    qe_gbuf *gbuf = qe_gbuf_new(1024);
    // 动态释放
    qe_free(gbuf);

向gbuf中追加数据
================

.. code-block:: c

    char data1[32], data2[64];
    qe_gbuf_append(gbuf, data1, 32); // 追加后，数据长度变为32
    qe_gbuf_append(gbuf, data2, 64); // 追加后，数据长度变为32+64

追加数据是在原有数据后的空闲位置写入数据，数据长度增加

向gbuf中拷贝数据
================

.. code-block:: c

    char data1[64], data2[32];
    qe_gbuf_memcpy(gbuf, data1, 64);    // 拷贝后，数据长度变为64
    qe_gbuf_memcpy(gbuf, data2, 32);    // 拷贝后，数据长度变为32

拷贝数据是从gbuf的起始位置写入数据，数据长度为拷贝长度，如果gbuf中存在数据，会被覆盖

gbuf清空
================

.. code-block:: c

    qe_gbuf_clear(gbuf);

该操作会清除数据长度

gbuf回退
================

.. code-block:: c

    char data[] = {0x01, 0x02, 0x03, 0x04, 0x05};
    qe_gbuf_append(gbuf, data, sizeof(data));   // 追加数据
    qe_gbuf_backoff(gbuf, 2);   // 回退两个数据，变为 [0x03, 0x04, 0x05]

gbuf一些帮助宏
================

.. code-block:: c

    qe_gbuf_index(gbuf, i)              // 获取索引<i>处字节内容
    qe_gbuf_pos(gbuf)                   // 获取数据头位置
    qe_gbuf_offs_pos(gbuf, offs)        // 获取偏移<offs>位置
    qe_gbuf_free_pos(gbuf)              // 获取空闲头位置
    qe_gbuf_free_size_remove(gbuf, n)   // 手动减少空闲长度<n>
    qe_gbuf_data_size_append(gbuf, n)   // 手动增加数据长度<n>
    qe_gbuf_data_size(gbuf)             // 获取数据长度
    qe_gbuf_free_size(gbuf)             // 获取空闲长度
    qe_gbuf_size(gbuf)                  // 获取内存空间大小

缓冲池创建和释放
================

.. code-block:: c

    // 静态创建
    qe_gbuf_pool pool;
    qe_gbuf_pool_init(&pool);

    // 动态创建
    qe_gbuf_pool *pool = qe_gbuf_pool_new();
    // 动态释放
    qe_gbuf_pool_destroy(pool);

清空缓冲池
================

.. code-block:: c

    qe_gbuf_pool_clear(pool);

合并缓冲池到一个缓冲区
======================

.. code-block:: c

    // 该函数不会释放pool原本的内存
    qe_gbuf *gbuf = qe_gbuf_pool_merge(pool);

向缓冲池中添加一个gbuf
======================

.. code-block:: c

    qe_gbuf_pool_merge(pool, gbuf);

