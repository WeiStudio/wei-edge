****************
Rilling File
****************

回滚文件借鉴了RingBuffer的思想，提供了一个文件级的限定大小的缓存

Example
====================

.. code-block:: c

    static void remove_hook(char *name)
    {
        qe_debug("remove %s", name);
    }

    /*
     * 在/home/user/xxx路径下创建一个record.txt的回滚文件
     * 回滚数量为8
     * 当单个文件超过1024时触发回滚，回滚的文件名形式如record-20240101000000.txt
     * 回滚文件超过达到8个后，创建新的回滚时删除最老的文件
     */ 
    qe_rollingfile *rfile = qe_rollingfile_new("/home/user/xxx", "record", "txt", 8, 1024);

    // 注册Hook函数，监听删除文件事件
    qe_rollingfile_set_hook(rfile, QE_ROLLINGFILE_HOOK_BEFORE_REMOVE, remove_hook);

    // 向回滚文件中写入数据
    qe_rollingfile_append_buffer(rfile, buf, len);

    // 释放文件
    qe_rollingfile_free(rfile);


    