********************
参数解析器 Argparse
********************

qelib提供了一个通用参数解析器argparse，与标准库中的option作用类似，
能够从argc,argv中解析出操作参数，接口方式比option更加清晰和易用，借鉴
了Python的argparse

argparse现支持以下参数类型

* QE_OPT_BOOLEAN：布尔类型操作，用于解析获取不需要参数值的操作，设置到布尔变量
* QE_OPT_TRUE：真值操作， `QE_OPT_BOOLEAN` 的简化版，自动将真值赋予变量
* QE_OPT_FALSE：假值操作， `QE_OPT_BOOLEAN` 的简化版，自动将假值赋予变量
* QE_OPT_BIT：比特位类型操作，用于解析获取不需要参数值的操作，通过掩码的方式设置到特定位
* QE_OPT_MASK：设置掩码操作， `QE_OPT_BIT` 的简化版，自动将掩码设置到特定位
* QE_OPT_UNMASK：清除掩码操作， `QE_OPT_BIT` 的简化版，自动清除特定位的掩码
* QE_OPT_INTEGER：整型类型操作
* QE_OPT_FLOAT：浮点类型操作
* QE_OPT_STRING：字符串类型操作
* QE_ACT_TRUE：带回调函数的真值操作
* QE_ACT_FALSE：带回调函数的假值操作
* QE_ACT_MASK：带回调函数的掩码设置操作
* QE_ACT_UNMASK：带回调函数的掩码清除操作
* QE_ACT_INTEGER：带回调函数的整型操作
* QE_ACT_STRING：带回调函数的字符串操作
* QE_ACT_FLOAT：带回调函数的浮点操作
* QE_ACT_SUBCMD：子命令
* QE_OPT_HELP：帮助类型
* QE_OPT_END：结束操作，在操作数组中用于标识数组的结束

示例
================

.. code-block:: c


    #include "qelib.h"



    #define ARGP_LOG_DOMAIN      "argp-test"
    #define argp_debug(...)      qelog_debug(ARGP_LOG_DOMAIN, __VA_ARGS__)
    #define argp_info(...)       qelog_info(ARGP_LOG_DOMAIN, __VA_ARGS__)
    #define argp_notice(...)     qelog_notice(ARGP_LOG_DOMAIN, __VA_ARGS__)
    #define argp_warning(...)    qelog_warning(ARGP_LOG_DOMAIN, __VA_ARGS__)
    #define argp_error(...)      qelog_error(ARGP_LOG_DOMAIN, __VA_ARGS__)



    static qe_const_str usages[] = {
        "basic [options] [[--] args]",
        "basic [options]",
        QE_NULL,
    };

    static qe_ret action_string(qe_argparse *parser, qe_option *option)
    {
        qe_str string = qe_option_string(option);
        argp_debug("action string:%s", string);
        return qe_yield;
    }

    static qe_ret action_integer(qe_argparse *parser, qe_option *option)
    {
        qe_int integer = qe_option_integer(option);
        argp_debug("action integer:%d", integer);
        return qe_yield;
    }

    static qe_ret action_float(qe_argparse *parser, qe_option *option)
    {
        qe_double vfloat = qe_option_float(option);
        argp_debug("action float:%f", vfloat);
        return qe_yield;
    }

    static qe_ret action_true(qe_argparse *parser, qe_option *option)
    {
        qe_bool true = qe_option_boolean(option);
        argp_debug("action true:%d", true);
        return qe_yield;
    }

    static qe_ret action_false(qe_argparse *parser, qe_option *option)
    {
        qe_bool false = qe_option_boolean(option);
        argp_debug("action false:%d", false);
        return qe_yield;
    }

    static qe_ret action_mask(qe_argparse *parser, qe_option *option)
    {
        qe_u32 mask = qe_option_mask(option);
        argp_debug("action mask:0x%x", mask);
        return qe_yield;
    }

    static qe_ret action_unmask(qe_argparse *parser, qe_option *option)
    {
        qe_u32 unmask = qe_option_mask(option);
        argp_debug("action unmask:0x%x", unmask);
        return qe_yield;
    }

    static qe_ret trace_cmd(qe_argparse *parser, qe_option *option)
    {
        qe_int en;
        qe_int num;
        qe_int mode;
        qe_ret ret;
        qe_argparse subparser;

        qe_option options[] = {
            QE_OPT_INTEGER('e', "--enable", "trace enable", &en),
            QE_OPT_INTEGER('e', "--enable", "trace number", &num),
            QE_OPT_INTEGER('e', "--enable", "trace mode", &mode),
            QE_OPT_HELP('h', "--help", "trace help"),
            QE_OPT_END(),
        };

        qe_argp_init(&subparser, options, usages,
            "\ntrace command.", 
            "\ntrace command end.");
        ret = qe_argp_parse(&subparser, parser->argc, parser->argv);
        if (ret != qe_ok) {
            return qe_yield;
        }

        argp_debug("en:%d num:%d mode:%d", en, num, mode);

        return qe_yield;
    }

    int main(int argc, char **argv)
        qe_ret ret;
        qe_str string = QE_NULL;
        qe_bool true_bool = qe_false;
        qe_bool false_bool = qe_true;
        qe_int integer = 0;
        qe_u32 mask = 0x0;
        qe_u32 unmask = 0x1;
        qe_double vfloat;
        qe_argparse parser;
        qe_argparse paresr;     /* 创建一个解析器 */ 

        /* 操作数组 */
        qe_argp_option options[] = {

            /* 赋值命令组 */
            QE_OPT_GROUP("value test"), /* GROUP操作仅在帮助信息中显示 */
            /* 字符串赋值 */
            QE_OPT_STRING(QE_NULL, "val-str", "value string", &string),
            /* 真值赋值 */
            QE_OPT_TRUE(QE_NULL, "val-true", "value true", &true_bool),
            /* 假值赋值 */
            QE_OPT_FALSE(QE_NULL, "val-false", "value false", &false_bool),
            /* 整型赋值 */
            QE_OPT_INTEGER('n', "val-int", "value integer", &integer),
            /* 浮点赋值 */
            QE_OPT_FLOAT('f', "val-float", "value float", &vfloat),
            /* 掩码赋值 */
            QE_OPT_MASK(QE_NULL, "val-mask", "value bits mask", &mask, 0x1),
            QE_OPT_UNMASK(QE_NULL, "val-unmask", "value bits unmask", &unmask, 0x1),

            QE_OPT_GROUP("action test"),
            /* 字符串回调 */
            QE_ACT_STRING(QE_NULL, "act-str", "action string", 
                &string, QE_NULL, action_string),
            /* 整型回调 */
            QE_ACT_INTEGER(QE_NULL, "act-int", "action integer", 
                &integer, QE_NULL, action_integer),
            /* 浮点回调 */
            QE_ACT_FLOAT(QE_NULL, "act-float", "action float", 
                &vfloat, QE_NULL, action_float),
            /* 真值回调 */
            QE_ACT_TRUE(QE_NULL, "act-true", "action true",
                &true_bool, QE_NULL, action_true),
            /* 假值回调 */
            QE_ACT_FALSE(QE_NULL, "act-false", "action false",
                &false_bool, QE_NULL, action_false),
            /* 掩码回调 */
            QE_ACT_MASK(QE_NULL, "act-mask", "action mask",
                &mask, 0x1, action_mask),
            QE_ACT_UNMASK(QE_NULL, "act-unmask", "action unmask",
                &unmask, 0x1, action_unmask),

            QE_OPT_GROUP("subcmd test"),
            /* 子命令 */
            QE_OPT_SUBCMD("trace", "trace command", QE_NULL, trace_cmd),

            QE_OPT_HELP('h', "help", "Help message"),
            QE_OPT_END(),
        };

        /* 使用帮助 */
        static qe_const_str usages[] = {
            "basic [options] [[--] args]",
            "basic [options]",
            NULL,
        };

        /* 解析器初始化 */
        qe_argp_init(&parser, options, usages, 
            "\nBefore description.",
            "\nAfter description.");
        
        /* 开始解析 */ 
        ret = qe_argp_parse(&parser, argc, (qe_const_str *)argv);
        if (ret != qe_ok) {
            /* 返回qe_yield表示主动退出 */ 
            if (ret == qe_yield) {
                return 0;
            }
            qelog_error("argp", "parse error:%d", ret);
            return -1;
        }

        return 0;
    }