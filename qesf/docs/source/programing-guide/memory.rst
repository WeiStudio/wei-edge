*****************
内存管理
*****************

qelib希望为应用程序提供灵活的内存管理方案，并实现内存分配接口的统一化，目前支持几种形式

* nano版本
    * 由用户指定内存分配器
    * 如果配置使能了bget分配器用于内存管理，则需要用户指定内存池
* full版本，直接使用系统提供的内存管理接口

用户指定内存分配器
==================
通过 ``qemem_set_allocator()`` 来指定内存分配器

.. code-block:: c

    // 定义内存分配器
    static qemem_allocator xxx_allocator = {
        .malloc   = xxx_malloc,
        .calloc   = xxx_calloc,
        .realloc  = xxx_realloc,
        .free     = xxx_free,
        .pool     = xxx_pool,
        .minfo    = xxx_minfo,
        .flags    = QEMEM_POOL,
    };
    
    void sys_mem_init() 
    {
        // 设置内存分配器用于内存管理
        qemem_set_allocator(&xxx_allocator);
    }

配置bget内存分配器
==================
配置CONFIG_MEM_BGET为1的情况下，在程序中通过 ``qemem_set_pool()`` 来指定内存池

.. code-block:: c

    // 定义内存池
    static heap[HEAP_SIZE];
    
    void sys_mem_init()
    {
        // 设置内存分配器用于内存管理
        qemem_set_pool(heap, HEAP_SIZE);
    }

内存管理接口
==================

.. code-block:: c

    void *qe_malloc(qe_size size);
    void *qe_calloc(qe_size num, qe_size size);
    void *qe_realloc(void *buf, qe_size size);
    void  qe_free(void *ptr);