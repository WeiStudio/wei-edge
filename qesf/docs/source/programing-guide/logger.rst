***************
Logger
***************

qelib提供了一个非常灵活的日志模块，如果配置了 ``LOG_DEFAULT_HANDLER``，则默认使用内置的格式处理
函数对日志消息进行格式化处理，如果配置了 ``LOG_DEFAULT_OUTPUT``，则默认使用内置的输出处理函数进行
输出(默认通过printf打印)

日志等级
========
qelib定义了7个日志等级，如下表所示。qelib内部通过一个 ``loglevel`` 来对不同等级日志进行过滤，目前
的过滤原则是低于 ``loglevel`` 的日志将不会被处理和输出。qelib默认的日志等级是 ``QELOG_WARNING``，
在配置阶段可以通过 ``CONFIG_LOG_LEVEL_DEFAULT`` 来设定默认的日志等级，在应用程序中还可以通过在日志
初始化函数 ``qelog_init()`` 中传入参数来设置日志等级

+----------------+----------+
|      等级      |   描述   |
+================+==========+
| QELOG_DEBUG    | 调试     |
+----------------+----------+
| QELOG_INFO     | 信息     |
+----------------+----------+
| QELOG_NOTICE   | 注意     |
+----------------+----------+
| QELOG_WARNING  | 警告     |
+----------------+----------+
| QELOG_CRITICAL | 严重告警 |
+----------------+----------+
| QELOG_ERROR    | 错误     |
+----------------+----------+
| QELOG_FATAL    | 严重错误 |
+----------------+----------+

格式化
========
当使用默认内置的格式处理函数时，可以选择在输出时携带时间、日志域、等级、文件名、函数名、行号、PID、
颜色等信息，这些信息无法在配置阶段设置，qelib默认会按如下格式进行处理

.. code-block:: shell

    [H:M:S] <domain> <level> : <message>

应用程序可以通过在初始化函数 ``qelog_init()`` 中传入参数来设置格式，内置格式处理函数支持的所有格式选择
如下表所示。 ``QELOG_HMS、QELOG_DATE、QELOG_PTS`` 不能同时设置。颜色使用ANSI颜色编码

+------------+--------------------+
|   标志位   |        描述        |
+============+====================+
| QELOG_HMS  | 时分秒             |
+------------+--------------------+
| QELOG_DATE | 日期，年月日时分秒 |
+------------+--------------------+
| QELOG_PTS  | 带小数的时间戳     |
+------------+--------------------+
| QELOG_DM   | 日志域名称         |
+------------+--------------------+
| QELOG_LV   | 等级               |
+------------+--------------------+
| QELOG_FILE | 文件名             |
+------------+--------------------+
| QELOG_FUNC | 函数名             |
+------------+--------------------+
| QELOG_LINE | 行号               |
+------------+--------------------+
| QELOG_PID  | 进程号             |
+------------+--------------------+
| QELOG_CL   | 颜色               |
+------------+--------------------+

日志接口
========
在不使用日志域的情况下，按照不同等级，qelib提供了以下几个日志输出宏

* qe_debug
* qe_info
* qe_notice
* qe_warning
* qe_critical
* qe_error
* qe_fatal

应用程序可直接使用这些宏进行日志记录，只是因为默认日志等级为WARNING，低等级内容不会被输出

.. code-block:: c

    qe_debug("hello, this is year %d", 2024);
    qe_info("hello, this is year %d", 2024);
    qe_notice("hello, this is year %d", 2024);
    qe_warning("hello, this is year %d", 2024);
    qe_critical("hello, this is year %d", 2024);
    qe_error("hello, this is year %d", 2024);
    qe_fatal("hello, this is year %d", 2024);

自定义格式化处理和输出
======================
qelib支持自定义日志格式化处理和输出，用户可以通过 ``qelog_set_default_handler()`` 来设置
一个自定义的 ``handler`` 来进行格式化处理，通过 ``qelog_set_default_output()`` 来设置一个自定义的
输出方式

RingBuffer
================
qelib支持将每条日志写入一个用户提供的缓冲区中，如果在初始化时传入了 ``QELOG_RB`` 标志，可以
通过 ``qelog_set_buffer`` 来传入缓冲区地址和长度，内部会按照RingBuffer的形式将每条格式化处
理后的内容写入该缓冲区，该功能必须使用内置的默认格式化处理函数

日志归档
================
如果配置项 ``CONFIG_LOG_ARCHIVE`` 启用，使能了回滚文件功能，且初始化日志时传入了 
``QELOG_AR`` 标志位，则日志支持将每条内容记录到回滚文件中并压缩，要在程序中启用归档
功能，需要调用 ``qelog_set_archive()`` 指定回滚文件名、回滚个数、回滚文件大小、以
及压缩文件大小限制

.. code-block:: c

    // 初始化时声明要启用归档功能
    qelog_init(QELOG_INFO, QELOG_DATE|QELOG_DM|QELOG_LV|QELOG_AR);

    /*
     * 设置归档
     * 路径为/home/user/logdir
     * 文件名为test.log
     * 最大回滚5个文件，大小为32KB
     * 归档文件为test.log.zip，最大大小为1MB
     */
    qelog_set_archive("/home/user/logdir", "test", "log", 5, 32*1024, 1024*1024);

Hexdump & Bitdump
====================
编程中经常需要查看某个缓冲区或结构体的数据内容，qelib提供了两个非常方便的16进制和比特的dump功能

.. code-block:: c

    char buffer[32];

    qe_hexdump_debug(buffer, 32);
    qe_hexdump_info(buffer, 32);
    qe_hexdump_notice(buffer, 32);
    qe_hexdump_warning(buffer, 32);

    qe_bitdump_debug(buffer, 32);
    qe_bitdump_info(buffer, 32);
    qe_bitdump_notice(buffer, 32);
    qe_bitdump_warning(buffer, 32);

日志域
========
日志域是qelib日志模块非常强大的一个功能，强烈建议应用程序创建自己的日志域来进行日志记录。日志域的
思想是希望为程序的不同模块或者组件提供一个标识，这个标识就是一个字符串，例如你的程序中有一个专门处
理套接字收发的模块，可以标记为"socket"，或者有一个专门进行串口处理的模块，可以标记为"uart"。标记
字符串可以任意定义，相同的标记会被认为是同一个日志域

使用日志域的第一个好处是不同模块日志在使用内置格式化处理之后，会携带日志域名称，这样可以从连续的多
行日志中非常清晰的看到不同模块的日志

.. code-block:: shell

    [10:00:00] socket I : create tcp socket at 192.168.1.100:7658
    [10:00:00] socket I : connect to 192.168.1.1:6657
    [10:00:05] uart E : send buffer error
    [10:00:06] uart E : recv error
    [10:00:08] uart E : recv packet checksum error

另一个好处是可以为不同的日志域设置不同的等级，这样可以精确控制每个模块的输出等级

.. code-block:: c

    // 设置socket日志域的等级为NOTICE
    qelog_domain_set_level("socket", QELOG_NOTICE);
    // 设置uart日志域的等级为DEBUG
    qelog_domain_set_level("uart", QELOG_DEBUG);

    // 设置socket日志域的输出处理函数为socket_log_output
    void socket_log_output(const char *message)
    {
        fwrite(message, strlen(message), 1, fp);
    }
    qelog_domain_set_output("socket", socket_log_output);

    // 设置uart日志域的格式处理函数为uart_log_handler
    void uart_log_handler(const char *domain_name, qelog_level_e level, 
	const char *file, const char *func, int line, const char *message)
    {
        const char *lvlstr = qelog_get_level_str(level);
        printf("[%s][%s] %s.%s.%d : %s\r\n", domain_name, lvlstr, 
            file, func, line, message);
    }
    qelog_domain_set_handler("uart", uart_log_handler);


更强大的使用方法，你可以完全自定义不同日志域的格式化处理方式

日志域和自定义处理的关系
========================

如下图所示，当程序中使用 ``qe_debug、qe_info、qe_notice`` 等默认日志接口时，
使用的是默认日志域，日志处理和输出都是使用默认流程，而用户自定义日志域"aaa" 使用
``aaa_debug、aaa_info`` 记录日志未设置自定义处理，也会通过默认处理和输出。
"bbb"日志域自定义了处理函数，它的日志记录消息会通过 ``bbb_handler`` 处理，注意
"bbb"的日志输出也需要在 ``bbb_handler`` 中自行完成，Logger本身不再接管。"ccc"
日志域通过 ``qelog_set_default_output`` 修改了默认输出函数，则默认日志域、"aaa"
和"ccc"的日志格式化处理使用默认处理，输出都变更为自定义输出，"bbb"保持不变

.. image:: /images/logger-01.png