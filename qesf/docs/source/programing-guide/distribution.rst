*******************
QELIB Distribution
*******************

.. code-block::

    |-- apps：Host Building模式的测试程序
    |-- arch：架构相关代码和编译选项
    |-- cmake：cmake脚本
    |-- common 主要功能实现
        |-- qe_array.c：动态数组
        |-- qe_assert.c：断言
        |-- qe_bitmap.c：位图
        |-- qe_bitstream.c：比特流
        |-- qe_buffer.c：缓冲区实现
        |-- qe_error.c：错误处理
        |-- qe_hash.c：哈希表
        |-- qe_initcall.c：initcall实现
        |-- qe_log.c：日志
        |-- qe_math.c：数学函数
        |-- qe_pack.c：组包/解包相关
        |-- qe_queue：队列实现
        |-- qe_rollingfile：回滚文件
        |-- qe_service.c：服务函数
        |-- qe_string.c：字符串处理
        |-- qe_thread.c：线程相关
        |-- qe_time.c：时间相关
    |-- configs：默认配置
    |-- device：设备模型
    |-- docs：文档
    |-- include：头文件
    |-- lib：第三方库
        |-- bget：内存分配器
        |-- fdt：设备树
        |-- lz4：lz4压缩解压
        |-- yyjson：json格式处理
        |-- zlib：zlib压缩解压
    |-- release：安装发布路径
    |-- scripts：脚本
    CMakeLists.txt：顶层cmake 
    Kconfig：顶层kconfig
