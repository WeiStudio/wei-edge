*******************
Programming Guide
*******************

.. toctree::
   :maxdepth: 1

   
   文件结构 <distribution.rst>
   基本类型 <base-type.rst>
   错误类型 <error-type.rst>
   内存管理 <memory.rst>
   日志 <logger.rst>
   抽象数据类型 <adt/index.rst>
   initcall <initcall.rst>
   Shell <shell.rst>
   参数解析器 <argparse.rst>
   栈回溯 <backtrace/backtrace.rst>