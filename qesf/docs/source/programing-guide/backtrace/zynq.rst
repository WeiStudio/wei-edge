*********************************
Xilinx Zynq + FreeRTOS使用栈回溯
*********************************

Xilinx Zynq + FreeRTOS使用栈回溯的流程如下

1. 在Xilinx SDK中引用qelib
2. 修改链接脚本，标识代码段
3. 修改FreeRTOS源码
4. 覆盖qelib中的弱函数
5. 在代码中添加backtrace初始化函数
6. 注册异常处理函数
7. 在assert和异常处理函数中获取调用栈信息
8. addr2line工具解析调用栈

Xilinx SDK 引用 qelib
------------------------

请参考 `Xilinx SDK使用qelib <../../quick-start/xilinx.html>`_ 以获取更多信息

修改链接脚本
------------------------

在Application工程中，修改lscript.ld文件，添加代码段标识，用于backtrace初始化时传入

.. code-block:: c

    SECTIONS
    {
        .text : {
            _stext = .;     // 添加代码段起始标识
            KEEP (*(.vectors))
            ...
        } > ps7_ddr_0

        ...

        .fini : {
            ...
            _etext = .; // 添加代码段结束标识
        } > ps7_ddr_0
    }

修改FreeRTOS源码
------------------------

backtrace需要获取当前任务栈信息，而FreeRTOS本身未提供相关函数，因此需要对FreRTOS源码
进行修改

在tasks.c中，找到 ``tskTCB`` 结构体，按照如下示例添加成员 ``uxSizeOfStack`` ，用于
记录任务栈大小

.. code-block:: c

    typedef struct tskTaskControlBlock
    {
        volatile StackType_t	*pxTopOfStack;

        ...

        #if ( ( portSTACK_GROWTH > 0 ) || ( configRECORD_STACK_HIGH_ADDRESS == 1 ) )
            StackType_t		*pxEndOfStack;
        #else
            UBaseType_t     uxSizeOfStack;  // 添加这一行
        #endif

        ...
    } tskTCB;

在tasks.c末尾，添加以下两个函数，分别用于获取当前任务栈起始地址和大小

.. code-block:: c

    uint32_t *vTaskStackAddr()
    {
        return pxCurrentTCB->pxStack;
    }

    uint32_t vTaskStackSize()
    {
        #if ( portSTACK_GROWTH > 0 )

        return (pxNewTCB->pxEndOfStack - pxNewTCB->pxStack + 1);

        #else

        return pxCurrentTCB->uxSizeOfStack;

        #endif
    }

在task.h中，声明以上两个函数

.. code-block:: c

    uint32_t *vTaskStackAddr();
    uint32_t vTaskStackSize();

覆盖qelib中的弱函数
------------------------

backtrace中定义了几个平台相关的弱函数，需要外部进行覆盖实现，请在Application工程中
创建一个qe_arch.c文件，并填入以下内容

.. code-block:: c

    // FreeRTOS头文件
    #include "FreeRTOS.h"
    #include "task.h"
    // qelib头文件
    #include "qelib.h"
    // 如果需要使用打印功能，引用串口输出函数
    #include "xuartps_hw.h"

    // 导入主栈和代码段标识
    #ifdef __GNUC__
    extern qe_ubase _stack_end;
    extern qe_ubase __undef_stack;
    extern qe_ubase _stext;
    extern qe_ubase _etext;
    #endif

    // 获取主栈起始地址
    qe_ubase qe_main_stack_start(void)
    {
        return ((qe_ubase)&_stack_end);
    }

    // 获取主栈大小
    qe_uint qe_main_stack_size(void)
    {
        return ((qe_uint)&__undef_stack - (qe_uint)&_stack_end);
    }

    // 获取当前任务栈起始地址
    qe_ubase qe_task_stack_start(void)
    {
        return (qe_ubase)vTaskStackAddr();
    }

    // 获取当前任务栈大小
    qe_uint qe_task_stack_size(void)
    {
        return vTaskStackSize() * sizeof(StackType_t);
    }

    // 获取代码段起始地址
    qe_ubase qe_text_start(void)
    {
        return ((qe_ubase)&_stext);
    }

    // 获取代码段大小
    qe_uint qe_text_size(void)
    {
        return ((qe_uint)&_etext - (qe_uint)&_stext);
    }

    // 获取当前任务名
    char *qe_current_task_name(void)
    {
        TaskHandle_t handle = xTaskGetCurrentTaskHandle();
        return pcTaskGetName(handle);
    }

    // 如果需要使用backtrace的打印函数，需要实现以下函数
    void qe_putc(char c)
    {
        // 默认打印到标准输出串口设备，可修改
        XUartPs_SendByte(STDOUT_BASEADDRESS, c);
    }

在代码中添加backtrace处理
--------------------------

需要在应用程序初始化阶段对backtrace进行初始化，通过上文实现的函数传入主栈和代码段信息，
使用Xilinx BSP中提供的异常注册函数注册取指异常、数据异常和未定义异常处理函数，然后在
所有异常处理函数中调用backtrace相关API获取调用栈信息

以下示例代码展示了如何注册异常处理函数，以及在不同的回调函数中如何使用backtrace，注意
异常函数中调用fault类型API


.. code-block:: c

    #include "FreeRTOS.h"
    #include "task.h"
    #include "qelib.h"
    #include "xil_exception.h"

    // FreeRTOS的应用程序Assert函数中获取调用栈信息
    void vApplicationAssert(const char *file, uint32_t line)
    {
        // 获取结构体形式的调用栈信息
        qe_backtrace_info info;
        qe_backtrace_call_stack(qe_get_fp(), &info);
        while(1);
    }

    // FreeRTOS的栈溢出钩子函数中获取调用栈信息
    void vApplicationStackOverflowHook(TaskHandle_t task, char *name)
    {
        qe_backtrace_print(qe_get_fp());
        while(1);
    }

    // 取指异常函数中获取调用栈信息
    static void exception_prefertch_abort(void *args)
    {
        // 获取结构体形式的调用栈信息
        qe_backtrace_info info;
        qe_backtrace_call_stack_fault(qe_get_fp(), &info);
        // 调用栈信息保存到flash
        flash_erase(...);
        flash_write(..., &info, sizeof(info));
        // 系统复位
        sys_reset();
    }

    // 数据异常函数中获取调用栈信息
    static void exception_data_abort(void *args)
    {
        // 获取字符串形式的调用栈信息到buffer
        char buffer[1024];
        qe_backtrace_get_str_fault(qe_get_fp(), buffer, 1024);
        // 打印buffer内容
        xil_printf("%s", buffer);
        while(1);
    }

    // 未定义异常函数中获取调用栈信息
    static void exception_undefined(void *args)
    {
        // 直接打印调用栈信息，输出方向由qe_putc定义
        qe_backtrace_print_fault(qe_get_fp());
        while(1);
    }

    void main(void)
    {
        ...

        // backtrace初始化
        qe_backtrace_init(
            qe_main_stack_start(),
            qe_main_stack_size(),
            qe_text_start(),
            qe_text_size());

        // 注册异常处理函数
        Xil_ExceptionRegisterHandler(
            XIL_EXCEPTION_ID_PREFETCH_ABORT_INT,
            exception_prefertch_abort, HY_NULL);

        Xil_ExceptionRegisterHandler(
            XIL_EXCEPTION_ID_DATA_ABORT_INT,
            exception_data_abort, HY_NULL);

        Xil_ExceptionRegisterHandler(
            XIL_EXCEPTION_ID_UNDEFINED_INT,
            exception_undefined, HY_NULL);

        ...
    }

字符串形式和直接打印的栈回溯内容如下

.. code-block:: shell

    -------- HYLIB Backtrace Start --------
    arch        : Cortex-A      # 处理器架构
    stack       : task shell    # 异常触发的任务
    stack start : 001ce3e0      # 异常触发的栈信息
    stack size  : 00001000
    fp          : 02390fe4      # 栈帧寄存器
    last frame -->              # 最后记录的栈帧内容
        02390fe0 001cefcc(fp)
        02390fe4 0010005c(lr)
    fault saved -->             # 异常保存的寄存器组
        02390fe8 00000000(r0)
        02390fec 001cf044(r1)
        02390ff0 00000000(r2)
        02390ff4 60000000(r3)
        02390ff8 001d0473(r12)
        02390ffc 001283a4(lr)
    calls dump -->              # 调用栈地址，最为重要的信息
        0010005c
        001283a4
        001304b0
    stack dump -->              # 最后记录的栈帧以下32个字的内容
        00000000 00000000 00000000 00000000
        00000000 00000000 60000000 00000000
        001cefdc 001cf044 001cf108 00000007
        001304b0 00000000 00000000 00000000
        00000000 00000000 0018b134 0018b160
        00169250 00000000 0018b158 00000002
        0018b15c 001cf134 00128774 00000000
        00000000 0018b158 00000002 00000003
    -------- HYLIB Backtrace End --------

addr2line解析调用栈
------------------------

addr2line工具能够结合ELF文件，将代码段地址转换为函数名和代码行号

.. code-block:: shell

    arm-none-eabi-addr2line.exe -e app.elf -f 0010005c
    FreeRTOS_DataAbortHandler
    port_asm_vectors.S:106

    arm-none-eabi-addr2line.exe -e app.elf -f 0010005c
    call_data_abort
    cmd_backtrace.c:11

    arm-none-eabi-addr2line.exe -e app.elf -f 001304b0
    qe_argp_parse
    hard/crtn.o:?