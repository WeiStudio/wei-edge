*******************
Backtrace | 栈回溯
*******************

QELIB提供了一个简易的栈回溯功能 ``backtrace`` ，能够在程序出现异常崩溃时记录栈帧现场情况以及函数调用栈，对于分析程序崩溃原因有很大帮助

字符串形式或直接打印输出的栈回溯信息如下

.. code-block:: shell

    -------- QELIB Backtrace Start --------
    arch        : Cortex-A      # 处理器架构
    stack       : task shell    # 异常触发的任务
    stack start : 001ce3e0      # 异常触发的栈信息
    stack size  : 00001000
    fp          : 02390fe4      # 栈帧寄存器
    last frame -->              # 最后记录的栈帧内容
        02390fe0 001cefcc(fp)
        02390fe4 0010005c(lr)
    fault saved -->             # 异常保存的寄存器组
        02390fe8 00000000(r0)
        02390fec 001cf044(r1)
        02390ff0 00000000(r2)
        02390ff4 60000000(r3)
        02390ff8 001d0473(r12)
        02390ffc 001283a4(lr)
    calls dump -->              # 调用栈地址，最为重要的信息
        0010005c
        001283a4
        001304b0
    stack dump -->              # 最后记录的栈帧以下32个字的内容
        00000000 00000000 00000000 00000000
        00000000 00000000 60000000 00000000
        001cefdc 001cf044 001cf108 00000007
        001304b0 00000000 00000000 00000000
        00000000 00000000 0018b134 0018b160
        00169250 00000000 0018b158 00000002
        0018b15c 001cf134 00128774 00000000
        00000000 0018b158 00000002 00000003
    -------- QELIB Backtrace End --------

适用条件
--------

* 目前仅适用 Xilinx Zynq + FreeRTOS 开发环境
* 建议编译选项包含 ``-g`` 调试选项
* 初始化时需要外部提供主栈和代码段信息
* 需要结合交叉编译工具 ``addr2line`` 以及程序对应的ELF文件一起分析

特性
--------

* 支持非fault和fault情况下的栈回溯记录，使用两套独立的API区分
* 支持提取调用栈信息到结构体、缓冲区或者直接打印输出


backtrace API
----------------

fault和非fault
================

在使用backtrace时必须清楚程序所处上下文为正常运行上下文还是异常上下文，正常上下文指的是sp栈指针
指向位置为正常代码位置，例如程序初始化、任务的运行，assert由于是软件主动报告错误，因此也算作正常上下文。
异常上下文指的是由于各种原因处理器打断了程序的正常执行，自动触发了异常处理过程，例如ARM的异常handler，
此时为异常上下文。backtrace的fault相关函数是为异常上下文准备的，非fault相关函数为正常上下文准备

初始化
========

backtrace初始化，在初始化阶段调用

.. code-block:: c

    void qe_backtrace_init(qe_ubase stack_start, qe_uint stack_size,
        qe_ubase code_start, qe_uint code_size);

* ``stack_start`` 程序主栈起始地址
* ``stack_size`` 程序主栈大小
* ``code_start`` 程序代码段起始地址
* ``code_start`` 程序代码段大小

获取结构体形式调用栈信息
========================

.. code-block:: c

    // 该函数可在初始化后的任意位置调用，获取结构体形式的调用栈信息
    void qe_backtrace_call_stack(qe_ubase fp, qe_backtrace_info *info);

    // 该函数用于在异常上下文获取结构体形式的调用栈信息
    void qe_backtrace_call_stack_fault(qe_ubase fp, qe_backtrace_info *info);

* ``fp`` 栈帧寄存器的值
* ``info`` 调用栈信息

获取字符串形式调用栈信息
========================

.. code-block:: c

    // 该函数可在初始化后的任意位置调用，获取字符串形式的调用栈信息
    qe_int qe_backtrace_get_str(qe_ubase fp, char *buffer, int length);

    // 该函数用于在异常上下文获取字符串形式的调用栈信息
    qe_int qe_backtrace_get_str_fault(qe_ubase fp, char *buffer, int length);

* ``fp`` 栈帧寄存器的值
* ``buffer`` 用于接收字符串形式调用栈信息的缓冲区
* ``length`` 缓冲区大小
* 返回值为调用栈信息字符串长度

直接打印调用栈信息
=======================

以下两个函数内部使用 ``qe_printf`` 函数进行字符串的输出，依赖 ``qe_putc`` 的实现

.. code-block:: c

    // 该函数可在初始化后的任意位置调用，直接打印调用栈信息
    void qe_backtrace_print(qe_ubase fp);

    // 该函数用于在异常上下文打印调用栈信息
    void qe_backtrace_print_fault(qe_ubase fp);

* ``fp`` 栈帧寄存器的值

调用栈信息结构体
=======================

``qe_backtrace_info`` 定义在 qe_backtrace.h头文件中

.. code-block:: c

    /* Cortex AArch32 */
    #if defined(__arm__)
    typedef struct
    {
        qe_ubase r0;
        qe_ubase r1;
        qe_ubase r2;
        qe_ubase r3;
        qe_ubase r4;
        qe_ubase r5;
        qe_ubase r6;
        qe_ubase r7;
        qe_ubase r8;
        qe_ubase r9;
        qe_ubase r10;
        qe_ubase fp;    /* r11 */
        qe_ubase r12;   /* r12 */
        qe_ubase sp;    /* r13 */
        qe_ubase lr;    /* r14 */
        qe_ubase pc;    /* r15 */
    } qe_backtrace_regs;
    #endif

    typedef struct
    {
        char taskname[16];      // 任务名称

        qe_bool is_main_stack;  // 调用栈是否位于主栈
        qe_bool is_on_fault;    // 调用栈是否位于异常上下文

        qe_backtrace_regs fault;    // 异常保存的上下文寄存器 架构相关
        qe_backtrace_regs frame;    // 异常栈帧 架构相关

        qe_ubase fp;                // 最后栈帧位置
        qe_ubase stack_start;       // 栈起始地址
        qe_ubase stack_size;        // 栈大小

        qe_uint calls_depth;        // 记录的调用深度
        qe_ubase calls[QE_BACKTRACE_CALLS_DEPTH];   // 调用地址

        qe_uint stack_depth;        // 记录的栈深度
        qe_uint stack[QE_BACKTRACE_STACK_DEPTH];    // 栈内容

    } qe_backtrace_info;

使用示例
--------

.. toctree::
    :maxdepth: 1

    Xilinx Zynq + FreeRTOS使用栈回溯 <zynq.rst>