******************
initcall
******************

qelib支持模块导出功能，能够让应用程序模块化开发，各模块的加载不需要显式的在main函数中添加调用，而是使用qelib提供的
模块导出宏定义对模块函数进行修饰，模块即可隐式的注册到qelib中。使用initcall的前提是对应平台的开发环境中能够自定义链
接脚本。以下是应用程序模块化的例子

.. code-block:: c

    // foo.c
    static int foo_init()
    {
        /*
         * 创建任务、线程、或者执行一些初始化 
         */
        return 0;
    }
    // 导出foo_init函数
    QE_APP_EXPORT(foo_init);

    // bar.c
    static int bar_init()
    {
        /*
         * 创建任务、线程、或者执行一些初始化 
         */
        return 0;
    }
    // 导出bar_init函数
    QE_APP_EXPORT(bar_init);

    // main.c
    int main()
    {
        /* do some init */

        // 调用 qe_initcall，会自动
        qe_initcall();
    }

当调用 ``qe_initcall()`` 时会自动加载运行在其他模块中导出的符号，而不需要显式的在main.c中引用模块头文件并调用模块初始化函数

为了区分不同阶段的初始化顺序，qelib提供了6个模块导出宏，定义如下

* ``QE_ARCH_EXPORT`` ：架构初始化
* ``QE_BOARD_EXPORT`` ：板级初始化
* ``QE_PREV_EXPORT`` ：设备预初始化
* ``QE_DEVICE_EXPORT`` ：设备初始化
* ``QE_COMPONENT_EXPORT`` ：组件初始化
* ``QE_ENV_EXPORT`` ：环境变量初始化
* ``QE_APP_EXPORT`` ：应用初始化

在链接脚本中的.init段或者.text段中，手动添加以下内容

.. code-block:: 

    . = ALIGN(4);
    __qe_init_start = .;
    KEEP(*(SORT(.qesi_fn*)))
    __qe_init_end = .;
    . = ALIGN(4);