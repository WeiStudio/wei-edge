****************
Shell
****************

qelib内置了一个命令行shell工具，支持按键、函数、可执行exec、变量等多种内容的操作

配置
================

在编译时，首先需要使能Shell

menuconfig:

.. code-block:: shell

    |-- Common Setup
        |-- Enable shell 使能shell
            |-- Shell show information 认证后输出shell信息
            |-- Shell show user information 认证后输出用户自定义信息
            |-- Shell using symbol export shell命令使用符号导出
            |-- Shell history max number 最大历史记录条数
            |-- Shell function max parameters number 函数最大参数个数
            |-- Shell default user name 默认用户名
            |-- Shell default password 默认密码
            |-- Shell help show variables 帮助命令输出变量列表
            |-- Shell support end line mode 支持尾行模式

对应的配置项为

* ENABLE_SHELL 使能shell
* SHELL_SHOW_INFO 认证后输出shell信息
* SHELL_SHOW_USR_INFO 认证后输出用户自定义信息
* SHELL_USING_EXPORT shell命令使用符号导出
* SHELL_HISTORY_MAX_NUM 最大历史记录条数
* SHELL_PARAM_MAX_NUM 函数最大参数个数
* SHELL_DEFAULT_USER 默认用户名
* SHELL_DEFAULT_PSWD 默认密码
* SHELL_HELP_LIST_VAR 帮助命令输出变量列表
* SHELL_SUPPORT_END_LINE 支持尾行模式

命令表
================
如果配置不支持 `SHELL_USING_EXPORT`，则需要用户自己创建一个类型为 `qe_shell_command`
的命令表，在初始化时将表地址和表项个数传入

* QE_SHELL_KEY_ITEM 构建一个按键命令
* QE_SHELL_USR_ITEM 构建一个账户
* QE_SHELL_VAR_INT 构建有个整型变量
* QE_SHELL_VAR_STR 构建一个字符串变量
  
.. code-block:: c

    int test_num;
    char *test_src = "string";

    static void test_exec(int argc, char *argv[])
    {
        /**/
    }

    static int test_func(qe_bool en, qe_int a, char *b)
    {
        /**/
    }

    static qe_shell_command shell_cmds[] = {

        /* qelib提供的默认账户，需要手动添加到命令表中 */
        QE_SHELL_USR_ITEM(QE_SHELL_DEFAULT_USER, QE_SHELL_DEFAULT_PSWD, default user);
        
        /* qelib提供的按键命令，需要手动添加到命令表中 */
        QE_SHELL_KEY_ITEM(QE_SHELL_KEY_CODE_UP, qe_shell_key_up, up);   // 上
        QE_SHELL_KEY_ITEM(QE_SHELL_KEY_CODE_DOWN, qe_shell_key_down, down); // 下
        QE_SHELL_KEY_ITEM(QE_SHELL_KEY_CODE_RIGHT, qe_shell_key_rigth, right); // 右
        QE_SHELL_KEY_ITEM(QE_SHELL_KEY_CODE_LEFT, qe_shell_key_left, left); // 左
        QE_SHELL_KEY_ITEM(QE_SHELL_KEY_CODE_ENTER, qe_shell_key_enter, enter); // 回车
        QE_SHELL_KEY_ITEM(QE_SHELL_KEY_CODE_BACKSPACE, qe_shell_key_backspace, backspace); //退格
    
        /* 创建两个变量命令，关联到 test_src和test_str */
        QE_SHELL_VAR_INT(test num, &c, test num);
        QE_SHELL_VAR_STR(test str, &test_src, test str);

        /* 创建一个exec命令 */
        QE_SHELL_CMD_EXEC(test exec, test_exec, test exec);

        /* 创建一个可以接收3个命令参数的func命令 */
        QE_SHELL_CMD_FUNC(3, test func, test_func, test func)
    };

推荐使用 `SHELL_USING_EXPORT`，只需要在需要导出的变量、函数处通过宏定义导出，初始化时
不需要提供表，而默认账户、按键命令已经在qelib中导出，不需要用户显式导出

.. code-block:: c


    int test_num;
    char *test_src = "string";

    QE_SHELL_EXPORT_VAR_INT(test num, &test_num, test num);
    QE_SHELL_EXPORT_VAR_STR(test str, &test_str, test str);

    static void test_exec(int argc, char *argv[])
    {
        /**/
    }
    QE_SHELL_EXPORT_CMD_EXEC(test exec, test_exec, test exec);

    static int test_func(qe_bool en, qe_int a, char *b)
    {
        /**/
    }
    QE_SHELL_EXPORT_CMD_FUNC(3, test func, test_func, test func);

初始化shell
=============

初始化shell使用函数 `qe_shell_init()` 。不使用 `SHELL_USING_EXPORT` 的情况下，函数
`qe_shell_init()` 需要传入命令表

.. code-block:: c

    qe_shell shell;
    qe_shell_init(&shell, 1024, shell_cmds, qe_array_size(shell_cmds), 
        QE_SHELL_DEFAULT_USER, shell_read, shell_write);

其中，shell变量是shell的上下文结构体，`QE_SHELL_DEFAULT_USER` 是shell要认证的账户，
`shell_read` 和 `shell_write` 是读写回调函数，1024是shell使用的缓冲区内存空间

使用 `SHELL_USING_EXPORT` 的情况下，函数 `qe_shell_init()` 不需要传入命令表

.. code-block:: c

    qe_shell shell;
    qe_shell_init(&shell, 1024, QE_SHELL_DEFAULT_USER, shell_read, shell_write);

读写回调
=============

读写回调函数用于shell从一个外设读取字符串以及写入字符串

在读回调中，用户需要从外设读取 `len`字节数据到 `data` 指针中

.. code-block:: c

    static int shell_read(char *data, int len)
    {
        /*read operation*/
    }

在写回调中，用户需要将 `len` 字节数从 `data` 写入到外设

.. code-block:: c

    static int shell_write(char *data, int len)
    {
        /*write operation*/
    }

shell运行
================

shell运行本质上就是在死循环中调用`qe_shell_handler`处理数据，需要从外设读取1字节数据，然后传递给
`qe_shell_handler`

.. code-block:: c

    while (1) {
        /*read data*/
        qe_shell_handler(&shell, &data);
    }
