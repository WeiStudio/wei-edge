Introduction
============

QELIB，全称Quick Embeded Library，是一个应用于嵌入式快速开发的软件框架。
提供除操作系统内核外的各项低层次软件功能和组件，使得开发者能够在QESF之上方便
快速的进行应用程序开发。

Features
------------

* 支持基于python kconfiglib和基于头文件的配置方案
* sphinx docs
* nano版本: 替代标准C库，QESF实现了标准C库中多个常用函数，例如memset、memcpy、memmove、memcmp、strstr、strncpy、strncmp、strnlen、vsnprintf、snprintf等
* 灵活的内存管理方案
* 平台分离，QESF与目标平台无关，所有和平台相关函数功能定义和实现都在qe_arch.c/.h中，由用户自定义实现
* 统一的设备驱动框架
* initcall

Roadmap
------------

* 集成轻量级协议栈
* 集成轻量级文件系统
* 集成Shell
* 集成树和图