/**
 * @file qe_hash.h
 * @author Wei.Studio
 * @brief Hash Table
 * @version 0.1
 * @date 2024-03-09
 * 
 * @copyright Copyright (c) 2024 Wei.Studio
 * 
 */



#ifndef __QE_HASH_H__
#define __QE_HASH_H__



#include "qe_def.h"



typedef qe_uint (*qe_hash_func)(qe_const_ptr key);

typedef qe_bool (*qe_hash_equal_func)(qe_const_ptr a, qe_const_ptr b);

typedef qe_bool (*qe_hash_r_func)(qe_ptr key, qe_ptr val, qe_ptr user);

typedef struct qe_hash_table qe_hashtab;

typedef struct
{
    qe_hashtab *tab;
    qe_int      pos;
}qe_hashtab_iter;

qe_hashtab *qe_hashtab_new(qe_hash_func hash_func, qe_hash_equal_func equal_func);

qe_hashtab *qe_hashtab_new_full(qe_hash_func       hash_func,
                                qe_hash_equal_func equal_func,
                                qe_destroy_notify  key_destroy_func,
                                qe_destroy_notify  val_destroy_func);

void qe_hashtab_destroy(qe_hashtab *tab);

qe_bool qe_hashtab_insert(qe_hashtab *tab, qe_ptr key, qe_ptr val);

qe_bool qe_hashtab_replace(qe_hashtab *tab, qe_ptr key, qe_ptr val);

qe_bool qe_hashtab_add(qe_hashtab *tab, qe_ptr key);

qe_bool qe_hashtab_remove(qe_hashtab *tab, qe_const_ptr key);

qe_bool qe_hashtab_steal(qe_hashtab *tab, qe_const_ptr key);

qe_uint qe_hashtab_size(qe_hashtab *tab);

qe_ptr qe_hashtab_lookup(qe_hashtab *tab, qe_const_ptr key);

qe_uint qe_str_hash(qe_const_ptr v);
qe_bool qe_str_equal(qe_const_ptr v1, qe_const_ptr v2);

qe_uint qe_direct_hash(qe_const_ptr v);

qe_bool qe_direct_equal(qe_const_ptr v1, qe_const_ptr v2);

qe_bool qe_int_equal(qe_const_ptr v1, qe_const_ptr v2);

qe_uint qe_int_hash(qe_const_ptr v);

qe_bool qe_uint_equal(qe_const_ptr v1, qe_const_ptr v2);

qe_uint qe_uint_hash(qe_const_ptr v);

qe_bool qe_int64_equal(qe_const_ptr v1, qe_const_ptr v2);

qe_uint qe_int64_hash(qe_const_ptr v);

qe_bool qe_double_equal(qe_const_ptr v1, qe_const_ptr v2);

qe_uint qe_double_hash(qe_const_ptr v);


void qe_hashtab_iter_init(qe_hashtab_iter *iter, qe_hashtab *tab);

qe_bool qe_hashtab_iter_next(qe_hashtab_iter *iter, 
    qe_ptr *key, qe_ptr *val);

#endif /* __QE_HASH_H__ */ 