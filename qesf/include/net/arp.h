


#ifndef __ARP_H__
#define __ARP_H__



#include "net.h"



void arp_request(void);
void arp_raw_request(ipaddr_t src_ip, const qe_u8 *target_ether,
	ipaddr_t target_ip);
void arp_receive(struct ethernet_hdr *et, struct ip_udp_hdr *ip, int len);
int arp_table_lookup_byipaddr(ipaddr_t ip);


#endif /* __ARP_H__ */
