


#ifndef __PING_H__
#define __PING_H__



#include "net.h"



/*
 * Deal with the receipt of a ping packet
 *
 * @param et Ethernet header in packet
 * @param ip IP header in the same packet
 * @param len Packet length
 */
void ping_receive(struct ethernet_hdr *et, struct ip_udp_hdr *ip, int len);



#endif /* __PING_H__ */