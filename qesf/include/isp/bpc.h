


#ifndef __BPC_H__
#define __BPC_H__



#include "qe_def.h"



#define BPC_XP_STATIC       0x01
#define BPC_XP_MANUAL       0x02
#define BPC_XP_DETECT       0x04
#define BPC_XP_MEM_DYNAMIC  0x80


typedef enum {
    BPTAB_SEQ_4K = 0,   /* Sequence table, max 4095 elements */
    BPTAB_SEQ_8K,       /* Sequence table, max 8191 elements */
    BPTAB_SEQ_V2,       /* Sequence table v2, variable length */
    BPTAB_BMP_CA,       /* Bitmap table, column arrangement */
    BPTAB_BMP_RA,       /* Bitmap table, row arrangement */
    BPTAB_SIZEOF,
} bptab_type_e;


/**
 * Normal format point 
 */
struct bpc_npoint {
    qe_u16 x;
    qe_u16 y;
    qe_u8  flags;
    qe_u8  reserve_bytes[1];
    qe_list   list;
};
typedef struct bpc_npoint bpc_npoint_t;


/**
 * Exchange format point 
 */
struct bpc_xpoint {
    qe_u16 x;
    qe_u16 y;
    qe_u8  direct;
    qe_u8  flags;
    qe_u8  reserve_bytes[2];
};
typedef struct bpc_xpoint bpc_xpoint_t;


/**
 * BPC Descriptor
 */
struct bpc_desc {

    qe_dim2 dim;

    unsigned int num_total;
    unsigned int num_static;
    unsigned int num_dynamic;

    void *compute_mem_addr;
    unsigned int compute_mem_size;

    qe_u8 fmt;
    qe_u8 rbytes[3];

    struct bpc_xpoint *xtab;
    
    qe_list dynamic_list;     /* Dynamic add points record */
};
typedef struct bpc_desc bpc_desc_t;



#endif /* __BPC_H__ */