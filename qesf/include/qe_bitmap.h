


#ifndef __QE_BITMAP_H__
#define __QE_BITMAP_H__



#include "qe_def.h"



typedef unsigned long qe_bits;
typedef qe_bits qe_bitmap;



/* Bitmap */
#define qe_bitmap_init(name, bits) \
    qe_bits name[QE_BITS_TO_LONG(bits)]
#define QE_BITMAP_FIRST_WORD_MASK(start) (~0UL << ((start) % QE_BITS_PER_LONG))
#define QE_BITMAP_LAST_WORD_MASK(nbits)					\
(									\
	((nbits) % QE_BITS_PER_LONG) ?					\
		(1UL<<((nbits) % QE_BITS_PER_LONG))-1 : ~0UL		\
)



/**
 * @brief 
 * 
 * @param bit 
 * @param p 
 */
static inline void qe_set_bit(qe_uint bit, volatile qe_bits *p)
{
	qe_bits mask = 1UL << (bit & (QE_BITS_PER_LONG-1));
	p += bit >> QE_LONG_POW;
	*p |= mask;
}

static inline void qe_clear_bit(qe_uint bit, volatile qe_bits *p)
{
	qe_bits mask = 1UL << (bit & (QE_BITS_PER_LONG-1));
	p += bit >> QE_LONG_POW;
	*p &= ~mask;
}

static inline void qe_change_bit(qe_uint bit, volatile qe_bits *p)
{
	qe_bits mask = 1UL << (bit & (QE_BITS_PER_LONG-1));
	p += bit >> QE_LONG_POW;
	*p ^= mask;
}

static inline int qe_test_bit(unsigned int bit, volatile qe_bits *p)
{
	return 1UL & (p[QE_BITS_WORD(bit)] >> (bit & (QE_BITS_PER_LONG-1)));
}

qe_bool qe_bitmap_empty(qe_bitmap *map, qe_uint bits);
qe_bool qe_bitmap_full(qe_bitmap *map, qe_uint bits);
void qe_bitmap_set(qe_bitmap *map, qe_uint start, qe_uint nr);
void qe_bitmap_clear(qe_bitmap *map, qe_uint start, qe_uint nr);
void qe_bitmap_zero(qe_bitmap *map, qe_uint nbits);
void qe_bitmap_fill(qe_bitmap *map, qe_uint nbits);
qe_bitmap *qe_bitmap_new(qe_uint num_bits);


#endif