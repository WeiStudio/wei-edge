/**
 * @file qe_trace.h
 * @author Wei.Studio
 * @brief Trace
 * @version 0.1
 * @date 2024-07-09
 * 
 * @copyright Copyright (c) 2024 Wei.Studio
 * 
 */



#ifndef __QE_TRACE_H__
#define __QE_TRACE_H__



#include "qe_def.h"



qe_ret qe_vtrace_init(void);
qe_ret qe_vtrace_printf(const char *fmt, ...);



#endif /* __QE_TRACE_H__ */