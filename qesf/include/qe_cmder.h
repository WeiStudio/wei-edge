
#ifndef __QE_CMDER_H__
#define __QE_CMDER_H__



#include "qe_def.h"



struct qe_cmder {
	qe_list tcvr_list;
	qe_u32 tx_ring_tail;
	struct qe_ring *tx_ring;
};

struct qe_cmder_transceiver
{
	char *name;
	struct qe_device *dev;
	qe_ret (*open)(struct qe_cmder_transceiver *);
	qe_ret (*rx)(struct qe_cmder_transceiver *);
	qe_ret (*tx)(struct qe_cmder_transceiver *, void *, int );
	struct qe_ringbuffer *rx_ring;
	void *private;
	qe_list list;
	qe_list proto_list;
	qe_u32 activated:1;
	qe_u32 reserve:31;
};

struct qe_cmder_protocol {
	char *name;
	qe_ret (*scan)(struct qe_cmder_protocol *, void *, int, int *);
	qe_ret (*match)(struct qe_cmder_protocol *, void *, int, int *);
	qe_ret (*read)(struct qe_cmder_protocol *, void *, int);
	qe_ret (*write)(struct qe_cmder_protocol *, void *, int);
	qe_ret (*connect)(struct qe_cmder_protocol *);
	qe_ret (*disconnect)(struct qe_cmder_protocol *);
	qe_u32 initialized:1;
	qe_u32 f_reserves:31;
	struct qe_cmder *cmder;
	struct qe_cmder_transceiver *tcvr;
	void *private;
    void *user;
	qe_list list;
};

struct qe_cmder_message {
	char *tcvr_name;
	void *payload;
	int length;
};


struct qe_cmder_transceiver *qe_cmder_tcvr_find(struct qe_cmder *cmder, const char *name);
struct qe_cmder_transceiver *qe_cmder_tcvr_find_bydev(struct qe_cmder *cmder, const char *name);
qe_ret qe_cmder_tcvr_open(struct qe_cmder *cmder);
qe_ret qe_cmder_proto_write(struct qe_cmder_protocol *proto, void *buf, int len, qe_bool async);
qe_ret qe_cmder_proto_register(struct qe_cmder *cmder,
											 struct qe_cmder_protocol *proto,
											 char *tcvr_name);
qe_ret qe_cmder_tcvr_register(struct qe_cmder *cmder,
										   const char *name,
										   qe_ret (*open)(struct qe_cmder_transceiver *),
										   qe_ret (*rx)(struct qe_cmder_transceiver *),
										   qe_ret (*tx)(struct qe_cmder_transceiver *, void *, int),
										   qe_u32 bufsz);
#endif /* __QE_CMDER_H__ */
