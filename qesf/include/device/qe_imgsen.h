
#ifndef __QE_IMGSENSOR_H__
#define __QE_IMGSENSOR_H__



#include "qe_device.h"



#define QE_IMGSEN_CTRL_REG_READ		0x21
#define QE_IMGSEN_CTRL_REG_WRITE	0x22
#define QE_IMGSEN_CTRL_SET_EXP		0x23
#define QE_IMGSEN_CTRL_SET_GAN		0x24



typedef struct
{
	qe_u32 offs;
	qe_u32 addr;
	qe_u32 dlen;
	qe_u8 *data;
} qe_imgsen_reg_t;

struct qe_imgsen_ops;

struct qe_imgsen_device {
	struct qe_device parent;
	
	/**
	 * register read interface
	 */
	qe_ret (*reg_read)(struct qe_imgsen_device *, qe_imgsen_reg_t *);

	/**
	 * register write interface
	 */
	qe_ret (*reg_write)(struct qe_imgsen_device *, qe_imgsen_reg_t *);

	/**
	 * setting exposure time, unit is us
	 */
	qe_ret (*set_exposure)(struct qe_imgsen_device *, qe_u32 val);

	/**
	 * setting gian, unit is 0.1dB
	 */
	qe_ret (*set_gian)(struct qe_imgsen_device *, qe_u32 val);
};
typedef struct qe_imgsen_device qe_imgsen_t;



qe_ret qe_imgsen_register(struct qe_imgsen_device *sensor, const char *name,
	void *data);



#endif /* __QE_IMGSENSOR_H__ */


