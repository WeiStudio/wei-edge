


#ifndef __QE_BITSTREAM_H__
#define __QE_BITSTREAM_H__



#include "qe_def.h"



typedef struct
{
    const qe_u8 *buf;
    const qe_u8 *end;
    int offs;
}qe_bitstream;

void qe_bitstream_init(qe_bitstream *s, qe_const_ptr *buf, qe_size size);

qe_u32 qe_bitstream_test_bits(qe_bitstream *s, int n);

qe_u32 qe_bitstream_get_bits(qe_bitstream *s, int n);

void qe_bitstream_skip_bits(qe_bitstream *s, int n);



#endif /* __QE_BITSTREAM_H__ */