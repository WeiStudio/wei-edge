


#ifndef __QE_MEMORY_H__
#define __QE_MEMORY_H__



#include "autocheck.h"
#include "autoconfig.h"
#include "qe_def.h"



typedef enum {
    QEMEM_POOL       =  1 << 0,
} qemem_allocator_flags;

typedef struct
{
    qe_size total;
    qe_size used;
    qe_size free;
} qemem_minfo;

typedef struct
{
	void* (*malloc)(qe_size size);
	void* (*calloc)(qe_size nitems, qe_size size);
	void* (*realloc)(void *buf, qe_size size);
	void  (*free)(void *ptr);
	void  (*pool)(void *pool, qe_size size);
	void  (*minfo)(qemem_minfo *);
	qe_u8 flags;
} qemem_allocator;



#if defined(CONFIG_NANO)
int qe_memcmp(const void *cs,const void *ct, qe_size count);
void *qe_memcpy(void *dst, void *src, qe_size count);
void *qe_memset(void *s, int c, qe_size count);
void *qe_memmove(void *dest, const void *src, qe_size count);
void *qe_memscan(void *addr, int c, qe_size size);
void *qe_malloc(qe_size size);
void *qe_calloc(qe_size num, qe_size size);
void *qe_realloc(void *buf, qe_size size);
void  qe_free(void *ptr);
void qemem_set_pool(void *start, qe_size size);
void qemem_get_minfo(qemem_minfo *info);
void qemem_set_allocator(qemem_allocator *allocator);
#else

#if defined(HAVE_STRING_H)
#include <string.h>
#define qe_memcpy   memcpy
#define qe_memset   memset
#define qe_memcmp   memcmp
#define qe_memmove  memmove
#endif

#if defined(HAVE_MALLOC_H)
#include <malloc.h>
#define qe_malloc 	malloc
#define qe_free	  	free
#if defined(HAVE_CALLOC)
#define qe_calloc 	calloc
#endif
#if defined(HAVE_ZALLOC)
#define qe_zalloc 	zalloc
#endif
#if defined(HAVE_REALLOC)
#define qe_realloc 	realloc
#endif
#endif

#endif


#define qe_new(type)		qe_malloc(sizeof(type))
#define qe_new_n(type, n)	qe_calloc(sizeof(type), n)

void *qe_memdup(void *mem, qe_size size);



#endif /* __QE_MEMORY_H__ */