#pragma once

#define QESF_VERSION_MAJOR   1
#define QESF_VERSION_MINOR   1
#define QESF_VERSION_PATCH   9

#define QESF_VERSION         "1.1.9"
#define QESF_LIBRARY_NAME    "libqelib"
