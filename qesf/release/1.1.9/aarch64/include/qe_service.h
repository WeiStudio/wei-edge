
#ifndef __QE_SERVICE_H__
#define __QE_SERVICE_H__


#include "qe_def.h"
#include "qe_pack.h"
#include "qe_string.h"



void qe_set_errno(qe_ret e);
qe_ret qe_get_errno(void);
int qe_errno(void);
unsigned long int qe_htonl(unsigned long int x);
unsigned short int qe_htons(unsigned short int x);


#define qe_mask_exst(mask, flag)   		((mask) & (flag))
#define qe_mask_push(mask, flag)   		((mask) |= (flag))
#define qe_mask_pull(mask, flag)		((mask) &= (~flag))
#define qe_mask_only(mask, flag)   		(!((mask) & (~(flag))))



#endif /* SRC_QE_SERVICE_H_ */
