
#ifndef __QE_MATH_H__
#define __QE_MATH_H__



double qe_ln(double x);
double qe_exp(double x);
double qe_sqrt(double x);
double qe_pow(double a, int n);
double qe_powf(double a, double x);



#endif /* __QE_MATH_H__ */

