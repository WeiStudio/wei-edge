#ifndef __QE_ARGPARSE_H__
#define __QE_ARGPARSE_H__



#include "qe_def.h"



struct qe_argparse;
struct qe_argparse_option;
typedef struct qe_argparse qe_argp;
typedef struct qe_argparse_option qe_argp_option;

typedef qe_ret (*qe_argp_callback)(qe_argp *argp, qe_argp_option *option);


typedef enum {
    /* special */
    QE_ARGP_END,
    QE_ARGP_GROUP,
    /* options with no arguments */
    QE_ARGP_BOOLEAN,
    QE_ARGP_BIT,
    /* options with arguments (optional or required) */
    QE_ARGP_INTEGER,
    QE_ARGP_FLOAT,
    QE_ARGP_STRING
} qe_argp_option_type;

typedef enum {
    QE_OPT_UNSET = 1 << 0,
} qe_argp_option_flag;

typedef struct qe_argparse_option
{
    qe_argp_option_type type;
    const char short_name;
    qe_const_str long_name;
    qe_ptr value;
    qe_const_str help;
    qe_u32 data;
    qe_u32 flags;
    qe_argp_callback callback;
} qe_argp_option;

typedef struct qe_argparse
{
    qe_argp_option *options;
    const char *const *usages;
    qe_const_str description;
    qe_const_str epilog;
    qe_int argc;
    const char **argv;
    qe_const_str *outv;
    qe_int cpidx;
    const char *opt;
} qe_argp;

qe_ret qe_argp_help_cb_no_exit(qe_argp *argp, 
    qe_argp_option *option);

#define QE_OPT_ITEM(type, sname, lname, val, help, data, flags, cb) \
    {type, sname, lname, val, help, data, flags, cb}

#define QE_OPT_END() \
    QE_OPT_ITEM(QE_ARGP_END, 0, QE_NULL, QE_NULL, QE_NULL, 0, 0, QE_NULL)

#define QE_OPT_BOOLEAN(sname, lname, val, flag, help) \
    QE_OPT_ITEM(QE_ARGP_BOOLEAN, sname, lname, val, help, 0, flag, QE_NULL)

#define QE_OPT_BIT(sname, lname, val, bit, flag, help) \
    QE_OPT_ITEM(QE_ARGP_BIT, sname, lname, val, help, bit, flag, QE_NULL)

#define QE_OPT_INTEGER(sname, lname, val, help) \
    QE_OPT_ITEM(QE_ARGP_INTEGER, sname, lname, val, help, 0, 0, QE_NULL)

#define QE_OPT_FLOAT(sname, lname, val, help) \
    QE_OPT_ITEM(QE_ARGP_FLOAT, sname, lname, val, help, 0, 0, QE_NULL)

#define QE_OPT_STRING(sname, lname, val, help) \
    QE_OPT_ITEM(QE_ARGP_STRING, sname, lname, val, help, 0, 0, QE_NULL)

#define QE_OPT_HELP(sname, lname, help) \
    QE_OPT_ITEM(QE_ARGP_BOOLEAN, sname, lname, QE_NULL, help, 0, 0, qe_argp_help_cb_no_exit)

qe_ret qe_argp_init(qe_argp *argp, qe_argp_option *options, 
    const char *const *usages, 
    qe_const_str description, qe_const_str epilog);

qe_ret qe_argp_parse(qe_argp *argp, qe_int argc, const char **argv);

void qe_argp_usage(qe_argp *argp);



#endif /* __QE_ARGPARSE_H__ */