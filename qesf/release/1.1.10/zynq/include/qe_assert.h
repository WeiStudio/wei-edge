


#ifndef __QE_ASSERT_H__
#define __QE_ASSERT_H__



#include "qe_def.h"



typedef void (*qe_assert_hook_t)(const char *ex, const char *func, qe_size line);

void qe_assert_set_hook(void (*hook)(const char *, const char *, qe_size));
void qe_assert_handler(const char *ex_string, const char *func, qe_size line);

#define qe_assert(ex)                                                      	\
if (!(ex))                                                               	\
{                                                                         	\
    qe_assert_handler(#ex, __FUNCTION__, __LINE__);                       	\
}



#endif /* __QE_ASSERT_H__ */