


#ifndef __QE_ARRAY_H__
#define __QE_ARRAY_H__



#include "qe_def.h"



typedef struct
{
    qe_u8 *data;
    qe_uint len;
    qe_uint space;
    qe_uint elem_size;
} qe_array;



#define qe_array_index(a,t,i)      (((t*) (a)->data) [(i)])



qe_array *qe_array_new(qe_uint elem_size);
qe_array *qe_array_new_sized(qe_uint elem_size, qe_uint reserved_size);
qe_array *qe_array_append(qe_array *array, const void *data, qe_uint len);
qe_array *qe_array_prepend(qe_array *array, const void *data, qe_uint len);
qe_array *qe_array_insert(qe_array *array, qe_uint index, const void *data, qe_uint len);
qe_array *qe_array_set_size(qe_array *array, qe_uint length);
qe_array *qe_array_remove_index(qe_array *array, qe_uint index);
qe_array *qe_array_remove_range(qe_array *array, qe_uint index, qe_uint length);
void qe_array_sort(qe_array *array, int (*compare)(const void *, const void *));
char *qe_array_free(qe_array *array, qe_bool free_segment);


#endif /* __QE_ARRAY_H__ */