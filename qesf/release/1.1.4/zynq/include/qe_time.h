


#ifndef __QE_TIME_H__
#define __QE_TIME_H__



#include "qe_def.h"



typedef qe_u64 qe_time_t;
typedef qe_u64 qe_pts;

typedef struct {
	int year;
	int mon;
	int day;
	int hour;
	int min;
	int sec;
} qe_date_t;



qe_time_t qe_time(void);
qe_time_t qe_time_ms(void);
qe_time_t qe_time_us(void);
void qe_sleep(qe_time_t second);
void qe_msleep(qe_time_t ms);
void qe_usleep(qe_time_t ms);
void qe_date(qe_date_t *date);



void qe_time_sec_update(qe_time_t second);
void qe_second_to_date(qe_time_t seconds, qe_date_t *date);


#endif /* __QE_TIME_H__ */