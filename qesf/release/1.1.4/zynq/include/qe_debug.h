#ifndef __QE_DEBUG_H__
#define __QE_DEBUG_H__



#include "qe_def.h"
#include "qe_list.h"
#include "qe_time.h"


struct qe_debugctrl;

typedef void (*qe_dbgctrl_callback)(struct qe_debugctrl *, void *user);

typedef struct qe_debugctrl
{
    qe_str name;
    qe_ptr user;

    qe_u32 rate;
    qe_u32 once;
    qe_s32 max;
    qe_u32 loss;
    qe_u32 count;

    qe_u32 bps;
    qe_u32 bps_bytes;
    qe_u32 pps;
    qe_u32 pps_count;
    
    qe_time_t pts;
    
    qe_bool en;
    qe_bool running;

    qe_list list;

    qe_dbgctrl_callback callback;
}qe_dbgctrl;

qe_ret qe_dbgctrl_init(qe_dbgctrl *ctrl, qe_const_str name,
    qe_u32 rate, qe_u32 once, qe_s32 max,
    qe_dbgctrl_callback cb, qe_ptr user);

qe_list *qe_dbgctrl_get_list(void);
qe_dbgctrl *qe_dbgctrl_find(qe_const_str name);
qe_ret qe_dbgctrl_register(qe_dbgctrl *ctrl);
void qe_dbgctrl_unregister(qe_dbgctrl *ctrl);
void qe_dbgctrl_enable(qe_dbgctrl *ctrl);
void qe_dbgctrl_disable(qe_dbgctrl *ctrl);
void qe_dbgctrl_count_increase(qe_dbgctrl *ctrl);
void qe_dbgctrl_tps_update(qe_dbgctrl *ctrl, qe_time_t pts, qe_u32 bytes);
void qe_dbgctrl_count_update(qe_dbgctrl *ctrl, qe_u32 count);
void qe_dbgctrl_start(qe_dbgctrl *ctrl);
void qe_dbgctrl_stop(qe_dbgctrl *ctrl);
qe_bool qe_dbgctrl_should_stop(qe_dbgctrl *ctrl);
qe_bool qe_dbgctrl_is_enable(qe_dbgctrl *ctrl);
void qe_dbgctrl_clear(qe_dbgctrl *ctrl);

qe_u32 qe_dbgctrl_get_rate(qe_dbgctrl *ctrl);
void qe_dbgctrl_set_rate(qe_dbgctrl *ctrl, qe_u32 rate);

void qe_dbgctrl_set_max(qe_dbgctrl *ctrl, qe_s32 max);
void qe_dbgctrl_set_once(qe_dbgctrl *ctrl, qe_u32 once);

qe_u32 qe_dbgctrl_get_bps(qe_dbgctrl *ctrl);
qe_u32 qe_dbgctrl_get_pps(qe_dbgctrl *ctrl);
qe_u32 qe_dbgctrl_get_count(qe_dbgctrl *ctrl);
qe_u32 qe_dbgctrl_get_loss(qe_dbgctrl *ctrl);
void qe_dbgctrl_invoke_callback(qe_dbgctrl *ctrl);


#endif /* __QE_DEBUG_H__ */