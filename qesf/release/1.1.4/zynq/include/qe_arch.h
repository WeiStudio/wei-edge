
#ifndef __QE_ARCH_H__
#define __QE_ARCH_H__



#include "qe_def.h"



void qe_interrupt_disable(void);
void qe_interrupt_enable(void);
void qe_irq_disable(int irq);
void qe_irq_unregister(int irq);
qe_ret qe_irq_enable(int irq);
qe_ret qe_irq_register(int irq, void (*handuler)(void *), void *ref);



#endif /* __QE_HW_H__ */
