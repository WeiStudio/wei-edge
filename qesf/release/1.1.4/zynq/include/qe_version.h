#pragma once

#define QESF_VERSION_MAJOR   1
#define QESF_VERSION_MINOR   1
#define QESF_VERSION_PATCH   4

#define QESF_VERSION         "1.1.4"
#define QESF_LIBRARY_NAME    "libqelib"
