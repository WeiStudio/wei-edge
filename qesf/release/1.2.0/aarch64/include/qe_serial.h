

#ifndef __QE_SERIAL_H__
#define __QE_SERIAL_H__



#include "qe_def.h"
#include "qe_device.h"
#include "qe_buffer.h"



#define BAUD_RATE_2400                  2400
#define BAUD_RATE_4800                  4800
#define BAUD_RATE_9600                  9600
#define BAUD_RATE_19200                 19200
#define BAUD_RATE_38400                 38400
#define BAUD_RATE_57600                 57600
#define BAUD_RATE_115200                115200
#define BAUD_RATE_230400                230400
#define BAUD_RATE_256000                256000
#define BAUD_RATE_460800                460800
#define BAUD_RATE_921600                921600
#define BAUD_RATE_2000000               2000000
#define BAUD_RATE_3000000               3000000

#define DATA_BITS_5                     5
#define DATA_BITS_6                     6
#define DATA_BITS_7                     7
#define DATA_BITS_8                     8
#define DATA_BITS_9                     9

#define STOP_BITS_1                     0
#define STOP_BITS_2                     1
#define STOP_BITS_3                     2
#define STOP_BITS_4                     3

#define PARITY_NONE                     0
#define PARITY_ODD                      1
#define PARITY_EVEN                     2

#define BIT_ORDER_LSB                   0
#define BIT_ORDER_MSB                   1

#define NRZ_NORMAL                      0       /* Non Return to Zero : normal mode */
#define NRZ_INVERTED                    1       /* Non Return to Zero : inverted mode */

#ifndef QE_SERIAL_RBSZ
#define QE_SERIAL_RBSZ					64
#endif

#define QE_SERIAL_DMA_RX                0x01

#define QE_SERIAL_EVENT_RX_IND          0x01    /* Rx indication */
#define QE_SERIAL_EVENT_TX_DONE         0x02    /* Tx complete   */
#define QE_SERIAL_EVENT_RX_DMADONE      0x03    /* Rx DMA transfer done */
#define QE_SERIAL_EVENT_TX_DMADONE      0x04    /* Tx DMA transfer done */
#define QE_SERIAL_EVENT_RX_TIMEOUT      0x05    /* Rx timeout    */

#define QE_SERIAL_IOC_SET_RX_TRIGGER    0x21

/* Default config for serial configure struct */
#define QE_SERIAL_DEFCONFIG						\
{												\
	BAUD_RATE_115200, /* 115200 bits/s */		\
	DATA_BITS_8,	  /* 8 databits */			\
	STOP_BITS_1,	  /* 1 stopbit */		    \
	PARITY_NONE,	  /* No parity  */			\
	BIT_ORDER_LSB,								\
	NRZ_NORMAL,									\
	QE_SERIAL_RBSZ,								\
	0,											\
}

typedef struct {
    qe_u32 baud_rate;
    qe_u32 data_bits               :4;
    qe_u32 stop_bits               :2;
    qe_u32 parity                  :2;
    qe_u32 bit_order               :1;
    qe_u32 invert                  :1;
    qe_u32 bufsz                   :16;
    qe_u32 reserved                :6;
}qe_serial_configure;

struct qe_serial_rxfifo {
	qe_u8 *buffer;
	qe_u16 put_index;
	qe_u16 get_index;
	qe_bool	is_full;
};

struct qe_serial_rxdma {
    qe_bool activated;
};

struct qe_serial_txfifo
{
    void *completion;
};

struct qe_serial_device;

/**
 * uart operators
 */
typedef struct {
    qe_ret (*configure)(struct qe_serial_device *serial, qe_serial_configure *cfg);
    qe_ret (*ioctl)(struct qe_serial_device *serial, int cmd, void *arg);

    int (*putc)(struct qe_serial_device *serial, char c);
    int (*getc)(struct qe_serial_device *serial);

    qe_int (*transfer_start)(struct qe_serial_device *serial);

    qe_size (*dma_transmit)(struct qe_serial_device *serial, qe_u8 *buf, qe_size size, int direction);
}qe_uart_ops;

typedef struct qe_serial_device {
    qe_dev parent;

    const qe_uart_ops *ops;
    qe_serial_configure config;

    void *rx;
    void *tx;

    qe_ringbuffer *rx_ring;
    qe_ringbuffer *tx_ring;
}qe_serial_dev ;



qe_ret qe_serial_register(qe_serial_dev *serial, const char *name, 
    qe_u32 flag, void *data);

#endif /* __QE_SERIAL_H__ */

