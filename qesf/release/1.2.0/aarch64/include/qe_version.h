#pragma once

#define QESF_VERSION_MAJOR   1
#define QESF_VERSION_MINOR   2
#define QESF_VERSION_PATCH   0

#define QESF_VERSION         "1.2.0"
#define QESF_LIBRARY_NAME    "libqelib"
