


#ifndef __QE_ROLLINGFILE_H__
#define __QE_ROLLINGFILE_H__



#include "qe_def.h"
#include "qe_list.h"



#if defined(HAVE_FOPEN)
#include <stdio.h>
#else
#error "System dont't have fopen()"
#endif



typedef enum {
    QE_ROLLINGFILE_HOOK_BEFORE_REMOVE = 0,
    QE_ROLLINGFILE_HOOK_SIZEOF,
} qe_rollingfile_hook_e;

typedef void (*qe_rollingfile_hook_t)(const char *name);

typedef struct
{
    FILE *fp;
    qe_size max_size;
    qe_u32 num_roll;
    qe_u32 count;
    char *dir;
    char *name;
    char *extension;
    char *filename;
    qe_list rollings;
    qe_rollingfile_hook_t hooks[QE_ROLLINGFILE_HOOK_SIZEOF];
} qe_rollingfile;



qe_rollingfile *qe_rollingfile_new(const char *dir, const char *name, 
    const char *extension, qe_u32 num_roll, qe_size max_size);
qe_ret qe_rollingfile_append_buffer(qe_rollingfile *file, const void *buf, qe_size len);
qe_ret qe_rollingfile_append_string(qe_rollingfile *file, const char *string);
void qe_rollingfile_free(qe_rollingfile *rf);
void qe_rollingfile_set_hook(qe_rollingfile *rf, qe_rollingfile_hook_e type, 
    qe_rollingfile_hook_t hook);



#endif /* __QE_ROLLINGFILE_H__ */