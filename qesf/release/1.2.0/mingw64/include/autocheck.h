#define HAVE_STRING_H
#define HAVE_MALLOC_H
#define HAVE_STDARG_H
#define HAVE_DIRECT_H
#define HAVE_TIME_H
#define HAVE_PTHREAD_H

/* #undef HAVE_VASPRINTF */
#define HAVE_FOPEN
#define HAVE_TIME
#define HAVE_CALLOC
/* #undef HAVE_ZALLOC */
#define HAVE_REALLOC
