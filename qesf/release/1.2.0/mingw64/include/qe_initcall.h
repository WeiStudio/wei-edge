


#ifndef __QE_INITCALL_H__
#define __QE_INITCALL_H__


#include "qe_macros.h"



typedef int (*init_fn_t)(void);
typedef int (*qe_initcall_t)(void);

#define qe_init                 __attribute__((section(".init.text")))
#define qe_initdata             __attribute__((section(".init.data")))

#define QE_EXPORT(fn, level) \
	static qe_initcall_t __qe_initcall_##fn qe_used\
		qe_section(".qe_initcall."level) = fn;

/* chip architecture initialization */
#define QE_ARCH_EXPORT(fn)				QE_EXPORT(fn, "1")

/* board init routines will be called in board_init() function */
#define QE_BOARD_EXPORT(fn)          	QE_EXPORT(fn, "2")

/* components pre-initialization (pure software initilization) */
#define QE_PREV_EXPORT(fn)            	QE_EXPORT(fn, "3")

/* device initialization */
#define QE_DEVICE_EXPORT(fn)          	QE_EXPORT(fn, "4")

/* components initialization (dfs, lwip, ...) */
#define QE_COMPONENT_EXPORT(fn)       	QE_EXPORT(fn, "5")

/* environment initialization (mount disk, ...) */
#define QE_ENV_EXPORT(fn)             	QE_EXPORT(fn, "6")

/* appliation initialization (application etc ...) */
#define QE_APP_EXPORT(fn)				QE_EXPORT(fn, "7")
/* @Space Stage Initialization }@ */



void qe_initcall(void);



#endif /* __QE_INITCALL_H__ */