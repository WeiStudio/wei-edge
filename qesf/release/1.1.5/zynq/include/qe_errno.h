


#ifndef __QE_ERRNO_H__
#define __QE_ERRNO_H__



#include "qe_def.h"



qe_ret qe_get_errno(void);
void qe_set_errno(qe_ret e);
int qe_errno(void);



#endif /* __QE_ERRNO_H__ */