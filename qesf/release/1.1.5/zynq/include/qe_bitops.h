


#ifndef __QE_BITOPS_H__
#define __QE_BITOPS_H__



#include "qe_def.h"



#define QE_BIT(nr)          (1UL << (nr))
#define QE_BIT_ULL(nr)      (1ULL << (nr))
#define QE_BIT_MASK(nr)		(1UL << ((nr) % QE_BITS_PER_LONG))
#define QE_BIT_WORD(nr)		((nr) / QE_BITS_PER_LONG)
#define QE_BIT_ULL_MASK(nr)	(1ULL << ((nr) % QE_BITS_PER_LONG_LONG))
#define QE_BIT_ULL_WORD(nr)	((nr) / QE_BITS_PER_LONG_LONG)

/*
 * Create a contiguous bitmask starting at bit position @l and ending at
 * position @h. For example
 * QE_GENMASK_ULL(39, 21) gives us the 64bit vector 0x000000ffffe00000.
 */
#define QE_GENMASK(h, l) \
	(((~0UL) << (l)) & (~0UL >> (QE_BITS_PER_LONG - 1 - (h))))

#define QE_GENMASK_ULL(h, l) \
	(((~0ULL) << (l)) & (~0ULL >> (QE_BITS_PER_LONG_LONG - 1 - (h))))

#endif /* __QE_BITOPS_H__ */