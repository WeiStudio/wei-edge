
#ifndef __QE_ARCH_H__
#define __QE_ARCH_H__



#include "qe_def.h"



void qe_interrupt_disable(void);
void qe_interrupt_enable(void);
void qe_irq_disable(int irq);
void qe_irq_unregister(int irq);
qe_ret qe_irq_enable(int irq);
qe_ret qe_irq_register(int irq, void (*handuler)(void *), void *ref);
void qe_irq_set_priority_trigger_type(qe_u32 irq, qe_u8 prior, qe_u8 trigger);


#endif /* __QE_HW_H__ */
