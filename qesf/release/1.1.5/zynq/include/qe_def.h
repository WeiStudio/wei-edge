/*
 * Copyright (c) 2021-2021, WeiStudio
 *
 * License: Apache-2.0
 *
 * Change Logs:
 * Date           Author        Notes
 * 2021-07-13     WeiStudio     the first version
 */



#ifndef __QE_DEF_H__
#define __QE_DEF_H__



#include "autocheck.h"
#include "autoconfig.h"
#include "qe_macros.h"



QE_DECLEAR_BEGIN

#define QE_NULL                         (0)

typedef int								qe_bool;	/**< boolean type */

typedef signed   char                   qe_s8;      /**<  8bit integer type */
typedef unsigned char                   qe_u8;     	/**<  8bit unsigned integer type */
typedef signed   short                  qe_s16;     /**< 16bit integer type */
typedef unsigned short                  qe_u16;    	/**< 16bit unsigned integer type */
typedef signed   int                    qe_s32;     /**< 32bit integer type */
typedef unsigned int                    qe_u32;    	/**< 32bit unsigned integer type */
typedef signed   long long              qe_s64;     /**< 64bit integer type */
typedef unsigned long long              qe_u64;    	/**< 64bit unsigned integer type */
typedef int 							qe_int;
typedef unsigned int 					qe_uint;

typedef float							qe_float;
typedef double 							qe_double;

#if defined(CONFIG_SYS_LLP64)
typedef unsigned long long             	qe_ubase_t;     	/**< Nbit unsigned CPU related data type */
typedef long long                      	qe_base_t;      	/**< Nbit CPU related date type */
#else
typedef unsigned long                   qe_ubase_t;     	/**< Nbit unsigned CPU related data type */
typedef long                            qe_base_t;      	/**< Nbit CPU related date type */
#endif

typedef qe_ubase_t                      qe_size;      		/**< Type for size number */
typedef qe_base_t                       qe_offs;       		/**< Type for offset */

typedef void *							qe_ptr;
typedef char *							qe_str;
typedef const void *					qe_const_ptr;
typedef const char *					qe_const_str;

typedef void                            (*qe_destroy_notify)(qe_ptr data);

#define qe_true							(1)
#define qe_false						(0)

#define QE_WAIT_FOREVER					(-1)

#define QE_BITS_PER_BYTE                8
#define QE_BYTES_PER_LONG               sizeof(unsigned long)
#define QE_BITS_PER_LONG                (QE_BITS_PER_BYTE*QE_BYTES_PER_LONG)
#define QE_LONG_POW                     5
#define QE_DIV_ROUND_UP(n,d)            (((n) + (d) - 1) / (d))
#define QE_BITS_WORD(bits)              ((bits) / QE_BITS_PER_LONG)
#define QE_BITS_TO_LONG(bits)           QE_DIV_ROUND_UP(bits, QE_BITS_PER_LONG)

#define QE_S8_MIN				((qe_s8)0x80)
#define QE_S8_MAX				((qe_s8)0x7F)
#define QE_U8_MAX				((qe_u8)0xFF)
#define QE_S16_MIN				((qe_s16)0x8000)
#define QE_S16_MAX				((qe_s16)0x7FFF)
#define QE_U16_MAX				((qe_u16)0xFFFF)
#define QE_S32_MIN				((qe_s32)0x80000000)
#define QE_S32_MAX				((qe_s32)0x7FFFFFFF)
#define QE_U32_MAX				((qe_u32)0xFFFFFFFF)
#define QE_S64_MIN				((qe_s64)0x8000000000000000)
#define QE_S64_MAX				((qe_s64)0x7FFFFFFFFFFFFFFF)
#define QE_U64_MAX				((qe_u64)0xFFFFFFFFFFFFFFFF)


/* error code definitions @{ */
typedef enum {

	qe_ok 			= 0,

	/* 1~10: special return value, means non-error */
	qe_finish,					/* for waiting areas */
	qe_unfinished,				/* for waiting areas */
	qe_yield,					/* need to exit from current function scope */
	qe_noneed,					/* don't need to do something */
	qe_reboot,					/* system reboot */
	qe_killed,					/* object be killed */
	qe_save,

	qe_err_common  	= 10,
	qe_err_mem,					/* out of memory */
	qe_err_range,					/* out of range */
	qe_err_busy,				/* Resource busy */
    qe_err_timeout,
	qe_err_full,
	qe_err_empty,
	qe_err_param,
	qe_err_exist,
	qe_err_notexist = 20,
	qe_err_incomplete,			/* data incomplete */
	qe_err_notfind,
	qe_err_notsupport,
    qe_err_notenough,
	qe_err_match,
	qe_err_send,
	qe_err_recv,
	qe_err_write,
	qe_err_read,
	qe_err_io,
} qe_ret;

typedef qe_ret 							(*qe_lock_acquire)(qe_ptr lock, qe_uint wait);
typedef void							(*qe_lock_release)(qe_ptr lock);

/* 
 * @Space Application version @{
 */
#define qe_version_string_make(x,y,z,d) 	#x"."#y"."#z" "d
#define qe_version_string_build(x,y,z,d)	qe_version_string_make(x,y,z,d)
#define qe_version_encode(x,y,z)		(qe_u32)(((x)<<16)|((y)<<8)|(z))

struct qe_app_version {
    qe_u8 major;
    qe_u8 minor;
    qe_u8 patch;
    qe_u8 flag;
    qe_u32 vcode;
    char string[16];
    char date[16];
    char time[16];
};
/* @Space Application version }@ */


typedef struct {
    unsigned int x;
    unsigned int y;
} qe_dim2;

typedef struct {
    unsigned int x;
    unsigned int y;
    unsigned int z;
} qe_dim3;

typedef struct {
    unsigned int x;
    unsigned int y;
	unsigned int w;
	unsigned int h;
} qe_box;

/* Binary Package */
typedef struct
{
	qe_u32 tag;
	qe_u32 offset;
	qe_u32 size;
} qe_binpkg_section;

typedef struct
{
	qe_u32    magic;
	qe_u32    num_sections;
	qe_u32    sectab_offset;
} qe_binpkg_header;

typedef struct
{
	qe_ptr lock;
	qe_lock_acquire acquire;
	qe_lock_release release;
}qe_lock;

QE_DECLEAR_END

#endif /* __QE_DEF_H__ */
