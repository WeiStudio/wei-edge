


#ifndef __QE_MACROS_H__
#define __QE_MACROS_H__



#ifdef __cplusplus
#define QE_DECLEAR_BEGIN    extern "C" {
#define QE_DECLEAR_END      }
#else
#define QE_DECLEAR_BEGIN
#define QE_DECLEAR_END
#endif

/**
 * Compiler related macro definitions
 * 
 */
#define qe_section(x)           __attribute__((section(x)))
#define qe_unused               __attribute__((unused))
#define qe_used                 __attribute__((used))
#define qe_align(n)             __attribute__((aligned(n)))
#define qe_weak                 __attribute__((weak))
#define qe_inline               static __inline

#ifdef __GNUC__
#define qe_gunc_check_version(major, minor) \
    ((__GNUC__ > (major)) || \
     ((__GNUC__ == (major)) && \
      (__GNUC_MINOR__ >= (minor))))
#else
#define qe_gunc_check_version(major, minor) 0
#endif

/**
 * Likely and Unlikely
 * ========================
 */
#if qe_gunc_check_version(2, 0) && defined(__OPTIMIZE__)
#define qe_likely(expr) (__builtin_expect(!!(expr), 1))
#define qe_unlikely(expr) (__builtin_expect(!!(expr), 0))
#else
#define qe_likely(expr) (expr)
#define qe_unlikely(expr) (expr)
#endif

/**
 * ContainerOf
 * ===========
 */
#define qe_container_of(ptr, type, member) \
    ((type *)((char *)(ptr) - (qe_ubase_t)(&((type *)0)->member)))

#define qe_max(a, b)    (((a) > (b)) ? (a) : (b))
#define qe_min(a, b)    (((a) < (b)) ? (a) : (b))
#define qe_abs(a)	    (((a) < 0) ? -(a) : (a))
#define qe_clamp(x, low, high)  (((x) > (high)) ? (high) : (((x) < (low)) ? (low) : (x)))


/**
 * Array size define
 * =================
 */
#define qe_array_size(array) (sizeof(array)/sizeof(array[0]))
#define qe_array_foreach(head, size, p)				\
		int __i;									\
		p = &head[0];								\
													\
		for (__i = 0;								\
			 __i < size;							\
			 __i++, 								\
				p = &head[__i]) 					\

#define qe_array_foreach_start(head, size, p) 		\
{													\
	int __i;										\
	p = &head[0];									\
	for (__i = 0;									\
		__i < size;									\
		__i++, 										\
		p = &head[__i]) 							\

#define qe_array_foreach_end() \
}



#endif /*  */