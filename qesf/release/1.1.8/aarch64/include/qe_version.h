#pragma once

#define QESF_VERSION_MAJOR   1
#define QESF_VERSION_MINOR   1
#define QESF_VERSION_PATCH   8

#define QESF_VERSION         "1.1.8"
#define QESF_LIBRARY_NAME    "libqelib"
